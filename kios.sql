-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 08 Mei 2019 pada 10.03
-- Versi Server: 10.1.26-MariaDB
-- PHP Version: 7.0.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kios`
--

DELIMITER $$
--
-- Fungsi
--
CREATE DEFINER=`root`@`localhost` FUNCTION `insert_admin` (`nama` VARCHAR(100), `email` TEXT, `nip` VARCHAR(25), `jabatan` VARCHAR(50), `id_bidang` VARCHAR(12), `lv` INT(2), `pass` VARCHAR(64), `admin_del` VARCHAR(12), `time_update` DATETIME) RETURNS VARCHAR(14) CHARSET latin1 NO SQL
BEGIN
  declare last_key_user varchar(20);
  declare count_row_user int;
  declare fix_key_user varchar(12);
  
  select count(*) into count_row_user from admin 
  	where substr(id_admin,3,6) = left(NOW()+0, 6);
        
  select id_admin into last_key_user from admin
  	where substr(id_admin,3,6) = left(NOW()+0, 6)
  	order by id_admin desc limit 1;
    
  if(count_row_user <1) then
  	set fix_key_user = concat("AD",left(NOW()+0, 6),"0001");
  else
      	set fix_key_user = concat("AD",substr(last_key_user,3,10)+1);
      
  END IF;
  
  
  insert into admin values(fix_key_user, '0', lv, email, pass, '0', nama, nip, jabatan, id_bidang, '0', '0000-00-00 00:00:00', admin_del, time_update);
  
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_antrian` (`nik` VARCHAR(16), `id_layanan_menu` INT, `id_jenis_kesehatan` VARCHAR(8), `id_rs` VARCHAR(15), `id_poli` VARCHAR(11), `wkt_pendaftaran` DATETIME, `wkt_booking` DATETIME, `time_update` DATETIME, `id_admin` VARCHAR(12), `no_booking` VARCHAR(10)) RETURNS VARCHAR(21) CHARSET latin1 NO SQL
BEGIN
  declare last_key_user varchar(30);
  declare count_row_user int;
  declare fix_key_user varchar(30);
  
  select count(*) into count_row_user from kesehatan_antrian 
  	where substr(id_antrian,3,8) = left(NOW()+0, 8);
        
  select id_antrian into last_key_user from kesehatan_antrian
  	where substr(id_antrian,3,8) = left(NOW()+0, 8)
  	order by id_antrian desc limit 1;
    
  if(count_row_user <1) then
  	set fix_key_user = concat("AC",left(NOW()+0, 8),"100000001");
  else
    set fix_key_user = concat("AC",left(NOW()+0, 8), RIGHT(last_key_user,9)+1);
      
  END IF;
  
  insert into kesehatan_antrian values(fix_key_user, nik, id_layanan_menu, id_jenis_kesehatan, id_rs, id_poli, wkt_pendaftaran, wkt_booking, no_booking, '0', '', '', '0', time_update, id_admin);
 
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_dinas` (`nama_dinas` TEXT, `alamat` TEXT, `page_kelola` TEXT, `admin_del` VARCHAR(12), `time_update` DATETIME) RETURNS VARCHAR(8) CHARSET latin1 BEGIN
  declare last_key_item varchar(20);
  declare count_row_item int;
  declare fix_key_item varchar(12);
  
  select count(*) into count_row_item from dinas;
        
  select id_dinas into last_key_item from dinas order by id_dinas desc limit 1;
        
 if(count_row_item <1) then
  	set fix_key_item = concat("DN","200001");
  else
    set fix_key_item = concat("DN",RIGHT(last_key_item,6)+1);
      
  END IF;
  
  insert into dinas values(fix_key_item, nama_dinas, alamat, page_kelola, "0", "0000-00-00 00:00:00", admin_del, time_update);
  
  return fix_key_item;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_ij_antrian` (`nik` VARCHAR(16), `id_layanan_menu` VARCHAR(9), `id_jenis` VARCHAR(8), `id_kategori` VARCHAR(10), `id_sub` VARCHAR(10), `wkt_pendaftaran` DATETIME, `wkt_booking` DATETIME, `time_update` DATETIME, `id_admin` VARCHAR(12), `no_booking` VARCHAR(10)) RETURNS VARCHAR(21) CHARSET latin1 NO SQL
BEGIN
  declare last_key_user varchar(30);
  declare count_row_user int;
  declare fix_key_user varchar(30);
  
  select count(*) into count_row_user from ijin_antrian 
  	where substr(id_antrian,3,8) = left(NOW()+0, 8);
        
  select id_antrian into last_key_user from ijin_antrian
  	where substr(id_antrian,3,8) = left(NOW()+0, 8)
  	order by id_antrian desc limit 1;
    
  if(count_row_user <1) then
  	set fix_key_user = concat("IJ",left(NOW()+0, 8),"100000001");
  else
    set fix_key_user = concat("IJ",left(NOW()+0, 8), RIGHT(last_key_user,9)+1);
      
  END IF;
  
  insert into ijin_antrian values(fix_key_user, nik, id_layanan_menu, id_jenis, id_kategori, id_sub, wkt_pendaftaran, wkt_booking, no_booking, '0', '', '', '0', time_update, id_admin);
 
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_ij_jenis` (`ket_jenis` TEXT, `waktu` DATETIME, `id_admin` VARCHAR(12)) RETURNS VARCHAR(10) CHARSET latin1 BEGIN
  declare last_key_user varchar(20);
  declare count_row_user int;
  declare fix_key_user varchar(12);
  
  select count(*) into count_row_user from ijin_jenis;
        
  select id_jenis into last_key_user from ijin_jenis order by id_jenis desc limit 1;
        
  if(count_row_user <1) then
  	set fix_key_user = concat("IJJ","10001");
  else
    set fix_key_user = concat("IJJ",RIGHT(last_key_user,5)+1);  
  END IF;
  
  INSERT INTO ijin_jenis VALUES(fix_key_user, ket_jenis, "", "0", waktu, id_admin);
  
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_ij_kategori` (`id_jenis` VARCHAR(8), `ket_kategori` TEXT, `waktu` DATETIME, `id_admin` VARCHAR(12)) RETURNS VARCHAR(10) CHARSET latin1 BEGIN
  declare last_key_user varchar(20);
  declare count_row_user int;
  declare fix_key_user varchar(12);
  
  select count(*) into count_row_user from ijin_kategori WHERE substr(id_kategori, 4, 2) = RIGHT(id_jenis, 2);
        
  select id_kategori into last_key_user from ijin_kategori WHERE substr(id_kategori, 4, 2) = RIGHT(id_jenis, 2) order by id_kategori desc limit 1;
        
  if(count_row_user <1) then
  	set fix_key_user = concat("IJK",RIGHT(id_jenis,2),"10001");
  else
    set fix_key_user = concat("IJK",RIGHT(id_jenis,2),RIGHT(last_key_user,5)+1);  
  END IF;
  
  INSERT INTO ijin_kategori VALUES(fix_key_user, id_jenis, ket_kategori, "", "0", waktu, id_admin);
  
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_ij_sub` (`id_kategori` VARCHAR(12), `ket_sub` TEXT, `waktu` DATETIME, `id_admin` VARCHAR(12)) RETURNS VARCHAR(12) CHARSET latin1 BEGIN
  declare last_key_user varchar(20);
  declare count_row_user int;
  declare fix_key_user varchar(12);
  
  select count(*) into count_row_user from ijin_sub_kategori where substr(id_sub, 4, 2) = RIGHT(id_kategori, 2);
        
  select id_sub into last_key_user from ijin_sub_kategori where substr(id_sub, 4, 2) = RIGHT(id_kategori, 2) order by id_sub desc limit 1;
        
  if(count_row_user <1) then
  	set fix_key_user = concat("IJS",RIGHT(id_kategori,2),"10001");
  else
    set fix_key_user = concat("IJS",RIGHT(id_kategori,2),RIGHT(last_key_user,5)+1);  
  END IF;
  
  INSERT INTO ijin_sub_kategori VALUES(fix_key_user, id_kategori, ket_sub, "", "0", waktu, id_admin);
  
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_jenis_rs` (`nama` VARCHAR(75), `time_update` DATETIME, `id_admin` VARCHAR(12)) RETURNS VARCHAR(70) CHARSET latin1 NO SQL
BEGIN
  declare last_key_item varchar(20);
  declare count_row_item int;
  declare fix_key_item varchar(12);
  
  select count(*) into count_row_item from kesehatan_jenis;
        
  select id_layanan into last_key_item from kesehatan_jenis order by id_layanan desc limit 1;
        
 if(count_row_item <1) then
  	set fix_key_item = concat("KSJ","10001");
  else
    set fix_key_item = concat("KSJ",RIGHT(last_key_item,5)+1);
      
  END IF;
  
  insert into kesehatan_jenis values(fix_key_item, nama, "", "0", time_update, id_admin);
  
  return fix_key_item;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_kecamatan` (`nama` VARCHAR(50), `area` TEXT, `luas` DOUBLE, `id_admin` VARCHAR(12), `waktu` DATETIME) RETURNS VARCHAR(7) CHARSET latin1 BEGIN
  declare last_key_user varchar(20);
  declare count_row_user int;
  declare fix_key_user varchar(12);
  
  select count(*) into count_row_user from master_kecamatan;
        
  select id_kecamatan into last_key_user from master_kecamatan order by id_kecamatan desc limit 1;
        
  if(count_row_user <1) then
  	set fix_key_user = concat("KEC1","001");
  else
    set fix_key_user = concat("KEC",SUBSTR(last_key_user,4,4)+1);  
  END IF;
  
  INSERT INTO master_kecamatan VALUES(fix_key_user, nama, area, luas, "0", waktu, id_admin);
  
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_kelurahan` (`nama` VARCHAR(64), `area` TEXT, `luas` DOUBLE, `id_kec` VARCHAR(7), `id_admin` VARCHAR(12), `waktu` INT) RETURNS VARCHAR(7) CHARSET latin1 BEGIN
  declare last_key_user varchar(20);
  declare count_row_user int;
  declare fix_key_user varchar(12);
  
  select count(*) into count_row_user from master_kelurahan;
        
  select id_kelurahan into last_key_user from master_kelurahan order by id_kelurahan desc limit 1;
        
  if(count_row_user <1) then
  	set fix_key_user = concat("KEL1","001");
  else
    set fix_key_user = concat("KEL",SUBSTR(last_key_user,4,4)+1);  
  END IF;
  
  INSERT INTO master_kelurahan VALUES(fix_key_user, id_kec, nama, area, luas, "0", waktu, id_admin);
  
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_kp_antrian` (`nik` VARCHAR(16), `id_layanan_menu` INT, `id_jenis` VARCHAR(8), `id_kategori` VARCHAR(8), `wkt_pendaftaran` DATETIME, `wkt_booking` DATETIME, `time_update` DATETIME, `id_admin` VARCHAR(12), `no_booking` VARCHAR(10)) RETURNS VARCHAR(21) CHARSET latin1 NO SQL
BEGIN
  declare last_key_user varchar(30);
  declare count_row_user int;
  declare fix_key_user varchar(30);
  
  select count(*) into count_row_user from kependudukan_antrian 
  	where substr(id_antrian,3,8) = left(NOW()+0, 8);
        
  select id_antrian into last_key_user from kependudukan_antrian
  	where substr(id_antrian,3,8) = left(NOW()+0, 8)
  	order by id_antrian desc limit 1;
    
  if(count_row_user <1) then
  	set fix_key_user = concat("PD",left(NOW()+0, 8),"100000001");
  else
    set fix_key_user = concat("PD",left(NOW()+0, 8), RIGHT(last_key_user,9)+1);
      
  END IF;
  
  insert into kependudukan_antrian values(fix_key_user, nik, id_layanan_menu, id_jenis, id_kategori, wkt_pendaftaran, wkt_booking, no_booking, '0', '', '', '0', time_update, id_admin);
 
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_kp_jenis` (`nama` VARCHAR(75), `time_update` DATETIME, `id_admin` VARCHAR(12)) RETURNS VARCHAR(8) CHARSET latin1 NO SQL
BEGIN
  declare last_key_item varchar(20);
  declare count_row_item int;
  declare fix_key_item varchar(12);
  
  select count(*) into count_row_item from kependudukan_jenis;
        
  select id_jenis into last_key_item from kependudukan_jenis order by id_jenis desc limit 1;
        
 if(count_row_item <1) then
  	set fix_key_item = concat("KPJ","10001");
  else
    set fix_key_item = concat("KPJ",RIGHT(last_key_item,5)+1);
      
  END IF;
  
  insert into kependudukan_jenis values(fix_key_item, nama, "", "0", time_update, id_admin);
  
  return fix_key_item;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_kp_kategori` (`nama` VARCHAR(75), `time_update` DATETIME, `id_admin` VARCHAR(12), `id_jenis` VARCHAR(8)) RETURNS VARCHAR(8) CHARSET latin1 NO SQL
BEGIN
  declare last_key_item varchar(20);
  declare count_row_item int;
  declare fix_key_item varchar(12);
  
  select count(*) into count_row_item from kependudukan_kategori;
        
  select id_kategori into last_key_item from kependudukan_kategori order by id_kategori desc limit 1;
        
 if(count_row_item <1) then
  	set fix_key_item = concat("KPC","10001");
  else
    set fix_key_item = concat("KPC",RIGHT(last_key_item,5)+1);
      
  END IF;
  
  insert into kependudukan_kategori values(fix_key_item, id_jenis, nama, "", "0", time_update, id_admin);
  
  return fix_key_item;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_page_main` (`id_kategori` INT(11), `nama_page` VARCHAR(100), `next_page` TEXT, `waktu` DATETIME, `id_admin` VARCHAR(12)) RETURNS VARCHAR(70) CHARSET latin1 NO SQL
BEGIN
  declare last_key_item varchar(20);
  declare count_row_item int;
  declare fix_key_item varchar(12);
  
  select count(*) into count_row_item from home_page_main;
        
  select id_page into last_key_item from home_page_main order by id_page desc limit 1;
        
 if(count_row_item <1) then
  	set fix_key_item = concat("MENU","10001");
  else
    set fix_key_item = concat("MENU",RIGHT(last_key_item,5)+1);
      
  END IF;
  
  insert into home_page_main values(fix_key_item, id_kategori, nama_page, "-", next_page, "0", waktu, id_admin);
  
  return fix_key_item;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_pd_jenis` (`id_strata` VARCHAR(8), `nama_jenis` TEXT, `waktu` DATETIME, `id_admin` VARCHAR(12)) RETURNS VARCHAR(10) CHARSET latin1 BEGIN
  declare last_key_user varchar(20);
  declare count_row_user int;
  declare fix_key_user varchar(12);
  
  select count(*) into count_row_user from pendidikan_jenis WHERE substr(id_jenis, 4, 2) = RIGHT(id_strata,2);
        
  select id_jenis into last_key_user from pendidikan_jenis WHERE substr(id_jenis, 4, 2) = RIGHT(id_strata,2) order by id_jenis desc limit 1;
        
  if(count_row_user <1) then
  	set fix_key_user = concat("PDJ",RIGHT(id_strata,2),"10001");
  else
    set fix_key_user = concat("PDJ",RIGHT(id_strata,2),RIGHT(last_key_user,5)+1);  
  END IF;
  
  INSERT INTO pendidikan_jenis VALUES(fix_key_user, id_strata, nama_jenis, "0", waktu, id_admin);
  
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_pd_sekolah` (`id_jenis` VARCHAR(10), `id_kecamatan` VARCHAR(8), `id_kelurahan` VARCHAR(8), `nama_sekolah` TEXT, `lokasi` TEXT, `detail_sekolah` TEXT, `waktu` DATETIME, `id_admin` VARCHAR(12)) RETURNS VARCHAR(17) CHARSET latin1 BEGIN
  declare last_key_user varchar(20);
  declare count_row_user int;
  declare fix_key_user varchar(17);
  
  select count(*) into count_row_user from pendidikan_sekolah where substr(id_sekolah,4,8) = left(NOW()+0, 8);
        
  select id_sekolah into last_key_user from pendidikan_sekolah where substr(id_sekolah,4,8) = left(NOW()+0, 8) order by id_sekolah desc limit 1;
        
  if(count_row_user <1) then
  	set fix_key_user = concat("SKL", left(NOW()+0, 8)+0, "100001");
  else
    set fix_key_user = concat("SKL", left(NOW()+0, 8)+0, RIGHT(last_key_user,6)+1);  
  END IF;
  
  INSERT INTO pendidikan_sekolah VALUES(fix_key_user, id_jenis, id_kecamatan, id_kelurahan, nama_sekolah, "", lokasi, detail_sekolah, "0", waktu, id_admin);
  
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_pd_strata` (`nama` TEXT, `waktu` DATETIME, `id_admin` VARCHAR(12)) RETURNS VARCHAR(8) CHARSET latin1 BEGIN
  declare last_key_user varchar(20);
  declare count_row_user int;
  declare fix_key_user varchar(12);
  
  select count(*) into count_row_user from pendidikan_strata;
        
  select id_strata into last_key_user from pendidikan_strata order by id_strata desc limit 1;
        
  if(count_row_user <1) then
  	set fix_key_user = concat("PDST1","001");
  else
    set fix_key_user = concat("PDST",SUBSTR(last_key_user,5,4)+1);  
  END IF;
  
  INSERT INTO pendidikan_strata VALUES(fix_key_user, nama, "0", waktu, id_admin);
  
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_poli` (`nama` VARCHAR(30), `time_update` DATETIME, `id_admin` VARCHAR(12)) RETURNS VARCHAR(70) CHARSET latin1 NO SQL
BEGIN
  declare last_key_item varchar(20);
  declare count_row_item int;
  declare fix_key_item varchar(12);
  
  select count(*) into count_row_item from kesehatan_poli;
        
  select id_poli into last_key_item from kesehatan_poli order by id_poli desc limit 1;
        
 if(count_row_item <1) then
  	set fix_key_item = concat("P","10001");
  else
    set fix_key_item = concat("P",RIGHT(last_key_item,5)+1);
      
  END IF;
  
  insert into kesehatan_poli values(fix_key_item, nama, sha2(fix_key_item, '256'), "0", time_update, id_admin);
  
  return fix_key_item;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_rs` (`nama_rs` TEXT, `alamat` TEXT, `foto` VARCHAR(70), `telephone` VARCHAR(13), `id_layanan` VARCHAR(8), `id_poli` TEXT, `waktu` DATETIME, `id_admin` VARCHAR(12)) RETURNS VARCHAR(15) CHARSET latin1 NO SQL
BEGIN
  declare last_key_item varchar(20);
  declare count_row_item int;
  declare fix_key_item varchar(15);
  
  select count(*) into count_row_item from kesehatan_rs where substr(id_rs,3,8) = left(NOW()+0, 8);
        
  select id_rs into last_key_item from kesehatan_rs where substr(id_rs,3,8) = left(NOW()+0, 8) order by id_rs desc limit 1;
        
 if(count_row_item <1) then
  	set fix_key_item = concat("RS", left(NOW()+0, 8), "10001");
  else
    set fix_key_item = concat("RS",substr(last_key_item,3,13)+1);
      
  END IF;
  
  insert into kesehatan_rs values(fix_key_item, nama_rs, alamat, foto, telephone, id_layanan, id_poli, "0", waktu, id_admin);
  
  return fix_key_item;
  
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE `admin` (
  `id_admin` char(12) NOT NULL,
  `del` enum('0','1') NOT NULL,
  `id_lv` int(2) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(64) NOT NULL,
  `status_active` enum('0','1') NOT NULL,
  `nama` varchar(100) NOT NULL,
  `nip` varchar(25) NOT NULL,
  `jabatan` varchar(50) NOT NULL,
  `id_bidang` varchar(8) NOT NULL,
  `is_del` enum('0','1') NOT NULL,
  `time_del` datetime NOT NULL,
  `admin_del` varchar(12) NOT NULL,
  `time_update` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`id_admin`, `del`, `id_lv`, `email`, `password`, `status_active`, `nama`, `nip`, `jabatan`, `id_bidang`, `is_del`, `time_del`, `admin_del`, `time_update`) VALUES
('AD2019020001', '0', 1, 'kominfo_super@gmail.com', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', '1', 'Suwardi Surya Ningrat', '19091999012130', 'staff bidang penelitian', 'DN200002', '0', '2019-03-13 08:22:29', '0', '2019-02-21 00:00:00'),
('AD2019030001', '0', 1, 'dinkes@gmail.com', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', '1', 'Sukiman Sumajono', '123456789011', 'Pengamat lapangan', 'DN200002', '0', '2019-03-13 08:22:20', 'AD2019020001', '2019-03-13 08:17:16'),
('AD2019040001', '0', 1, 'meilinda_ika@yahoo.com', '7717a57ec3d7fb939eff71741ceca0a01808b7b6ea01edb40b9b7facf5a061cf', '0', 'Mei', '5555555555555555', 'tpok', 'DN200002', '0', '0000-00-00 00:00:00', 'AD2019020001', '2019-04-15 10:49:01');

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin_lv`
--

CREATE TABLE `admin_lv` (
  `id_lv` int(2) NOT NULL,
  `ket` varchar(32) NOT NULL,
  `is_del` enum('0','1') NOT NULL,
  `time_del` datetime NOT NULL,
  `admin_del` varchar(12) NOT NULL,
  `time_update` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `admin_lv`
--

INSERT INTO `admin_lv` (`id_lv`, `ket`, `is_del`, `time_del`, `admin_del`, `time_update`) VALUES
(1, 'admin super', '0', '0000-00-00 00:00:00', '0', '2019-02-21 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `device`
--

CREATE TABLE `device` (
  `id_device_kios` varchar(7) NOT NULL,
  `ip_lan` varchar(16) NOT NULL,
  `ip_public` varchar(16) NOT NULL,
  `key` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `device`
--

INSERT INTO `device` (`id_device_kios`, `ip_lan`, `ip_public`, `key`) VALUES
('1', '192.168.21.26', '36.74.196.6', 'myname_surya_hanggara'),
('2', '192.168.86.254', '222.124.213.68', 'aku_ini_sehat');

-- --------------------------------------------------------

--
-- Struktur dari tabel `dinas`
--

CREATE TABLE `dinas` (
  `id_dinas` varchar(8) NOT NULL,
  `nama_dinas` text NOT NULL,
  `alamat` text NOT NULL,
  `page_kelola` text NOT NULL,
  `is_del` enum('0','1') NOT NULL,
  `time_del` datetime NOT NULL,
  `admin_del` varchar(12) NOT NULL,
  `time_update` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `dinas`
--

INSERT INTO `dinas` (`id_dinas`, `nama_dinas`, `alamat`, `page_kelola`, `is_del`, `time_del`, `admin_del`, `time_update`) VALUES
('DN200001', 'Dinas Perdagangan', 'Jl. Simp. Terusan Danau Sentani 3 Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200002', 'Dinas Komunikasi dan Informatika (DISKOMINFO)', 'Perkantoran Terpadu Gedung A Lt. 4 Malang, Jl. Mayjend. Sungkono Malang 65132', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200003', 'Dinas Kebudayaan dan Pariwisata (DISBUDPAR)', 'Museum Mpu Purwa, Jl. Sukarno Hatta B. 210 Malang 65142', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200004', 'Dinas Perindustrian (DISPERIN)', 'Perkantoran Terpadu Gedung A Lt.3, Jl. Mayjen Sungkono Malang 65132', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200005', 'Dinas Pendidikan (DISDIK)', 'Jl. Veteran 19 Malang 65145', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200006', 'Dinas Kesehatan (DINKES)', 'Jl. Simp. Laksda Adisucipto 45 Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200007', 'Dinas Perhubungan (DISHUB)', 'Jl. Raden Intan 1 Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200008', 'Dinas Pertanian dan Ketahanan Pangan', 'Jl. A. Yani Utara 202 Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200009', 'Dinas Perumahan dan Kawasan Pemukiman (DISPERKIM)', 'Jl. Bingkil 1 Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200010', 'Dinas Koperasi dan Usaha Mikro', 'Jl. Panji Suroso 18 Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200011', 'Dinas Kepemudaan dan Olahraga (DISPORA)', 'Jl. Tenes Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200012', 'Dinas Kepedudukan dan Pencatatan Sipil (DISPENDUKCAPIL)', 'Perkantoran Terpadu Gedung A Lt. 2, Jl. Mayjen Sungkono Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200013', 'Dinas Pemberdayaan Perempuan, Perlindungan Anak, Pengendalian Penduduk dan Keluarga Berencana (DP3AP2KB)', 'Jl. Ki Ageng Gribig Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200014', 'Dinas Lingkungan Hidup (DLH)', 'Jl. Mojopahit Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200015', 'Dinas Perpustakaan Umum dan Arsip Daerah', 'Jl. Besar Ijen No.30a Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200016', 'Badan Kesatuan Bangsa dan Politik (BAKESBANGPOL)', 'Jl. Jend. A. Yani 98 Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200017', 'Badan Kepegawaian Daerah', 'Jl. Tugu 1 Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200018', 'Dinas Penanaman Modal dan Pelayanan Terpadu Satu Pintu (DPMPTSP)', 'Perkantoran Terpadu Gedung A Lt. 2 Malang,\r\nJl. Mayjend. Sungkono Malang 65132', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200019', 'Dinas Tenaga Kerja (DISNAKER)', 'Perkantoran Terpadu Gedung B Lt.3, Jl. Mayjen Sungkono Malang 65132', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200020', 'Dinas Pekerjaan Umum dan Penataan Ruang (DPUPR)', ' Jl. Bingkil 1 Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200021', 'Dinas Sosial (DINSOS)', 'Jl. Sulfat No. 12 Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200022', 'Inspektorat', 'Jl. Gajah Mada No. 2A Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200023', 'Badan Perencanaan, Penelitian dan Pengembangan (Barenlitbang)', 'Jl. Tugu No. 1 Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200024', 'Badan Pelayanan Pajak Daerah (BP2D)', 'Perkantoran Terpadu Gedung B Lt.1 Jl. Mayjen Sungkono Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200025', 'Badan Pengelola Keuangan dan Aset Daerah (BPKAD)', 'Jl. Tugu 1 Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200026', 'Badan Penanggulangan Bencana Daerah (BPBD)', 'Jl. Jend. A. Yani 98 Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200027', 'Satuan Polisi Pamong Praja', 'Jl. Simpang Mojopahit No 1 Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200028', 'Kecamatan Klojen', 'Jl. Surabaya 6 Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200029', 'Kecamatan Blimbing', 'Jl. Raden Intan Kav. 14 Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200030', 'Kecamatan Lowokwaru', 'Jl. Cengger Ayam I/12 Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200031', 'Kecamatan Sukun', 'Jl. Keben I Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200032', 'Kecamatan Kedungkandang', 'Jl. Mayjen Sungkono 59 Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200033', 'Badan Pengawas Pemilihan Umum (BAWASLU)', 'Jl. Teluk Cendrawasih No.206, Arjosari, Blimbing, Kota Malang, Jawa Timur 65126', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200034', 'Dewan Perwakilan Rakyat Daerah Kota Malang (DPRD)', 'Jl. Tugu No.1A, Kiduldalem, Klojen, Kota Malang, Jawa Timur 65119', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200035', 'Rumah Potong Hewan Kota Malang', 'Jl. Kol. Sugiono No. 176, Malang, Jawa Timur 65148', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200036', 'PDAM Kota Malang ', 'Jl. Danau Sentani Raya No.100, Madyopuro, Kedungkandang, Kota Malang, Jawa Timur 65142', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200037', 'Komisi Pemilihan Umum Kota Malang (KPU) ', 'Jl. Bantaran No.6, Purwantoro, Blimbing, Kota Malang, Jawa Timur 65126', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200038', 'Bank Perkreditan Rakyat', 'JL. Borobudur no. 18', '', '0', '0000-00-00 00:00:00', 'AD2019020001', '2019-02-28 09:48:41'),
('50', 'dinas perbuatan tidak menyenangkan', 'Malang', '', '1', '2019-02-27 03:35:41', 'AD2019020001', '2019-02-27 03:35:24'),
('52', 'surya', 'surya', '', '1', '2019-03-04 02:58:07', 'AD2019020001', '2019-03-01 03:57:29'),
('', 'admin', 'malang', '{}', '0', '0000-00-00 00:00:00', '0', '2019-03-13 10:03:22');

-- --------------------------------------------------------

--
-- Struktur dari tabel `home_page_kategori`
--

CREATE TABLE `home_page_kategori` (
  `id_kategori` int(11) NOT NULL,
  `nama_kategori` text NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `waktu` datetime NOT NULL,
  `id_admin` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `home_page_kategori`
--

INSERT INTO `home_page_kategori` (`id_kategori`, `nama_kategori`, `is_delete`, `waktu`, `id_admin`) VALUES
(1, 'Daftar Antrian', '0', '2019-04-02 09:07:12', 'AD2019020001'),
(2, 'Pelayanan Kesehatan', '0', '2019-04-02 09:08:29', 'AD2019020001'),
(3, 'Pelayanan Pendidikan', '0', '2019-04-02 09:08:44', 'AD2019020001'),
(4, 'Pariwisata dan Lingkungan Terbuka Hijau', '0', '2019-04-02 09:09:22', 'AD2019020001');

-- --------------------------------------------------------

--
-- Struktur dari tabel `home_page_main`
--

CREATE TABLE `home_page_main` (
  `id_page` varchar(9) NOT NULL,
  `id_kategori` int(11) NOT NULL,
  `nama_page` text NOT NULL,
  `foto_page` varchar(70) NOT NULL,
  `next_page` text NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `waktu` datetime NOT NULL,
  `id_admin` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `home_page_main`
--

INSERT INTO `home_page_main` (`id_page`, `id_kategori`, `nama_page`, `foto_page`, `next_page`, `is_delete`, `waktu`, `id_admin`) VALUES
('MENU10001', 1, 'Kesehatan', 'ed88bc4c866227e94d8eda9d75e84e1a710dbad4313b38a21e2f98da393f8c45.jpg', 'beranda/kesehatan/jenis_rumah_sakit', '0', '2019-04-23 05:50:07', 'AD2019020001'),
('MENU10002', 1, 'Kependudukan', 'cc2cd863e992fa37b539a9b9f0d694e1372913711bf38eaedcf6944d80853f6b.jpg', 'beranda/kependudukan/jenis', '0', '2019-05-03 04:34:18', 'AD2019020001'),
('MENU10003', 1, 'Perijinan', 'e429082bcf2cf1a17177355ac172db5ddeb8f12a2c11bcd91a71d4d988dde9b7.jpg', 'beranda/perijinan/jenis', '0', '2019-05-03 09:31:49', 'AD2019020001'),
('MENU10004', 3, 'Daftar Sekolah Menengah Pertama', '72899716adeffb9cf601d31228768070cd754dced9d930c7725a539ad04821df.jpg', 'beranda/pendidikan/a723ff721d91d9f34309debc39b7082f69c6b9990eb1c4aff93361aeec412bcb', '0', '2019-05-06 09:03:22', 'AD2019020001'),
('MENU10005', 3, 'Daftar Sekolah Dasar', 'b2c92547857a6d646e1353592b56ddfa12c8bd41e573b7d31fa0977030b8eac0.jpg', 'beranda/pendidikan/7fd7db09359f9723b6ba6e98be7a685ceae45115d77f5d7026c167a3f6e557b9', '0', '2019-05-06 09:03:39', 'AD2019020001'),
('MENU10006', 3, 'Daftar Sekolah Menegah Atas dan Kejuruan', 'bb0e73ca3117728c47a3f4857a6d193da4a70ce1e80da5cd0ebd2aa29931b0a9.jpg', 'beranda/pendidikan/53b03dc2c8be76a9528f86943503b65f3381f0c773882b537e9437b02a36cbbb', '0', '2019-05-06 09:03:54', 'AD2019020001'),
('MENU10007', 3, 'Pondok Pesantren', '8c0de8bb3e203c5611262e9e33814f8632a864a1889c43c75df0e184355e8b30.jpg', 'localhost', '0', '2019-04-23 06:00:16', 'AD2019020001'),
('MENU10008', 3, 'Universitas, Politehnik, Sekolah Tinggi dan Akademi', '44e81762f202cc44c6cf991597904802907e8e01a11ebef94d460f062575e06d.jpg', 'localhost', '0', '2019-04-23 08:22:16', 'AD2019020001'),
('MENU10009', 3, 'Perpustakaan', '6ba4c8b0fcea36da1c18274363b1be1bcf76dbaf90b81bb1f84a47a6ef81311a.jpg', 'local', '0', '2019-04-23 06:01:42', 'AD2019020001'),
('MENU10010', 2, 'Daftar Puskesmas', '03cab6c4801747cd8e34811cd74ebaf32e85422122f23ebf5cb41047cdeb224e.jpg', 'local', '0', '2019-04-23 06:04:07', 'AD2019020001'),
('MENU10011', 2, 'Daftar Klinik', '9f4c5c0c6117a92e225b76e9c5b315e3dd21429eaa187c0652596e7108fed168.jpg', 'local', '0', '2019-04-23 06:04:30', 'AD2019020001'),
('MENU10012', 2, 'Daftar Rumah Sakit', '973f1680e3b39cea60ce18e29f3e93bee61fcbd6ae193ffa3e4586f0520dd17c.jpg', 'local', '0', '2019-04-23 06:05:04', 'AD2019020001'),
('MENU10013', 2, 'Apotik', 'fb3c2af21f1b5021f459269e892f5edcc716873cc46ef8ade17db2e30d66520a.jpg', 'local', '0', '2019-04-23 06:09:32', 'AD2019020001'),
('MENU10014', 2, 'Dokter Praktek', '84d0e058a241a28ee081e938d912a4ca859cb6553a151fd67820b5b933042748.jpg', 'local', '0', '2019-04-23 06:08:10', 'AD2019020001'),
('MENU10015', 2, 'Dokter Gigi', '1f95613edfef290f100164ed291c24be3b96e697f120fc53b6a39d6b03ec8b3e.jpg', 'local', '0', '2019-04-23 06:08:39', 'AD2019020001'),
('MENU10016', 2, 'Optik', '489b6471fed740d5057001bfea73e4038ea9b27278f440b19774ff9c6352ef9a.jpg', 'local', '0', '2019-04-23 06:09:18', 'AD2019020001'),
('MENU10017', 2, 'Laboratorium', '786b8c352d1a0fc2c9cbd301c13d707ef5f4cdb690cd90827e1355d63d65afd3.jpg', 'local', '0', '2019-04-23 06:10:09', 'AD2019020001');

-- --------------------------------------------------------

--
-- Struktur dari tabel `ijin_antrian`
--

CREATE TABLE `ijin_antrian` (
  `id_antrian` varchar(30) NOT NULL,
  `nik` varchar(16) NOT NULL,
  `id_layanan_menu` varchar(9) NOT NULL,
  `id_jenis` varchar(8) NOT NULL,
  `id_kategori` varchar(10) NOT NULL,
  `id_sub` varchar(10) NOT NULL,
  `wkt_pendaftaran` datetime NOT NULL,
  `wkt_booking` datetime NOT NULL,
  `no_booking` varchar(5) NOT NULL,
  `status_antrian` enum('0','1','2','3') NOT NULL,
  `admin_acc` varchar(12) NOT NULL,
  `text_antrian` text NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `time_update` datetime NOT NULL,
  `id_admin` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `ijin_antrian`
--

INSERT INTO `ijin_antrian` (`id_antrian`, `nik`, `id_layanan_menu`, `id_jenis`, `id_kategori`, `id_sub`, `wkt_pendaftaran`, `wkt_booking`, `no_booking`, `status_antrian`, `admin_acc`, `text_antrian`, `is_delete`, `time_update`, `id_admin`) VALUES
('IJ20190506100000001', '3573011903940006', 'MENU10003', 'IJJ10001', 'IJK0110001', 'IJS0110001', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0', '0', '', '', '0', '0000-00-00 00:00:00', '0'),
('IJ20190506100000002', '3573011903940006', 'MENU10003', 'IJJ10001', 'IJK0110001', 'IJS0110001', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0', '0', '', '', '0', '0000-00-00 00:00:00', '0'),
('IJ20190506100000003', '3573011903940006', 'MENU10003', 'IJJ10001', 'IJK0110001', 'IJS0110001', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0', '0', '', '', '0', '0000-00-00 00:00:00', '0'),
('IJ20190506100000004', '3573011903940006', 'MENU10003', 'IJJ10001', 'IJK0110001', '', '2019-05-06 05:15:15', '2019-05-06 00:00:00', 'A-002', '0', '', '', '0', '2019-05-06 05:15:15', '0'),
('IJ20190506100000005', '3573011903940006', 'MENU10003', 'IJJ10001', 'IJK0110001', 'IJS0110001', '2019-05-06 05:16:03', '2019-05-06 00:00:00', 'A-002', '0', '', '', '0', '2019-05-06 05:16:03', '0'),
('IJ20190506100000006', '3573011903940006', 'MENU10003', 'IJJ10001', 'IJK0110001', 'IJS0110001', '2019-05-06 05:17:21', '2019-05-06 00:00:00', 'A-002', '0', '', '', '0', '2019-05-06 05:17:21', '0'),
('IJ20190506100000007', '3573011903940006', 'MENU10003', 'IJJ10001', 'IJK0110001', 'IJS0110001', '2019-05-06 05:17:46', '2019-05-06 00:00:00', 'A-002', '0', '', '', '0', '2019-05-06 05:17:46', '0'),
('IJ20190506100000008', '3573011903940006', 'MENU10003', 'IJJ10001', 'IJK0110001', 'IJS0110001', '2019-05-06 05:18:54', '2019-05-06 00:00:00', 'A-002', '0', '', '', '0', '2019-05-06 05:18:54', '0'),
('IJ20190506100000009', '3573011903940006', 'MENU10003', 'IJJ10001', 'IJK0110001', 'IJS0110001', '2019-05-06 05:22:09', '2019-05-06 00:00:00', 'A-002', '0', '', '', '0', '2019-05-06 05:22:09', '0'),
('IJ20190506100000010', '3573011903940006', 'MENU10003', 'IJJ10001', 'IJK0110002', 'IJS0210002', '2019-05-06 05:26:08', '2019-05-08 00:00:00', 'A-002', '0', '', '', '0', '2019-05-06 05:26:08', '0');

-- --------------------------------------------------------

--
-- Struktur dari tabel `ijin_jenis`
--

CREATE TABLE `ijin_jenis` (
  `id_jenis` varchar(8) NOT NULL,
  `ket_jenis` text NOT NULL,
  `foto_jenis` varchar(70) NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `waktu` datetime NOT NULL,
  `id_admin` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `ijin_jenis`
--

INSERT INTO `ijin_jenis` (`id_jenis`, `ket_jenis`, `foto_jenis`, `is_delete`, `waktu`, `id_admin`) VALUES
('IJJ10001', 'usaha pariwisata', 'b189b2f570a3504e71d1b204a1f88f93d2f823f9c4e24373dec77c0bbfd36e6c.jpg', '0', '2019-04-30 07:47:10', 'AD2019020001'),
('IJJ10002', 'usaha makanan', '4371f391ef11c3f8ade3b53b6720a5ca7b2e8ff81985c2cb83fc51f1e8d0c79a.jpg', '0', '2019-04-30 07:48:42', 'AD2019020001');

-- --------------------------------------------------------

--
-- Struktur dari tabel `ijin_kategori`
--

CREATE TABLE `ijin_kategori` (
  `id_kategori` varchar(10) NOT NULL,
  `id_jenis` varchar(8) NOT NULL,
  `ket_kategori` text NOT NULL,
  `foto_kategori` varchar(70) NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `waktu` datetime NOT NULL,
  `id_admin` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `ijin_kategori`
--

INSERT INTO `ijin_kategori` (`id_kategori`, `id_jenis`, `ket_kategori`, `foto_kategori`, `is_delete`, `waktu`, `id_admin`) VALUES
('IJK0110001', 'IJJ10001', 'ijin keramainan umum', 'cc8069250df5515236f666f0070a5de9cfb5b6ad7f5223a5b631cb52b22585ea.jpg', '0', '2019-04-30 08:29:27', 'AD2019020001'),
('IJK0110002', 'IJJ10001', 'ijin penggunaan tanah atau makam', '02fa195b7e83d3edd5ee8adfdb24327c29b70d4c1b77246c6abc5a89e0eb4d36.jpg', '0', '2019-04-30 08:29:35', 'AD2019020001'),
('IJK0110003', 'IJJ10001', 'ijin sewa', 'c10b1999e0e3d079789db43b05e027b5742eaaa0e2822584d1612e6e93b3adbd.jpg', '0', '2019-04-30 08:29:42', 'AD2019020001');

-- --------------------------------------------------------

--
-- Struktur dari tabel `ijin_sub_kategori`
--

CREATE TABLE `ijin_sub_kategori` (
  `id_sub` varchar(10) NOT NULL,
  `id_kategori` varchar(10) NOT NULL,
  `ket_sub` text NOT NULL,
  `foto_sub` text NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `time_update` datetime NOT NULL,
  `id_admin` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `ijin_sub_kategori`
--

INSERT INTO `ijin_sub_kategori` (`id_sub`, `id_kategori`, `ket_sub`, `foto_sub`, `is_delete`, `time_update`, `id_admin`) VALUES
('IJS0110001', 'IJK0110001', 'Memperpanjang', '39430fb6f0f37571c6674ed5947604f20e197c88f0a424a17a76ee8bbdd8b8d6.jpg', '0', '2019-05-02 10:33:21', 'AD2019020001'),
('IJS0110002', 'IJK0110001', 'ok', 'ec166c7bd6f59683052ac9ee83d9fac34d464dada02350accb679ef7a14975fd.jpg', '0', '2019-05-02 10:36:58', 'AD2019020001'),
('IJS0210001', 'IJK0110002', 'Membuat Baru', '49ca361f5fa014e9a6888f93bb629b7b75e47ee8e8817362c493af03ece1c95f.jpg', '0', '2019-05-02 10:34:55', 'AD2019020001'),
('IJS0210002', 'IJK0110002', 'Memperpanjang', 'd1368b1f694c97c2e2fbe560c8423a29969a1be45fb02482db28452cb646cb40.jpg', '0', '2019-05-02 10:35:15', 'AD2019020001');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kependudukan_antrian`
--

CREATE TABLE `kependudukan_antrian` (
  `id_antrian` varchar(30) NOT NULL,
  `nik` varchar(16) NOT NULL,
  `id_layanan_menu` varchar(1) NOT NULL,
  `id_jenis` varchar(8) NOT NULL,
  `id_kategori` varchar(8) NOT NULL,
  `wkt_pendaftaran` datetime NOT NULL,
  `wkt_booking` datetime NOT NULL,
  `no_booking` varchar(5) NOT NULL,
  `status_antrian` enum('0','1','2','3') NOT NULL,
  `admin_acc` varchar(12) NOT NULL,
  `text_antrian` text NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `time_update` datetime NOT NULL,
  `id_admin` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kependudukan_antrian`
--

INSERT INTO `kependudukan_antrian` (`id_antrian`, `nik`, `id_layanan_menu`, `id_jenis`, `id_kategori`, `wkt_pendaftaran`, `wkt_booking`, `no_booking`, `status_antrian`, `admin_acc`, `text_antrian`, `is_delete`, `time_update`, `id_admin`) VALUES
('PD20190503100000001', '3573011903940006', '0', 'KPJ10001', 'KPC10001', '2019-05-03 08:02:03', '2019-05-03 00:00:00', 'A-002', '0', '', '', '0', '2019-05-03 08:02:03', '0'),
('PD20190503100000002', '3573011903940006', '0', 'KPJ10001', 'KPC10001', '2019-05-03 08:02:53', '2019-05-03 00:00:00', 'A-002', '0', '', '', '0', '2019-05-03 08:02:53', '0'),
('PD20190503100000003', '3573011903940006', '0', 'KPJ10001', 'KPC10001', '2019-05-03 08:04:09', '2019-05-03 00:00:00', 'A-002', '0', '', '', '0', '2019-05-03 08:04:09', '0'),
('PD20190503100000004', '3573011903940006', '0', 'KPJ10001', 'KPC10001', '2019-05-03 08:04:49', '2019-05-03 00:00:00', 'A-002', '0', '', '', '0', '2019-05-03 08:04:49', '0'),
('PD20190503100000005', '3573011903940006', '0', 'KPJ10001', 'KPC10001', '2019-05-03 08:05:02', '2019-05-03 00:00:00', 'A-002', '0', '', '', '0', '2019-05-03 08:05:02', '0'),
('PD20190503100000006', '3573011903940006', '0', 'KPJ10001', 'KPC10001', '2019-05-03 08:05:35', '2019-05-03 00:00:00', 'A-002', '0', '', '', '0', '2019-05-03 08:05:35', '0'),
('PD20190503100000007', '3573011903940006', '0', 'KPJ10001', 'KPC10001', '2019-05-03 08:06:24', '2019-05-03 00:00:00', 'A-002', '0', '', '', '0', '2019-05-03 08:06:24', '0'),
('PD20190503100000008', '3573011903940006', '0', 'KPJ10001', 'KPC10001', '2019-05-03 08:07:13', '2019-05-03 00:00:00', 'A-002', '0', '', '', '0', '2019-05-03 08:07:13', '0'),
('PD20190503100000009', '3573011903940006', '0', 'KPJ10001', 'KPC10001', '2019-05-03 08:09:14', '2019-05-03 00:00:00', 'A-002', '0', '', '', '0', '2019-05-03 08:09:14', '0'),
('PD20190503100000010', '3573011903940006', '0', 'KPJ10001', 'KPC10001', '2019-05-03 08:10:46', '2019-05-03 00:00:00', 'A-002', '0', '', '', '0', '2019-05-03 08:10:46', '0'),
('PD20190503100000011', '3573011903940006', '0', 'KPJ10001', 'KPC10001', '2019-05-03 08:11:35', '2019-05-03 00:00:00', 'A-002', '0', '', '', '0', '2019-05-03 08:11:35', '0'),
('PD20190503100000012', '3573011903940006', '0', 'KPJ10001', 'KPC10001', '2019-05-03 08:12:05', '2019-05-03 00:00:00', 'A-002', '0', '', '', '0', '2019-05-03 08:12:05', '0'),
('PD20190503100000013', '3573011903940006', '0', 'KPJ10001', 'KPC10001', '2019-05-03 08:12:54', '2019-05-03 00:00:00', 'A-002', '0', '', '', '0', '2019-05-03 08:12:54', '0'),
('PD20190503100000014', '3573011903940006', '0', 'KPJ10001', 'KPC10001', '2019-05-03 08:13:21', '2019-05-03 00:00:00', 'A-002', '0', '', '', '0', '2019-05-03 08:13:21', '0'),
('PD20190503100000015', '3573011903940006', '0', 'KPJ10001', 'KPC10001', '2019-05-03 08:22:26', '2019-05-03 00:00:00', 'A-002', '0', '', '', '0', '2019-05-03 08:22:26', '0'),
('PD20190503100000016', '3573011903940006', '0', 'KPJ10001', 'KPC10001', '2019-05-03 08:22:55', '2019-05-03 00:00:00', 'A-002', '0', '', '', '0', '2019-05-03 08:22:55', '0'),
('PD20190503100000017', '3573011903940006', '0', 'KPJ10001', 'KPC10001', '2019-05-03 08:23:18', '2019-05-03 00:00:00', 'A-002', '0', '', '', '0', '2019-05-03 08:23:18', '0'),
('PD20190503100000018', '3573011903940006', '0', 'KPJ10001', 'KPC10001', '2019-05-03 08:25:29', '2019-05-03 00:00:00', 'A-002', '0', '', '', '0', '2019-05-03 08:25:29', '0'),
('PD20190503100000019', '3573011903940006', '0', 'KPJ10001', 'KPC10001', '2019-05-03 08:25:54', '2019-05-03 00:00:00', 'A-002', '0', '', '', '0', '2019-05-03 08:25:54', '0'),
('PD20190503100000020', '3573011903940006', '0', 'KPJ10001', 'KPC10001', '2019-05-03 08:31:44', '2019-05-03 00:00:00', 'A-002', '0', '', '', '0', '2019-05-03 08:31:44', '0'),
('PD20190506100000001', '3573011903940006', '0', 'KPJ10001', 'KPC10001', '2019-05-06 05:25:22', '2019-05-06 00:00:00', 'A-002', '0', '', '', '0', '2019-05-06 05:25:22', '0');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kependudukan_jenis`
--

CREATE TABLE `kependudukan_jenis` (
  `id_jenis` varchar(8) NOT NULL,
  `ket_jenis` text NOT NULL,
  `foto_jenis` varchar(70) NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `waktu` datetime NOT NULL,
  `id_admin` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kependudukan_jenis`
--

INSERT INTO `kependudukan_jenis` (`id_jenis`, `ket_jenis`, `foto_jenis`, `is_delete`, `waktu`, `id_admin`) VALUES
('KPJ10001', 'Kematian', 'f5a947d14485004b9af99c6ae699cd36aa01d9429cd97ca9235f49c16a2e9bb5.jpg', '0', '2019-04-29 07:14:46', 'AD2019020001'),
('KPJ10002', 'Kematian', 'c1887f6856a874b80daf339597e748df1ddf7df9b35f32648b794257333990b0.jpg', '0', '2019-04-29 07:14:54', 'AD2019020001'),
('KPJ10003', 'Kematian', 'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855.jpg', '0', '2019-04-29 07:12:18', 'AD2019020001'),
('KPJ10004', 'perceraian', 'a853833515f259419d39493197156c46634599ce8d65d88c2f65b634f5e890fb.jpg', '0', '2019-04-29 07:15:40', 'AD2019020001'),
('KPJ10005', 'pindah datang', '5adf937d088955a45e377cca1eeb3f22f6bf6a98ef0d6441914aeda712d97856.jpg', '0', '2019-04-29 07:15:54', 'AD2019020001'),
('KPJ10006', 'perpindahan', '6611484c096c23acfcb2a441887171f5b62668a4cd4510ebce01366bb23bfcd4.jpg', '0', '2019-04-29 07:16:20', 'AD2019020001'),
('KPJ10007', 'donal', 'bd0d411b5ed5d23e326df94f71723c27d1a2785a248aa30f54725f2115986454.jpg', '1', '2019-04-29 07:16:36', 'AD2019020001');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kependudukan_kategori`
--

CREATE TABLE `kependudukan_kategori` (
  `id_kategori` varchar(8) NOT NULL,
  `id_jenis` varchar(8) NOT NULL,
  `ket_kategori` text NOT NULL,
  `foto_kategori` varchar(70) NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `waktu` datetime NOT NULL,
  `id_admin` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kependudukan_kategori`
--

INSERT INTO `kependudukan_kategori` (`id_kategori`, `id_jenis`, `ket_kategori`, `foto_kategori`, `is_delete`, `waktu`, `id_admin`) VALUES
('KPC10001', 'KPJ10001', 'baru', '3079daa67b41e5fe307aece27914a28a01e1af6106679dd660f95cc32a59e2c3.jpg', '0', '2019-04-29 07:00:52', 'AD2019020001'),
('KPC10002', 'KPJ10001', 'perbaikan', '15fb79b3b82d6dc7783b356b91a6374b9e6a3dbed4e268e745c76b1c370916d2.jpg', '0', '2019-04-29 07:02:08', 'AD2019020001'),
('KPC10003', 'KPJ10002', 'baru', '37453faee4b7ccdbe84bb49b4faf19a2536f978ee4834583953b9360f529de6a.jpg', '0', '2019-04-29 07:02:21', 'AD2019020001'),
('KPC10004', 'KPJ10002', 'perbaikan', '416eb5cffdcc5b1d134be61c8928621ffbc28cc32d468b57f7beb390f7787bdf.jpg', '0', '2019-04-29 07:02:40', 'AD2019020001');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kesehatan_antrian`
--

CREATE TABLE `kesehatan_antrian` (
  `id_antrian` varchar(30) NOT NULL,
  `nik` varchar(16) NOT NULL,
  `id_layanan_menu` varchar(1) NOT NULL,
  `id_jenis_kesehatan` varchar(8) NOT NULL,
  `id_rs` varchar(15) NOT NULL,
  `id_poli` varchar(11) NOT NULL,
  `wkt_pendaftaran` datetime NOT NULL,
  `wkt_booking` datetime NOT NULL,
  `no_booking` varchar(5) NOT NULL,
  `status_antrian` enum('0','1','2','3') NOT NULL,
  `admin_acc` varchar(12) NOT NULL,
  `text_antrian` text NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `time_update` datetime NOT NULL,
  `id_admin` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kesehatan_antrian`
--

INSERT INTO `kesehatan_antrian` (`id_antrian`, `nik`, `id_layanan_menu`, `id_jenis_kesehatan`, `id_rs`, `id_poli`, `wkt_pendaftaran`, `wkt_booking`, `no_booking`, `status_antrian`, `admin_acc`, `text_antrian`, `is_delete`, `time_update`, `id_admin`) VALUES
('AC20190412100000001', '3573011903940006', '0', '1', 'RS2019032710003', 'P10001', '2019-04-12 05:02:17', '2019-04-12 00:00:00', 'A-002', '0', '', '', '0', '2019-04-12 05:02:17', ''),
('AC20190412100000002', '3573011903940006', '0', '1', 'RS2019032710003', 'P10001', '2019-04-12 05:02:57', '2019-04-15 00:00:00', 'A-002', '0', '', '', '0', '2019-04-12 05:02:57', ''),
('AC20190418100000001', '3573011903940006', '0', '1', 'RS2019032710002', 'P10001', '2019-04-18 09:09:11', '2019-04-18 00:00:00', 'A-002', '0', '', '', '0', '2019-04-18 09:09:11', ''),
('AC20190418100000002', '3573011903940006', '0', '1', 'RS2019032710002', 'P10001', '2019-04-18 09:13:18', '2019-04-18 00:00:00', 'A-002', '0', '', '', '0', '2019-04-18 09:13:18', ''),
('AC20190418100000003', '3573011903940006', '0', '1', 'RS2019032710002', 'P10001', '2019-04-18 09:39:34', '2019-04-18 00:00:00', 'A-002', '0', '', '', '0', '2019-04-18 09:39:34', ''),
('AC20190418100000004', '3573011903940006', '0', '1', 'RS2019032710002', 'P10001', '2019-04-18 09:51:36', '2019-04-18 00:00:00', 'A-002', '0', '', '', '0', '2019-04-18 09:51:36', ''),
('AC20190418100000005', '3573011903940006', '0', '1', 'RS2019032710002', 'P10001', '2019-04-18 09:51:39', '2019-04-18 00:00:00', 'A-002', '0', '', '', '0', '2019-04-18 09:51:39', ''),
('AC20190418100000006', '3573011903940006', '0', '1', 'RS2019032710002', 'P10001', '2019-04-18 09:54:52', '2019-04-18 00:00:00', 'A-002', '0', '', '', '0', '2019-04-18 09:54:52', ''),
('AC20190418100000007', '3573011903940006', '0', '1', 'RS2019032710002', 'P10001', '2019-04-18 09:55:07', '2019-04-18 00:00:00', 'A-002', '0', '', '', '0', '2019-04-18 09:55:07', ''),
('AC20190418100000008', '3573011903940006', '0', '1', 'RS2019032710002', 'P10001', '2019-04-18 09:55:16', '2019-04-18 00:00:00', 'A-002', '0', '', '', '0', '2019-04-18 09:55:16', ''),
('AC20190418100000009', '3573011903940006', '0', '1', 'RS2019032710002', 'P10001', '2019-04-18 09:55:23', '2019-04-18 00:00:00', 'A-002', '0', '', '', '0', '2019-04-18 09:55:23', ''),
('AC20190418100000010', '3573011903940006', '0', '1', 'RS2019032710002', 'P10001', '2019-04-18 09:55:33', '2019-04-18 00:00:00', 'A-002', '0', '', '', '0', '2019-04-18 09:55:33', ''),
('AC20190418100000011', '3573011903940006', '0', '1', 'RS2019032710002', 'P10001', '2019-04-18 09:55:50', '2019-04-18 00:00:00', 'A-002', '0', '', '', '0', '2019-04-18 09:55:50', ''),
('AC20190418100000012', '3573011903940006', '0', '1', 'RS2019032710002', 'P10001', '2019-04-18 09:56:11', '2019-04-18 00:00:00', 'A-002', '0', '', '', '0', '2019-04-18 09:56:11', ''),
('AC20190418100000013', '3573011903940006', '0', '1', 'RS2019032710002', 'P10001', '2019-04-18 09:56:33', '2019-04-18 00:00:00', 'A-002', '0', '', '', '0', '2019-04-18 09:56:33', ''),
('AC20190418100000014', '3573011903940006', '0', '1', 'RS2019032710002', 'P10001', '2019-04-18 09:57:57', '2019-04-18 00:00:00', 'A-002', '0', '', '', '0', '2019-04-18 09:57:57', ''),
('AC20190418100000015', '3573011903940006', '0', '1', 'RS2019032710002', 'P10001', '2019-04-18 09:58:16', '2019-04-18 00:00:00', 'A-002', '0', '', '', '0', '2019-04-18 09:58:16', ''),
('AC20190418100000016', '3573011903940006', '0', '1', 'RS2019032710002', 'P10001', '2019-04-18 09:58:30', '2019-04-18 00:00:00', 'A-002', '0', '', '', '0', '2019-04-18 09:58:30', ''),
('AC20190418100000017', '3573011903940006', '0', '1', 'RS2019032710002', 'P10001', '2019-04-18 09:58:43', '2019-04-18 00:00:00', 'A-002', '0', '', '', '0', '2019-04-18 09:58:43', ''),
('AC20190418100000018', '3573011903940006', '0', '1', 'RS2019032710002', 'P10001', '2019-04-18 10:00:58', '2019-04-18 00:00:00', 'A-002', '0', '', '', '0', '2019-04-18 10:00:58', ''),
('AC20190418100000019', '3573011903940006', '0', '1', 'RS2019032710002', 'P10001', '2019-04-18 10:01:08', '2019-04-18 00:00:00', 'A-002', '0', '', '', '0', '2019-04-18 10:01:08', ''),
('AC20190418100000020', '3573011903940006', '0', '1', 'RS2019032710002', 'P10001', '2019-04-18 10:06:15', '2019-04-18 00:00:00', 'A-002', '0', '', '', '0', '2019-04-18 10:06:15', ''),
('AC20190418100000021', '3573011903940006', '0', '1', 'RS2019032710002', 'P10001', '2019-04-18 10:06:40', '2019-04-18 00:00:00', 'A-002', '0', '', '', '0', '2019-04-18 10:06:40', ''),
('AC20190418100000022', '3573011903940006', '0', '1', 'RS2019032710002', 'P10001', '2019-04-18 10:09:28', '2019-04-18 00:00:00', 'A-002', '0', '', '', '0', '2019-04-18 10:09:28', ''),
('AC20190418100000023', '3573011903940006', '0', '1', 'RS2019032710002', 'P10001', '2019-04-18 10:41:06', '2019-04-18 00:00:00', 'A-002', '0', '', '', '0', '2019-04-18 10:41:06', ''),
('AC20190418100000024', '3573011903940006', '0', '1', 'RS2019032710002', 'P10001', '2019-04-18 10:41:29', '2019-04-18 00:00:00', 'A-002', '0', '', '', '0', '2019-04-18 10:41:29', ''),
('AC20190418100000025', '3573011903940006', '0', '1', 'RS2019032710002', 'P10001', '2019-04-18 10:49:49', '2019-04-18 00:00:00', 'A-002', '0', '', '', '0', '2019-04-18 10:49:49', ''),
('AC20190422100000001', '3573011903940006', '0', '1', 'RS2019032710002', 'P10001', '2019-04-22 08:17:13', '2019-04-22 00:00:00', 'A-002', '0', '', '', '0', '2019-04-22 08:17:13', ''),
('AC20190422100000002', '3573011903940006', '0', '1', 'RS2019032710002', 'P10001', '2019-04-22 08:19:37', '2019-04-22 00:00:00', 'A-002', '0', '', '', '0', '2019-04-22 08:19:37', ''),
('AC20190422100000003', '3573011903940006', '0', '1', 'RS2019032710002', 'P10001', '2019-04-22 08:21:38', '2019-04-22 00:00:00', 'A-002', '0', '', '', '0', '2019-04-22 08:21:38', ''),
('AC20190422100000004', '3573011903940006', '0', '1', 'RS2019032710002', 'P10001', '2019-04-22 08:22:34', '2019-04-22 00:00:00', 'A-002', '0', '', '', '0', '2019-04-22 08:22:34', ''),
('AC20190422100000005', '3573011903940006', '0', '1', 'RS2019032710002', 'P10001', '2019-04-22 08:23:41', '2019-04-22 00:00:00', 'A-002', '0', '', '', '0', '2019-04-22 08:23:41', ''),
('AC20190422100000006', '3573011903940006', '0', '1', 'RS2019032710002', 'P10001', '2019-04-22 08:38:53', '2019-04-22 00:00:00', 'A-002', '0', '', '', '0', '2019-04-22 08:38:53', ''),
('AC20190422100000007', '3573011903940006', '0', '1', 'RS2019032710001', 'P10001', '2019-04-22 08:39:46', '2019-04-22 00:00:00', 'A-002', '0', '', '', '0', '2019-04-22 08:39:46', ''),
('AC20190422100000008', '3573011903940006', '0', '1', 'RS2019032710002', 'P10001', '2019-04-22 09:24:47', '2019-04-22 00:00:00', 'A-002', '0', '', '', '0', '2019-04-22 09:24:47', ''),
('AC20190422100000009', '3573011903940006', '0', '1', 'RS2019032710001', 'P10001', '2019-04-22 10:18:32', '0000-00-00 00:00:00', 'A-002', '0', '', '', '0', '2019-04-22 10:18:32', ''),
('AC20190422100000010', '3573011903940006', '0', '1', 'RS2019032710001', 'P10001', '2019-04-22 10:18:32', '0000-00-00 00:00:00', 'A-002', '0', '', '', '0', '2019-04-22 10:18:32', ''),
('AC20190422100000011', '3573011903940006', '0', '1', 'RS2019032710001', 'P10001', '2019-04-22 10:24:16', '0000-00-00 00:00:00', 'A-002', '0', '', '', '0', '2019-04-22 10:24:16', ''),
('AC20190422100000012', '3573011903940006', '0', '1', 'RS2019032710001', 'P10001', '2019-04-22 10:28:50', '0000-00-00 00:00:00', 'A-002', '0', '', '', '0', '2019-04-22 10:28:50', ''),
('AC20190422100000013', '3573011903940006', '0', '1', 'RS2019032710002', 'P10001', '2019-04-22 10:32:44', '2019-04-22 00:00:00', 'A-002', '0', '', '', '0', '2019-04-22 10:32:44', ''),
('AC20190422100000014', '3573011903940006', '0', '1', 'RS2019032710001', 'P10001', '2019-04-22 10:33:23', '0000-00-00 00:00:00', 'A-002', '0', '', '', '0', '2019-04-22 10:33:23', ''),
('AC20190503100000001', '3573011903940006', '0', '1', 'RS2019032710002', 'P10001', '2019-05-03 08:00:50', '2019-05-03 00:00:00', 'A-002', '0', '', '', '0', '2019-05-03 08:00:50', ''),
('AC20190503100000002', '3573011903940006', '0', '1', 'RS2019032710002', 'P10001', '2019-05-03 08:01:12', '2019-05-03 00:00:00', 'A-002', '0', '', '', '0', '2019-05-03 08:01:12', ''),
('AC20190506100000001', '3573011903940006', '0', '1', 'RS2019032710002', 'P10001', '2019-05-06 05:24:27', '2019-05-07 00:00:00', 'A-002', '0', '', '', '0', '2019-05-06 05:24:27', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kesehatan_jenis`
--

CREATE TABLE `kesehatan_jenis` (
  `id_layanan` varchar(8) NOT NULL,
  `nama_layanan` text NOT NULL,
  `foto` varchar(70) NOT NULL,
  `is_delete` enum('0','1') DEFAULT NULL,
  `waktu` datetime DEFAULT NULL,
  `id_admin` varchar(12) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kesehatan_jenis`
--

INSERT INTO `kesehatan_jenis` (`id_layanan`, `nama_layanan`, `foto`, `is_delete`, `waktu`, `id_admin`) VALUES
('KSJ10001', 'rumah sakit umum', '41e216aa43a641cac7d611b84323e37dfd67043b28a6d0d50fc16fbd46b189b6.jpg', '0', '2019-03-25 08:53:55', 'AD2019020001'),
('KSJ10002', 'rumah sakit ibu dan anak', '1483bcd0544c8299806b216c188f730ef9e187c076d69c75248f2dce221f8f28.jpg', '0', '2019-03-25 08:51:17', 'AD2019020001'),
('KSJ10003', 'rumah sakit islam', 'b990492b26b993f77bab8fae398eb0909b2ac41e1c942c9e2d7786871ef3c4ce.jpg', '0', '2019-03-25 08:51:42', 'AD2019020001'),
('KSJ10004', 'rumah sakit bersalin', '4a607a6fb0c8c71ca148b9377b0912a4d5f5e6ed9ed6995439e236f7f4cc8d1d.jpg', '0', '2019-03-25 08:52:54', 'AD2019020001'),
('KSJ10005', 'klinik', '113cbc2d33df0f661807223a4533f00de21d8b7e6b2034bc787b26a6ac3dcc68.jpg', '0', '2019-03-25 08:53:06', 'AD2019020001'),
('KSJ10006', 'puskesmas', 'f52d513956d3b4bbbf1b198a74dd7b9fa8e1ef9e52dc0166e86455ecadad3bf1.jpg', '0', '2019-03-25 08:53:20', 'AD2019020001'),
('KSJ10007', 'test 1', 'c25f2a7fafaa546cf1da84ac792f033b9fb1c0deea1ac3d6340055b0405e629c.jpg', '1', '2019-03-25 08:57:04', 'AD2019020001'),
('KSJ10008', 'test 2', '048ef36684ed06d8c259d24a37ac70aefb0eae1dea84482c686cfea246b482c5.jpg', '1', '2019-03-25 08:57:00', 'AD2019020001'),
('KSJ10009', 'sadsad', '', '0', '2019-05-07 05:17:40', 'AD2019020001');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kesehatan_poli`
--

CREATE TABLE `kesehatan_poli` (
  `id_poli` varchar(11) NOT NULL,
  `nama_poli` varchar(30) NOT NULL,
  `foto` varchar(70) NOT NULL,
  `is_delete` enum('0','1') DEFAULT NULL,
  `waktu` datetime DEFAULT NULL,
  `id_admin` varchar(12) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kesehatan_poli`
--

INSERT INTO `kesehatan_poli` (`id_poli`, `nama_poli`, `foto`, `is_delete`, `waktu`, `id_admin`) VALUES
('P10001', 'mata', '931085c2eaa6b8e4d8f49226d3177ec1522cec30262d0b99bb5d28b7fff0edb5.jpg', '0', '2019-03-25 05:33:43', 'AD2019020001'),
('P10002', 'gigi', '50059ad385a3bd7b793c72d11735b8d0d63ff93e88d284b1c55104006fef17bc.jpg', '0', '2019-03-25 05:33:36', 'AD2019020001'),
('P10003', 'penyakit dalam', '5f36c0d7291d99d01226f8fe60574df9ebb62b97fbe3f6e05848d6ac28f72bab.jpg', '0', '2019-03-25 03:07:55', 'AD2019020001'),
('P10004', 'paru-paru', '6a118570fc84e88aebb381ec8b27ae7d7348efc32220c3c4783e82c8fc89a5f4.jpg', '0', '2019-03-25 05:32:28', 'AD2019020001'),
('P10005', 'jantung', '2ab596154ea6d2f891ccd586291f50849447152e97071737baed6b815136015a.jpg', '0', '2019-03-25 05:32:36', 'AD2019020001'),
('P10006', 'bedah khusus', '2fbc38462a94c056d78ff07e23916c04615c60e90b07153b46c14d527ba92606.jpg', '0', '2019-03-25 05:32:55', 'AD2019020001'),
('P10007', 'bedah ringan', 'bc0a539247035b47948776452eebf9cfbd26b93719677204b6c7f571093181a1.jpg', '0', '2019-03-25 05:33:06', 'AD2019020001');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kesehatan_rs`
--

CREATE TABLE `kesehatan_rs` (
  `id_rs` varchar(15) NOT NULL,
  `nama_rumah_sakit` text NOT NULL,
  `alamat` text NOT NULL,
  `foto_rs` varchar(70) DEFAULT NULL,
  `telepon` varchar(13) NOT NULL,
  `id_layanan` varchar(8) NOT NULL,
  `id_poli` text NOT NULL,
  `is_delete` enum('0','1') DEFAULT NULL,
  `waktu` datetime DEFAULT NULL,
  `id_admin` varchar(12) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kesehatan_rs`
--

INSERT INTO `kesehatan_rs` (`id_rs`, `nama_rumah_sakit`, `alamat`, `foto_rs`, `telepon`, `id_layanan`, `id_poli`, `is_delete`, `waktu`, `id_admin`) VALUES
('RS2019032710001', 'RSJ', 'malang', '82361e1c39f6b3fd75aa97b9dbe470af7ad1a0d81cb1e5afc28df7091ad67076.jpg', '085841920243', 'KSJ10002', '[\'P10001\',\'P10003\',\'P10006\']', '0', '2019-04-15 09:56:09', 'AD2019020001'),
('RS2019032710002', 'panti nirmala', 'malang', '0788dcb56780f8d06164aa6635a3e500b9f4b37ea790b6de9454fea8a9f62ca0.jpg', '085841920243', 'KSJ10001', '[\'P10001\',\'P10002\',\'P10003\',\'P10005\',\'P10006\',\'P10007\']', '0', '2019-04-15 09:54:58', 'AD2019020001'),
('RS2019032710003', 'Rumah Sakit Saiful Anwar', 'malang', '576ef93d7b0f65f037da189f569e938045c26f753d1b42c3a1fbc61bb0ecea64.jpg', '085841920243', 'KSJ10001', '[\'P10001\',\'P10002\',\'P10003\',\'P10005\',\'P10006\',\'P10007\']', '1', '2019-04-15 10:59:13', 'AD2019020001'),
('RS2019032710004', 'Rumah Sakit Lavalete malang', 'malang', 'b12e37dc1ad28043eea7cbaab0d24ef82210017b58baa0ff81e5721994694879.jpg', '085841920243', 'KSJ10001', '[\'P10001\',\'P10003\',\'P10005\',\'P10007\']', '0', '2019-04-15 10:50:10', 'AD2019020001'),
('RS2019032710005', 'Rumah Sakit Saiful Anwar ', 'malang', '2522405e2d190fa08cb58b6f989941cbad42e0ce4143eba42d9fe392d720fee1.jpg', '085841920243', 'KSJ10001', '[\'P10001\',\'P10003\',\'P10005\',\'P10007\']', '0', '2019-04-15 10:50:42', 'AD2019020001'),
('RS2019032710006', 'Rumah Sakti Lavalete malangsa', 'malang', 'e44c711b36f1fa21b433a5494829a7dab4fd4e1032b19f6348eaf586f76adb00.jpg', '085841920243', 'KSJ10001', '[\'P10001\',\'P10003\',\'P10005\',\'P10007\']', '1', '2019-03-27 09:30:15', 'AD2019020001'),
('RS2019032710007', 'Rumah Sakit Daerah Aisiyah', 'Malang', 'cd933da191a29d2df0f2f26afcac8c3445b81bd38191f81b2689ab367e40b5b1.jpg', '085841920243', 'KSJ10001', '[\'P10001\',\'P10002\',\'P10003\',\'P10004\',\'P10005\',\'P10006\']', '1', '2019-03-27 09:30:02', 'AD2019020001'),
('RS2019041510001', 'Rumah Sakit Saiful Anwar Malang', 'malang', 'b81184675053bd523fec65d76759f16979b0b1735b0b761b9068e929bd0ab5f4.jpg', '034712532', 'KSJ10001', '[\'P10001\',\'P10002\',\'P10004\',\'P10005\']', '0', '2019-04-15 11:00:13', 'AD2019020001');

-- --------------------------------------------------------

--
-- Struktur dari tabel `master_kecamatan`
--

CREATE TABLE `master_kecamatan` (
  `id_kecamatan` varchar(7) NOT NULL,
  `nama_kecamatan` varchar(64) NOT NULL,
  `area` text NOT NULL,
  `luas` double NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `waktu` datetime NOT NULL,
  `id_admin` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `master_kecamatan`
--

INSERT INTO `master_kecamatan` (`id_kecamatan`, `nama_kecamatan`, `area`, `luas`, `is_delete`, `waktu`, `id_admin`) VALUES
('KEC1001', 'Kedungkandang', 'null', 39.87, '0', '0000-00-00 00:00:00', ''),
('KEC1002', 'Sukun', 'null', 20.97, '0', '0000-00-00 00:00:00', ''),
('KEC1003', 'Klojen', 'null', 8.83, '0', '0000-00-00 00:00:00', ''),
('KEC1004', 'Blimbing', 'null', 17.77, '0', '0000-00-00 00:00:00', ''),
('KEC1005', 'Lowokwaru', 'null', 22.6, '0', '0000-00-00 00:00:00', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `master_kelurahan`
--

CREATE TABLE `master_kelurahan` (
  `id_kelurahan` varchar(7) NOT NULL,
  `id_kecamatan` varchar(7) NOT NULL,
  `nama_kelurahan` varchar(64) NOT NULL,
  `area` text NOT NULL,
  `luas_kel` double NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `waktu` datetime NOT NULL,
  `id_admin` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `master_kelurahan`
--

INSERT INTO `master_kelurahan` (`id_kelurahan`, `id_kecamatan`, `nama_kelurahan`, `area`, `luas_kel`, `is_delete`, `waktu`, `id_admin`) VALUES
('KEL1001', 'KEC1001', 'Bumiayu', '0', 12, '0', '0000-00-00 00:00:00', ''),
('KEL1002', 'KEC1001', 'Mergosono', '12', 12, '0', '0000-00-00 00:00:00', ''),
('KEL1003', 'KEC1001', 'Kotalama', '12', 12, '0', '0000-00-00 00:00:00', ''),
('KEL1004', 'KEC1001', 'Wonokoyo', '12', 12, '0', '0000-00-00 00:00:00', ''),
('KEL1005', 'KEC1001', 'Buring', '12', 12, '0', '0000-00-00 00:00:00', ''),
('KEL1006', 'KEC1001', 'Kedungkandang', '12', 12, '0', '0000-00-00 00:00:00', ''),
('KEL1007', 'KEC1001', 'Lesanpuro', '12', 12, '0', '0000-00-00 00:00:00', ''),
('KEL1008', 'KEC1001', 'Sawojajar', '12', 12, '0', '0000-00-00 00:00:00', ''),
('KEL1009', 'KEC1001', 'Madyopuro', '12', 12, '0', '0000-00-00 00:00:00', ''),
('KEL1010', 'KEC1001', 'Cemorokandang', '12', 12, '0', '0000-00-00 00:00:00', ''),
('KEL1011', 'KEC1001', 'Arjowinangun', '12', 12, '0', '0000-00-00 00:00:00', ''),
('KEL1012', 'KEC1001', 'Tlogowaru', '12', 12, '0', '0000-00-00 00:00:00', ''),
('KEL1013', 'KEC1002', 'Ciptomulyo', '12', 12, '0', '0000-00-00 00:00:00', ''),
('KEL1014', 'KEC1002', 'Gadang', '12', 12, '0', '0000-00-00 00:00:00', ''),
('KEL1015', 'KEC1002', 'Kebonsari', '12', 12, '0', '0000-00-00 00:00:00', ''),
('KEL1016', 'KEC1002', 'Bandungrejosari', '12', 12, '0', '0000-00-00 00:00:00', ''),
('KEL1017', 'KEC1002', 'Sukun', '12', 12, '0', '0000-00-00 00:00:00', ''),
('KEL1018', 'KEC1005', 'Tunggulwulung', '1', 12, '0', '0000-00-00 00:00:00', ''),
('KEL1019', 'KEC1005', 'Merjosari', '1', 1, '0', '0000-00-00 00:00:00', ''),
('KEL1020', 'KEC1005', 'Tlogomas', '1', 1, '0', '0000-00-00 00:00:00', ''),
('KEL1021', 'KEC1003', 'Klojen', '1', 1, '0', '0000-00-00 00:00:00', ''),
('KEL1022', 'KEC1003', 'Samaan', '1', 1, '0', '0000-00-00 00:00:00', ''),
('KEL1023', 'KEC1003', 'Rampalcelaket', '1', 1, '0', '0000-00-00 00:00:00', ''),
('KEL1024', 'KEC1004', 'Balearjosari', '1', 1, '0', '0000-00-00 00:00:00', ''),
('KEL1025', 'KEC1004', 'Arjosari', '1', 1, '0', '0000-00-00 00:00:00', ''),
('KEL1026', 'KEC1004', 'Polowijen', '1', 1, '0', '0000-00-00 00:00:00', ''),
('KEL1027', 'KEC1001', 'pandanwangis', '0', 0, '0', '0000-00-00 00:00:00', 'AD2019020001');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pendidikan_jenis`
--

CREATE TABLE `pendidikan_jenis` (
  `id_jenis` varchar(10) NOT NULL,
  `id_strata` varchar(8) NOT NULL,
  `nama_jenis` text NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `waktu` datetime NOT NULL,
  `id_admin` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pendidikan_jenis`
--

INSERT INTO `pendidikan_jenis` (`id_jenis`, `id_strata`, `nama_jenis`, `is_delete`, `waktu`, `id_admin`) VALUES
('PDJ0110001', 'PDST1002', 'SD Negeri', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('PDJ0110002', 'PDST1001', 'SD Swasta', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('PDJ0110003', 'PDST1002', 'surya', '1', '2019-05-07 05:55:43', ''),
('PDJ0110004', 'PDST1003', 'susah ya', '1', '2019-05-07 05:55:40', ''),
('PDJ0110005', 'PDST1004', 'dinda', '1', '2019-05-07 05:55:36', 'AD2019020001');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pendidikan_sekolah`
--

CREATE TABLE `pendidikan_sekolah` (
  `id_sekolah` varchar(17) NOT NULL,
  `id_jenis` varchar(10) NOT NULL,
  `id_kecamatan` varchar(7) NOT NULL,
  `id_kelurahan` varchar(7) NOT NULL,
  `nama_sekolah` text NOT NULL,
  `foto_sklh` text NOT NULL,
  `lokasi` varchar(32) NOT NULL,
  `detail_sekolah` text NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `waktu` datetime NOT NULL,
  `id_admin` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pendidikan_sekolah`
--

INSERT INTO `pendidikan_sekolah` (`id_sekolah`, `id_jenis`, `id_kecamatan`, `id_kelurahan`, `nama_sekolah`, `foto_sklh`, `lokasi`, `detail_sekolah`, `is_delete`, `waktu`, `id_admin`) VALUES
('SKL20190429100001', 'PDJ0110001', 'KEC1003', '0', 'SDN KLOJEN', '0', '[\'-7.970732\', \'112.632759\']', '{\'alamat\': \'JL. Kalpataru Malang\',\'url\': \'sdnkalpataru.sch.id\',\'tlp\': \'0341256741\'}', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190429100002', 'PDJ0110001', 'KEC1003', '0', 'SDN Kiduldalem 1', '0', '[\'-7.978529\', \'112.633037\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190429100003', 'PDJ0110001', 'KEC1003', '0', 'SDN Kiduldalem 2', '0', '[\'-7.979886\', \'112.635053\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190429100004', 'PDJ0110001', 'KEC1003', '0', 'SDN Kauman 1', '0', '[\'-7.984757\', \'112.630161\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190429100005', 'PDJ0110001', 'KEC1003', '0', 'SDN Kauman 2', '0', '[\'-7.978359\', \'112.624812\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190429100006', 'PDJ0110001', 'KEC1003', '0', 'SDN Kauman 3', '0', '[\'-7.983679\', \'112.628547\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190429100007', 'PDJ0110001', 'KEC1003', '0', 'SDN Kasin', '0', '[\'-7.985550\', \'112.625451\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190429100008', 'PDJ0110001', 'KEC1003', '0', 'SDN Sukoharjo 1', '0', '[\'-7.990147\', \'112.633196\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190429100009', 'PDJ0110001', 'KEC1003', '0', 'SDN Sukoharjo 2', '0', '[\'-7.989915\', \'112.632845\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190429100010', 'PDJ0110001', 'KEC1003', '0', 'SDN Bareng 1', '0', '[\'-7.980727\', \'112.625345\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190429100011', 'PDJ0110001', 'KEC1003', '0', 'SDN Bareng 2', '0', '[\'-7.979924\', \'112.621778\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190429100012', 'PDJ0110001', 'KEC1003', '0', 'SDN Bareng 3', '0', '[\'-7.975719\', \'112.618941\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100001', 'PDJ0110001', 'KEC1003', '0', 'SDN Bareng 4', '0', '[\'-7.976788\', \'112.617087\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100002', 'PDJ0110001', 'KEC1003', '0', 'SDN Bareng 5', '0', '[\'-7.977995\', \'112.622715\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100003', 'PDJ0110001', 'KEC1004', '0', 'SDN Balearjosari 1', '0', '[\'-7.923529\', \'112.651103\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100004', 'PDJ0110001', 'KEC1004', '0', 'SDN Balearjosari 2', '0', '[\'-7.926450\', \'112.657830\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100005', 'PDJ0110001', 'KEC1004', '0', 'SDN Polowijen 1', '0', '[\'-7.929054\', \'112.648380\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100006', 'PDJ0110001', 'KEC1004', '0', 'SDN Polowijen 2', '0', '[\'-7.929044\', \'112.642837\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100007', 'PDJ0110001', 'KEC1004', '0', 'SDN Polowijen 3', '0', '[\'-7.928972\', \'112.645164\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100008', 'PDJ0110001', 'KEC1004', '0', 'SDN Arjosari 1', '0', '[\'-7.922440\', \'112.651512\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100009', 'PDJ0110001', 'KEC1004', '0', 'SDN Arjosari 2', '0', '[\'-7.930654\', \'112.658624\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100010', 'PDJ0110001', 'KEC1004', '0', 'SDN Arjosari 3', '0', '[\'-7.927625\', \'112.654032\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100011', 'PDJ0110001', 'KEC1004', '0', 'SDN Purwodadi 1', '0', '[\'-7.932690\', \'112.646803\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100012', 'PDJ0110001', 'KEC1004', '0', 'SDN Purwodadi 2', '0', '[\'-7.9382737\', \' 112.6464189\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100013', 'PDJ0110001', 'KEC1004', '0', 'SDN Purwodadi 3', '0', '[\'-7.9384437 \', \'112.6469653\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100014', 'PDJ0110001', 'KEC1001', '0', 'SDN Kedungkandang 1', '0', '[\'-7.989953 \', \'112.650530\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100015', 'PDJ0110001', 'KEC1001', '0', 'SDN Kedungkandang 2', '0', '[\'-7.99115 \', \'112.647413\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100016', 'PDJ0110001', 'KEC1001', '0', 'SDN Lesanpuro 1', '0', '[\'-7.984983 \', \'112.657145\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100017', 'PDJ0110001', 'KEC1001', '0', 'SDN Lesanpuro 2', '0', '[\'-7.991327 \', \'112.664996\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100018', 'PDJ0110001', 'KEC1001', '0', 'SDN Lesanpuro 3', '0', '[\'-7.981281 \', \'112.660270\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100019', 'PDJ0110001', 'KEC1001', '0', 'SDN Buring', '0', '[\'-8.005362 \', \'112.644048\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100020', 'PDJ0110001', 'KEC1001', '0', 'SDN Wonokoyo 1', '0', '[\'-8.021992 \', \'112.648034\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100021', 'PDJ0110001', 'KEC1001', '0', 'SDN Wonokoyo 2', '0', '[\'-8.023499 \', \'112.669706\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100022', 'PDJ0110001', 'KEC1001', '0', 'SDN Tlogowaru 1', '0', '[\'-8.039433 \', \'112.650648\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100023', 'PDJ0110001', 'KEC1001', '0', 'SDN Tlogowaru 2', '0', '[\'-8.037279 \', \'112.666295\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100024', 'PDJ0110001', 'KEC1005', '0', 'SDN Lowokwaru 1', '0', '[\'-7.956957 \', \'112.635960\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100025', 'PDJ0110001', 'KEC1005', '0', 'SDN Lowokwaru 2', '0', '[\'-7.962162 \', \'112.632506\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100026', 'PDJ0110001', 'KEC1005', '0', 'SDN Lowokwaru 3', '0', '[\'-7.961433 \', \'112.634560\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100027', 'PDJ0110001', 'KEC1005', '0', 'SDN Lowokwaru 4', '0', '[\'-7.952290 \', \'112.632080\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100028', 'PDJ0110001', 'KEC1005', '0', 'SDN Lowokwaru 5', '0', '[\'-7.956750 \', \'112.625608\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100029', 'PDJ0110001', 'KEC1005', '0', 'SDN Tulusrejo 1', '0', '[\'-7.951383 \', \'112.633635\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100030', 'PDJ0110001', 'KEC1005', '0', 'SDN Tulusrejo 2', '0', '[\'-7.944885 \', \' 112.626397\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100031', 'PDJ0110001', 'KEC1005', '0', 'SDN Tulusrejo 3', '0', '[\'-7.946951 \', \' 112.635431\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100032', 'PDJ0110001', 'KEC1005', '0', 'SDN Tulusrejo 4', '0', '[\'-7.951106 \', \' 112.634715\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100033', 'PDJ0110001', 'KEC1005', '0', 'SDN Jatimulyo', '0', '[\'-7.942121 \', \' 112.616067\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100034', 'PDJ0110001', 'KEC1002', '0', 'SDN Sukun 1', '0', '[\'-7.994275 \', \' 112.620484\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100035', 'PDJ0110001', 'KEC1002', '0', 'SDN Sukun 2', '0', '[\'-7.991602 \', \' 112.616819\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100036', 'PDJ0110001', 'KEC1002', '0', 'SDN Sukun 3', '0', '[\'-7.991235 \', \' 112.620182\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100037', 'PDJ0110001', 'KEC1002', '0', 'SDN Bandungrejosari 1', '0', '[\'-8.008017 \', \' 112.618806\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100038', 'PDJ0110001', 'KEC1002', '0', 'SDN Bandungrejosari 2', '0', '[\'-8.001061 \', \' 112.614058\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pendidikan_strata`
--

CREATE TABLE `pendidikan_strata` (
  `id_strata` varchar(8) NOT NULL,
  `nama_strata` text NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `waktu` datetime NOT NULL,
  `id_admin` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pendidikan_strata`
--

INSERT INTO `pendidikan_strata` (`id_strata`, `nama_strata`, `is_delete`, `waktu`, `id_admin`) VALUES
('PDST1001', 'Taman Kanak-Kanak (TK)', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('PDST1002', 'Sekolah Dasar (SD)', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('PDST1003', 'Sekolah Menengah Pertama (SMP)', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('PDST1004', 'Universitas', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('PDST1005', 'bagus', '1', '2019-05-07 09:16:45', 'AD2019020001'),
('PDST1006', 'mas', '1', '2019-05-07 09:16:39', 'AD2019020001'),
('PDST1007', 'jhonathan', '0', '2019-05-07 09:13:18', 'AD2019020001');

-- --------------------------------------------------------

--
-- Struktur dari tabel `table1`
--

CREATE TABLE `table1` (
  `id` varchar(7) NOT NULL DEFAULT '0',
  `name` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `table1`
--

INSERT INTO `table1` (`id`, `name`) VALUES
('LHPL001', 'joni'),
('LHPL002', 'joni'),
('LHPL003', 'joni'),
('LHPL004', 'joni'),
('LHPL005', 'joni'),
('LHPL006', 'joni'),
('LHPL007', 'joni'),
('LHPL008', 'joni'),
('LHPL009', 'joni'),
('LHPL010', 'joni'),
('LHPL011', 'joni'),
('LHPL012', 'joni'),
('LHPL013', 'joni'),
('LHPL014', 'joni'),
('LHPL015', 'joni'),
('LHPL016', 'joni'),
('LHPL017', 'joni'),
('LHPL018', 'joni'),
('LHPL019', 'joni'),
('LHPL020', 'joni'),
('LHPL021', 'joni'),
('LHPL022', 'joni'),
('LHPL023', 'joni'),
('LHPL024', 'joni'),
('LHPL025', 'joni'),
('LHPL026', 'joni'),
('LHPL027', 'joni'),
('LHPL028', 'joni'),
('LHPL029', 'joni'),
('LHPL030', 'joni'),
('LHPL031', 'joni'),
('LHPL032', 'joni'),
('LHPL033', 'joni'),
('LHPL034', 'joni'),
('LHPL035', 'joni'),
('LHPL036', 'joni'),
('LHPL037', 'joni'),
('LHPL038', 'joni'),
('LHPL039', 'joni'),
('LHPL040', 'joni'),
('LHPL041', 'joni'),
('LHPL042', 'joni'),
('LHPL043', 'joni'),
('LHPL044', 'joni'),
('LHPL045', 'joni'),
('LHPL046', 'joni'),
('LHPL047', 'joni'),
('LHPL048', 'joni'),
('LHPL049', 'joni'),
('LHPL050', 'joni'),
('LHPL051', 'joni'),
('LHPL052', 'joni'),
('LHPL053', 'joni'),
('LHPL054', 'joni'),
('LHPL055', 'joni'),
('LHPL056', 'joni'),
('LHPL057', 'joni'),
('LHPL058', 'joni'),
('LHPL059', 'joni'),
('LHPL060', 'joni'),
('LHPL061', 'joni'),
('LHPL062', 'joni'),
('LHPL063', 'joni'),
('LHPL064', 'joni'),
('LHPL065', 'joni'),
('LHPL066', 'joni'),
('LHPL067', 'joni'),
('LHPL068', 'joni'),
('LHPL069', 'joni'),
('LHPL070', 'joni'),
('LHPL071', 'joni'),
('LHPL072', 'joni'),
('LHPL073', 'joni'),
('LHPL074', 'joni'),
('LHPL075', 'joni'),
('LHPL076', 'joni'),
('LHPL077', 'joni'),
('LHPL078', 'joni'),
('LHPL079', 'joni'),
('LHPL080', 'joni'),
('LHPL081', 'joni'),
('LHPL082', 'joni'),
('LHPL083', 'joni'),
('LHPL084', 'joni'),
('LHPL085', 'joni'),
('LHPL086', 'joni'),
('LHPL087', 'joni'),
('LHPL088', 'joni'),
('LHPL089', 'joni'),
('LHPL090', 'joni'),
('LHPL091', 'joni'),
('LHPL092', 'joni'),
('LHPL093', 'joni'),
('LHPL094', 'joni'),
('LHPL095', 'joni'),
('LHPL096', 'joni'),
('LHPL097', 'joni'),
('LHPL098', 'joni'),
('LHPL099', 'joni'),
('LHPL100', 'joni'),
('LHPL101', 'joni'),
('LHPL102', 'joni'),
('LHPL103', 'joni'),
('LHPL104', 'joni'),
('LHPL105', 'joni'),
('LHPL106', 'joni'),
('LHPL107', 'joni'),
('LHPL108', 'joni'),
('LHPL109', 'joni'),
('LHPL110', 'joni'),
('LHPL111', 'joni'),
('LHPL112', 'joni'),
('LHPL113', 'joni'),
('LHPL114', 'joni'),
('LHPL115', 'joni'),
('LHPL116', 'joni'),
('LHPL117', 'joni'),
('LHPL118', 'joni'),
('LHPL119', 'joni'),
('LHPL120', 'joni'),
('LHPL121', 'joni'),
('LHPL122', 'joni'),
('LHPL123', 'joni'),
('LHPL124', 'joni'),
('LHPL125', 'joni'),
('LHPL126', 'joni'),
('LHPL127', 'joni'),
('LHPL128', 'joni'),
('LHPL129', 'joni'),
('LHPL130', 'joni'),
('LHPL131', 'joni'),
('LHPL132', 'joni'),
('LHPL133', 'joni'),
('LHPL134', 'joni'),
('LHPL135', 'joni'),
('LHPL136', 'joni'),
('LHPL137', 'joni'),
('LHPL138', 'joni'),
('LHPL139', 'joni'),
('LHPL140', 'joni'),
('LHPL141', 'joni'),
('LHPL142', 'joni'),
('LHPL143', 'joni'),
('LHPL144', 'joni'),
('LHPL145', 'joni'),
('LHPL146', 'joni'),
('LHPL147', 'joni'),
('LHPL148', 'joni'),
('LHPL149', 'joni'),
('LHPL150', 'joni'),
('LHPL151', 'joni'),
('LHPL152', 'joni'),
('LHPL153', 'joni'),
('LHPL154', 'joni'),
('LHPL155', 'joni'),
('LHPL156', 'joni'),
('LHPL157', 'joni'),
('LHPL158', 'joni'),
('LHPL159', 'joni'),
('LHPL160', 'joni'),
('LHPL161', 'joni'),
('LHPL162', 'joni'),
('LHPL163', 'joni'),
('LHPL164', 'joni'),
('LHPL165', 'joni'),
('LHPL166', 'joni'),
('LHPL167', 'joni'),
('LHPL168', 'joni'),
('LHPL169', 'joni'),
('LHPL170', 'joni'),
('LHPL171', 'joni'),
('LHPL172', 'joni'),
('LHPL173', 'joni'),
('LHPL174', 'joni'),
('LHPL175', 'joni'),
('LHPL176', 'joni'),
('LHPL177', 'joni'),
('LHPL178', 'joni'),
('LHPL179', 'joni'),
('LHPL180', 'joni'),
('LHPL181', 'joni'),
('LHPL182', 'joni'),
('LHPL183', 'joni'),
('LHPL184', 'joni'),
('LHPL185', 'joni'),
('LHPL186', 'joni'),
('LHPL187', 'joni'),
('LHPL188', 'joni'),
('LHPL189', 'joni'),
('LHPL190', 'joni'),
('LHPL191', 'joni'),
('LHPL192', 'joni'),
('LHPL193', 'joni'),
('LHPL194', 'joni'),
('LHPL195', 'joni'),
('LHPL196', 'joni'),
('LHPL197', 'joni'),
('LHPL198', 'joni'),
('LHPL199', 'joni'),
('LHPL200', 'joni'),
('LHPL201', 'joni'),
('LHPL202', 'joni'),
('LHPL203', 'joni'),
('LHPL204', 'joni'),
('LHPL205', 'joni'),
('LHPL206', 'joni'),
('LHPL207', 'joni'),
('LHPL208', 'joni'),
('LHPL209', 'joni'),
('LHPL210', 'joni'),
('LHPL211', 'joni'),
('LHPL212', 'joni'),
('LHPL213', 'joni'),
('LHPL214', 'joni'),
('LHPL215', 'joni'),
('LHPL216', 'joni'),
('LHPL217', 'joni'),
('LHPL218', 'joni'),
('LHPL219', 'joni'),
('LHPL220', 'joni'),
('LHPL221', 'joni'),
('LHPL222', 'joni'),
('LHPL223', 'joni'),
('LHPL224', 'joni'),
('LHPL225', 'joni'),
('LHPL226', 'joni'),
('LHPL227', 'joni'),
('LHPL228', 'joni'),
('LHPL229', 'joni'),
('LHPL230', 'joni'),
('LHPL231', 'joni'),
('LHPL232', 'joni'),
('LHPL233', 'joni'),
('LHPL234', 'joni'),
('LHPL235', 'joni'),
('LHPL236', 'joni'),
('LHPL237', 'joni'),
('LHPL238', 'joni'),
('LHPL239', 'joni'),
('LHPL240', 'joni'),
('LHPL241', 'joni'),
('LHPL242', 'joni'),
('LHPL243', 'joni'),
('LHPL244', 'joni'),
('LHPL245', 'joni'),
('LHPL246', 'joni'),
('LHPL247', 'joni'),
('LHPL248', 'joni'),
('LHPL249', 'joni'),
('LHPL250', 'joni'),
('LHPL251', 'joni'),
('LHPL252', 'joni'),
('LHPL253', 'joni'),
('LHPL254', 'joni'),
('LHPL255', 'joni'),
('LHPL256', 'joni'),
('LHPL257', 'joni'),
('LHPL258', 'joni'),
('LHPL259', 'joni'),
('LHPL260', 'joni'),
('LHPL261', 'joni'),
('LHPL262', 'joni'),
('LHPL263', 'joni'),
('LHPL264', 'joni'),
('LHPL265', 'joni'),
('LHPL266', 'joni'),
('LHPL267', 'joni'),
('LHPL268', 'joni'),
('LHPL269', 'joni'),
('LHPL270', 'joni'),
('LHPL271', 'joni'),
('LHPL272', 'joni'),
('LHPL273', 'joni'),
('LHPL274', 'joni'),
('LHPL275', 'joni'),
('LHPL276', 'joni'),
('LHPL277', 'joni'),
('LHPL278', 'joni'),
('LHPL279', 'joni'),
('LHPL280', 'joni'),
('LHPL281', 'joni'),
('LHPL282', 'joni'),
('LHPL283', 'joni'),
('LHPL284', 'joni'),
('LHPL285', 'joni'),
('LHPL286', 'joni'),
('LHPL287', 'joni'),
('LHPL288', 'joni'),
('LHPL289', 'joni'),
('LHPL290', 'joni'),
('LHPL291', 'joni'),
('LHPL292', 'joni'),
('LHPL293', 'joni'),
('LHPL294', 'joni'),
('LHPL295', 'joni'),
('LHPL296', 'joni'),
('LHPL297', 'joni'),
('LHPL298', 'joni'),
('LHPL299', 'joni'),
('LHPL300', 'joni'),
('LHPL301', 'joni'),
('LHPL302', 'joni'),
('LHPL303', 'joni'),
('LHPL304', 'joni'),
('LHPL305', 'joni'),
('LHPL306', 'joni'),
('LHPL307', 'joni'),
('LHPL308', 'joni'),
('LHPL309', 'joni'),
('LHPL310', 'joni'),
('LHPL311', 'joni'),
('LHPL312', 'joni'),
('LHPL313', 'joni'),
('LHPL314', 'joni'),
('LHPL315', 'joni'),
('LHPL316', 'joni'),
('LHPL317', 'joni'),
('LHPL318', 'joni'),
('LHPL319', 'joni'),
('LHPL320', 'joni'),
('LHPL321', 'joni'),
('LHPL322', 'joni'),
('LHPL323', 'joni'),
('LHPL324', 'joni'),
('LHPL325', 'joni'),
('LHPL326', 'joni'),
('LHPL327', 'joni'),
('LHPL328', 'joni'),
('LHPL329', 'joni'),
('LHPL330', 'joni'),
('LHPL331', 'joni'),
('LHPL332', 'joni'),
('LHPL333', 'joni'),
('LHPL334', 'joni'),
('LHPL335', 'joni'),
('LHPL336', 'joni'),
('LHPL337', 'joni'),
('LHPL338', 'joni'),
('LHPL339', 'joni'),
('LHPL340', 'joni'),
('LHPL341', 'joni'),
('LHPL342', 'joni'),
('LHPL343', 'joni'),
('LHPL344', 'joni'),
('LHPL345', 'joni'),
('LHPL346', 'joni'),
('LHPL347', 'joni'),
('LHPL348', 'joni'),
('LHPL349', 'joni'),
('LHPL350', 'joni'),
('LHPL351', 'joni'),
('LHPL352', 'joni'),
('LHPL353', 'joni'),
('LHPL354', 'joni'),
('LHPL355', 'joni'),
('LHPL356', 'joni'),
('LHPL357', 'joni'),
('LHPL358', 'joni'),
('LHPL359', 'joni'),
('LHPL360', 'joni'),
('LHPL361', 'joni'),
('LHPL362', 'joni'),
('LHPL363', 'joni'),
('LHPL364', 'joni'),
('LHPL365', 'joni'),
('LHPL366', 'joni'),
('LHPL367', 'joni'),
('LHPL368', 'joni'),
('LHPL369', 'joni'),
('LHPL370', 'joni'),
('LHPL371', 'joni'),
('LHPL372', 'joni'),
('LHPL373', 'joni'),
('LHPL374', 'joni'),
('LHPL375', 'joni'),
('LHPL376', 'joni'),
('LHPL377', 'joni'),
('LHPL378', 'joni'),
('LHPL379', 'joni'),
('LHPL380', 'joni'),
('LHPL381', 'joni'),
('LHPL382', 'joni'),
('LHPL383', 'joni'),
('LHPL384', 'joni'),
('LHPL385', 'joni'),
('LHPL386', 'joni'),
('LHPL387', 'joni'),
('LHPL388', 'joni'),
('LHPL389', 'joni'),
('LHPL390', 'joni'),
('LHPL391', 'joni'),
('LHPL392', 'joni'),
('LHPL393', 'joni'),
('LHPL394', 'joni'),
('LHPL395', 'joni'),
('LHPL396', 'joni'),
('LHPL397', 'joni'),
('LHPL398', 'joni'),
('LHPL399', 'joni'),
('LHPL400', 'joni'),
('LHPL401', 'joni'),
('LHPL402', 'joni'),
('LHPL403', 'joni'),
('LHPL404', 'joni'),
('LHPL405', 'joni'),
('LHPL406', 'joni');

--
-- Trigger `table1`
--
DELIMITER $$
CREATE TRIGGER `tg_table1_insert` BEFORE INSERT ON `table1` FOR EACH ROW BEGIN
  declare last_key_item varchar(20);
  declare count_row_item int;
  declare fix_key_item varchar(12);
  
  select count(*) into count_row_item from table1;
  
  select id into last_key_item from table1 order by id desc limit 1;
  
  
  if(count_row_item <1) then
  	set fix_key_item = concat("LHPL","001");
  else
    set fix_key_item = concat('LHPL', LPAD(RIGHT(last_key_item, 3)+1, 3, '0'));
      
  END IF;
  
  SET NEW.id = fix_key_item;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `table1_seq`
--

CREATE TABLE `table1_seq` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `table1_seq`
--

INSERT INTO `table1_seq` (`id`) VALUES
(1),
(2),
(3),
(4),
(5),
(6),
(7),
(8),
(9),
(10),
(11),
(12),
(13),
(14),
(15),
(16),
(17),
(18),
(19),
(20),
(21),
(22),
(23),
(24),
(25),
(26),
(27),
(28),
(29),
(30),
(31),
(32),
(33),
(34),
(35),
(36),
(37),
(38),
(39),
(40),
(41),
(42),
(43),
(44),
(45),
(46),
(47),
(48),
(49),
(50),
(51),
(52),
(53),
(54),
(55),
(56),
(57),
(58),
(59),
(60),
(61),
(62),
(63),
(64),
(65),
(66),
(67),
(68),
(69),
(70),
(71),
(72),
(73),
(74),
(75),
(76),
(77),
(78),
(79),
(80),
(81),
(82),
(83),
(84),
(85),
(86),
(87),
(88),
(89),
(90),
(91),
(92),
(93),
(94),
(95),
(96),
(97),
(98),
(99),
(100),
(101),
(102),
(103),
(104),
(105),
(106),
(107),
(108),
(109),
(110),
(111),
(112),
(113),
(114),
(115),
(116),
(117),
(118),
(119),
(120),
(121),
(122),
(123),
(124),
(125),
(126),
(127),
(128),
(129),
(130),
(131),
(132),
(133),
(134),
(135),
(136),
(137),
(138),
(139),
(140),
(141),
(142),
(143),
(144),
(145),
(146),
(147),
(148),
(149),
(150),
(151),
(152);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `admin_lv`
--
ALTER TABLE `admin_lv`
  ADD PRIMARY KEY (`id_lv`);

--
-- Indexes for table `device`
--
ALTER TABLE `device`
  ADD PRIMARY KEY (`id_device_kios`);

--
-- Indexes for table `dinas`
--
ALTER TABLE `dinas`
  ADD PRIMARY KEY (`id_dinas`);

--
-- Indexes for table `home_page_kategori`
--
ALTER TABLE `home_page_kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `home_page_main`
--
ALTER TABLE `home_page_main`
  ADD PRIMARY KEY (`id_page`);

--
-- Indexes for table `ijin_antrian`
--
ALTER TABLE `ijin_antrian`
  ADD PRIMARY KEY (`id_antrian`);

--
-- Indexes for table `ijin_jenis`
--
ALTER TABLE `ijin_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `ijin_kategori`
--
ALTER TABLE `ijin_kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `ijin_sub_kategori`
--
ALTER TABLE `ijin_sub_kategori`
  ADD PRIMARY KEY (`id_sub`);

--
-- Indexes for table `kependudukan_antrian`
--
ALTER TABLE `kependudukan_antrian`
  ADD PRIMARY KEY (`id_antrian`);

--
-- Indexes for table `kependudukan_jenis`
--
ALTER TABLE `kependudukan_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `kependudukan_kategori`
--
ALTER TABLE `kependudukan_kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `kesehatan_antrian`
--
ALTER TABLE `kesehatan_antrian`
  ADD PRIMARY KEY (`id_antrian`);

--
-- Indexes for table `kesehatan_jenis`
--
ALTER TABLE `kesehatan_jenis`
  ADD PRIMARY KEY (`id_layanan`);

--
-- Indexes for table `kesehatan_poli`
--
ALTER TABLE `kesehatan_poli`
  ADD PRIMARY KEY (`id_poli`);

--
-- Indexes for table `kesehatan_rs`
--
ALTER TABLE `kesehatan_rs`
  ADD PRIMARY KEY (`id_rs`);

--
-- Indexes for table `master_kecamatan`
--
ALTER TABLE `master_kecamatan`
  ADD PRIMARY KEY (`id_kecamatan`);

--
-- Indexes for table `master_kelurahan`
--
ALTER TABLE `master_kelurahan`
  ADD PRIMARY KEY (`id_kelurahan`);

--
-- Indexes for table `pendidikan_jenis`
--
ALTER TABLE `pendidikan_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `pendidikan_sekolah`
--
ALTER TABLE `pendidikan_sekolah`
  ADD PRIMARY KEY (`id_sekolah`);

--
-- Indexes for table `pendidikan_strata`
--
ALTER TABLE `pendidikan_strata`
  ADD PRIMARY KEY (`id_strata`);

--
-- Indexes for table `table1`
--
ALTER TABLE `table1`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `table1_seq`
--
ALTER TABLE `table1_seq`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_lv`
--
ALTER TABLE `admin_lv`
  MODIFY `id_lv` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `home_page_kategori`
--
ALTER TABLE `home_page_kategori`
  MODIFY `id_kategori` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `table1_seq`
--
ALTER TABLE `table1_seq`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=153;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
