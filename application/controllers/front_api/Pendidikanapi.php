<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Pendidikanapi extends CI_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->model("main/mainmodel", "mm");
		$this->load->model("pendidikan_main", "pm");
		
        $this->load->library("encrypt");

		$this->load->library("get_identity");
		$this->load->library("response_message");
	}
    

#=================================================================================================#
#-------------------------------------------index_penddidikan_sekolah-----------------------------#
#=================================================================================================#
    public function index_pendidikan_home($id_strata){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        $msg_detail = array();

        $data_send = array();
        $kec = $this->mm->get_data_all_where("master_kecamatan", array("is_delete"=>"0"));

        $no = 0;
        foreach ($kec as $key => $value) {
            $data_send[$no]["kec"]["id_kec"]    = $this->encrypt->encode($value->id_kecamatan);
            $data_send[$no]["kec"]["nama_kec"]  = $value->nama_kecamatan;

            $data_send[$no]["item"] = $this->pm->get_sch_api(array("ps.id_kecamatan"=>$value->id_kecamatan, "sha2(pj.id_strata, \"256\")="=> $id_strata, "ps.is_delete"=>"0"));
            // $data_send[$no]["item"] = $this->pm->get_sch_api(array("ps.id_kecamatan"=>$value->id_kecamatan, "ps.is_delete"=>"0"));

            $msg_detail["item"] = $data_send;
            $msg_detail["url_core"] = base_url()."assets/core_img/icon_menu_jenis/";
            $no++;
        }

        
        if($msg_detail["item"]){
            $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
        }

        $msg_array = $this->response_message->default_mgs($msg_main,$msg_detail);
        print_r(json_encode($msg_array));
    }
#=================================================================================================#
#-------------------------------------------index_penddidikan_sekolah-----------------------------#
#=================================================================================================#

#=================================================================================================#
#-------------------------------------------index_penddidikan_detail------------------------------#
#=================================================================================================#
    public function get_sekolah_detail($id_sekolah){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        $msg_detail = array();

        $data_send = $this->pm->get_detail_sch_api(array("sha2(ps.id_sekolah, \"256\")="=> $id_sekolah, "ps.is_delete"=>"0"));

        $msg_detail["item"] = $data_send;
        $msg_detail["url_core"] = base_url()."assets/core_img/icon_menu_jenis/";
                
        if($msg_detail["item"]){
            $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
        }

        $msg_array = $this->response_message->default_mgs($msg_main,$msg_detail);
        print_r(json_encode($msg_array));
    }
#=================================================================================================#
#-------------------------------------------index_penddidikan_detail------------------------------#
#=================================================================================================#



#=================================================================================================#
#-------------------------------------------index_penddidikan_univ--------------------------------#
#=================================================================================================#
    public function index_pendidikan_univ(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        $msg_detail = array();

        $data_send = array();
        $kec = $this->mm->get_data_all_where("master_kecamatan", array("is_delete"=>"0"));

        $no = 0;
        foreach ($kec as $key => $value) {
            $data_send[$no]["kec"]["id_kec"]    = $this->encrypt->encode($value->id_kecamatan);
            $data_send[$no]["kec"]["nama_kec"]  = $value->nama_kecamatan;

            $data_send[$no]["item"] = $this->pm->get_sch_api(array("ps.id_kecamatan"=>$value->id_kecamatan, "pj.id_strata"=> "PDST1002", "ps.is_delete"=>"0"));
            // $data_send[$no]["item"] = $this->pm->get_sch_api(array("ps.id_kecamatan"=>$value->id_kecamatan, "ps.is_delete"=>"0"));

            $msg_detail["item"] = $data_send;
            $msg_detail["url_core"] = base_url()."assets/core_img/icon_menu_jenis/";
            $no++;
        }

        
        if($msg_detail["item"]){
            $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
        }

        $msg_array = $this->response_message->default_mgs($msg_main,$msg_detail);
        print_r(json_encode($msg_array));
    }
#=================================================================================================#
#-------------------------------------------index_penddidikan_univ--------------------------------#
#=================================================================================================#

    public function cek(){
        echo hash("sha256", "SKL20190429100001");
    }

}
?>