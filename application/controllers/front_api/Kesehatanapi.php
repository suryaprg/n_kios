<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Kesehatanapi extends CI_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->model("main/mainmodel", "mm");
		$this->load->model("Kesehatan_main", "ks");
		
        $this->load->library("encrypt");

		$this->load->library("get_identity");
		$this->load->library("response_message");
	}
    

#=================================================================================================#
#-------------------------------------------Kategori_rs-------------------------------------------#
#=================================================================================================#
    private function validate_post_get_kesehatan(){
        $config_val_input = array(
                array(
                    'field'=>'id_layanan',
                    'label'=>'Id',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function get_data_kesehatan_jenis(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        $msg_detail = array("id_layanan"=>""); 

        if($this->validate_post_get_kesehatan()){
            $id_layanan = $this->input->post("id_layanan");
            if($id_layanan == "1"){
                $data = $this->ks->get_kesehatan_layanan_api(array("id_layanan"=>$id_layanan));

                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
                $msg_detail["id_layanan"] = $this->encrypt->encode($id_layanan);
                $msg_detail["item"] = $data;
                $msg_detail["url_core"] = base_url()."assets/core_img/icon_menu_jenis/";
            }
        }

        $msg_array = $this->response_message->default_mgs($msg_main,$msg_detail);
        print_r(json_encode($msg_array));
    }
#=================================================================================================#
#-------------------------------------------Kategori_rs-------------------------------------------#
#=================================================================================================#


#=================================================================================================#
#-------------------------------------------Poli--------------------------------------------------#
#=================================================================================================#
    private function validate_post_get_kesehatan_poli(){
        $config_val_input = array(
                array(
                    'field'=>'id_rs',
                    'label'=>'Id',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function get_data_kesehatan_poli(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        $msg_detail = array("id_rs"=>""); 

        if($this->validate_post_get_kesehatan_poli()){
            $id_rs = $this->input->post("id_rs");
            $data_rs = $this->ks->get_rs_each(array("sha2(b.id_rs, 512)="=>$id_rs));

            if($data_rs){
                $list_poli = json_decode(str_replace("'", "\"", $data_rs["id_poli"]));
                $data_poli = $this->ks->get_kesehatan_poli_api($list_poli);
                if($data_poli){
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));

                    $data_tmp = array('id_rs' => $this->encrypt->encode($data_rs["id_rs"]),
                                        'id_jenis_rs' => $this->encrypt->encode($data_rs["id_layanan"]),
                                        'nama_rs' => $data_rs["nama_rumah_sakit"],
                                        'alamat' => $data_rs["alamat"],
                                        'telepon' => $data_rs["telepon"]
                                    );
                    
                    $msg_detail["id_rs"]= $data_tmp;

                    $msg_detail["item"] = $data_poli;
                    // $msg_detail["data_rs"] = $data_rs;
                    $msg_detail["url_core"] = base_url()."assets/core_img/icon_menu/";
                }
            }

            // print_r($data_rs);
        }

        $msg_array = $this->response_message->default_mgs($msg_main,$msg_detail);
        print_r(json_encode($msg_array));
    }
#=================================================================================================#
#-------------------------------------------Poli--------------------------------------------------#
#=================================================================================================#


#=================================================================================================#
#-------------------------------------------Rumah_Sakit-------------------------------------------#
#=================================================================================================#
    private function validate_post_get_kesehatan_rs(){
        $config_val_input = array(
                array(
                    'field'=>'id_layanan',
                    'label'=>'Id',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function get_data_kesehatan_rs(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        $msg_detail = array("id_layanan"=>""); 

        if($this->validate_post_get_kesehatan()){
            $id_layanan = $this->input->post("id_layanan");
            $data = $this->ks->get_kesehatan_rs_api(array("sha2(b.id_layanan, 512)="=>$id_layanan));
            $data_jenis_rs = $this->mm->get_data_each("kesehatan_jenis", array("sha2(id_layanan, 512)="=>$id_layanan));

            $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
            $msg_detail["id_layanan"] = array("id_jenis_rs"=>$this->encrypt->encode($data_jenis_rs["id_layanan"]),
                                                "nama_layanan"=>$data_jenis_rs["nama_layanan"]);
            $msg_detail["item"] = $data;
            $msg_detail["url_core"] = base_url()."assets/core_img/icon_menu_rs/";
        }

        $msg_array = $this->response_message->default_mgs($msg_main,$msg_detail);
        print_r(json_encode($msg_array));
    }
#=================================================================================================#
#-------------------------------------------Rumah_Sakit-------------------------------------------#
#=================================================================================================#

#=================================================================================================#
#-------------------------------------------insert_antrian----------------------------------------#
#=================================================================================================#
    private function validate_insert_antrian(){
        $config_val_input = array(
                array(
                    'field'=>'nik',
                    'label'=>'Nomor Induk Kependudukan',
                    'rules'=>'required|exact_length[16]|numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'exact_length'=>"%s 16 ".$this->response_message->get_error_msg("REQUIRED"),
                        'numeric'=>"%s n ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),array(
                    'field'=>'nama',
                    'label'=>'Nama Anda',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),array(
                    'field'=>'waktu',
                    'label'=>'Tanggal Pendaftaran',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),array(
                    'field'=>'id_poli',
                    'label'=>'id',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),array(
                    'field'=>'id_layanan',
                    'label'=>'id_layanan',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),array(
                    'field'=>'id_rs',
                    'label'=>'id_rs',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),array(
                    'field'=>'ip_lan',
                    'label'=>'ip_lan',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),array(
                    'field'=>'ip_public',
                    'label'=>'ip_public',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function insert_antrian(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        $msg_detail = array("nik"=>"","nama"=>"","waktu"=>"","id_layanan"=>"","id_rs"=>"","id_poli"=>"",); 

        if($this->validate_insert_antrian()){

            // print_r("<pre>");
            // print_r($_POST);
            $nik    = $this->input->post("nik");
            $nama   = $this->input->post("nama");
            $waktu  = $this->input->post("waktu");
            
            $id_rs      = $this->input->post("id_rs");
            $id_layanan = $this->input->post("id_layanan");
            $id_poli    = $this->input->post("id_poli");

            $ip_lan     = $this->input->post("ip_lan");
            $ip_public  = $this->input->post("ip_public");

            $time_add = date("Y-m-d H:i:s");

            #----------get_identity--------------
            $msg_detail["data_response"]["data_identity"] = array(
                                                                "nik"=>$nik,
                                                                "nama"=>$nama,
                                                                "time_add"=>$time_add,
                                                                "time_book"=>$waktu
                                                            );

            #----------get_rs--------------
            $data_rs = $this->mm->get_data_each("kesehatan_rs", array("id_rs"=>$this->encrypt->decode($id_rs)));
            $msg_detail["data_response"]["data_rs"] = array("id_rs"=>$this->encrypt->encode($data_rs["id_rs"]), 
                                                            "nama_rumah_sakit"=>$data_rs["nama_rumah_sakit"],
                                                            "alamat"=>$data_rs["alamat"]);

            #----------get_layanan---------
            $data_layanan = $this->mm->get_data_each("kesehatan_jenis", array("id_layanan"=>$this->encrypt->decode($id_layanan)));
            $msg_detail["data_response"]["data_layanan"] = array("id_layanan"=>$this->encrypt->encode($data_layanan["id_layanan"]), 
                                                            "nama_layanan"=>$data_layanan["nama_layanan"]);

                            
            #----------get_poli------------
            $data_poli = $this->mm->get_data_each("kesehatan_poli", array("sha2(id_poli, 512)="=>$id_poli));
            $msg_detail["data_response"]["data_poli"] = array("id_poli"=>$this->encrypt->encode($data_poli["id_poli"]), 
                                                            "nama_poli"=>$data_poli["nama_poli"]);

            #----------set_ip--------------
            $msg_detail["data_response"]["set_ip"] = array("ip_public"=>$ip_public, 
                                                            "ip_lan"=>$ip_lan);

            
            #----------date_daftar---------
                        if($waktu > date("Y-m-d") || $waktu == date("Y-m-d")){
            #----------cek_device_antrian_register------
                            $where_device = array("ip_lan"=>$ip_lan, "ip_public"=>$ip_public);
                            $check_device = $this->mm->get_data_each("device", $where_device);
                            if($check_device){
                                $count_antrian = $this->mm->get_data_each("kesehatan_antrian", array());
            #----------cek_kuota_antrian_hari_ini-------
                                $insert = $this->db->query("SELECT insert_antrian('".$nik."',
                                                                            '".$data_layanan["id_layanan"]."',
                                                                            '1',
                                                                            '".$data_rs["id_rs"]."',
                                                                            '".$data_poli["id_poli"]."',
                                                                            '".$time_add."',
                                                                            '".$waktu."',
                                                                            '".$time_add."',
                                                                            '',
                                                                            'A-002') AS insert_antrian")->row_array();

                                $msg_detail["data_response"]["data_identity"]["no_antrean"] = 'A-002';
            #----------jika_tersedia_maka_insert-------- 
            #----------jika_tidak_maka_send_response_fail--------
            
                                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
                            }
                        }
                    
            
            #----------send_response-------
            
        }else {
            $msg_detail["nik"]      = strip_tags(form_error("nik"));
            $msg_detail["nama"]     = strip_tags(form_error("nama"));
            $msg_detail["waktu"]    = strip_tags(form_error("waktu"));
            $msg_detail["id_layanan"] = strip_tags(form_error("id_layanan"));
            $msg_detail["id_rs"]    = strip_tags(form_error("id_rs"));
            $msg_detail["id_poli"]  = strip_tags(form_error("id_poli"));

            $msg_detail["ip_lan"]    = strip_tags(form_error("ip_lan"));
            $msg_detail["ip_public"]  = strip_tags(form_error("ip_public"));
        }

        $msg_array = $this->response_message->default_mgs($msg_main,$msg_detail);
        print_r(json_encode($msg_array));
    }
#=================================================================================================#
#-------------------------------------------insert_antrian----------------------------------------#
#=================================================================================================#


}
?>