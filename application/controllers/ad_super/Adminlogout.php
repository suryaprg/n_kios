<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Adminlogout extends CI_Controller{

	public function __construct(){
		parent::__construct();
	}
    
    public function logout(){
        unset($_SESSION["admin_lv_1"]);
        redirect(base_url()."back-admin/login");
    }	
}
?>