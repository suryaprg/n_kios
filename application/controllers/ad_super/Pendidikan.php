<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Pendidikan extends CI_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->model("main/mainmodel", "mm");
		$this->load->model("Pendidikan_main", "pm");
		
        $this->load->library("encrypt");

		$this->load->library("get_identity");
		$this->load->library("file_upload");
		$this->load->library("response_message");
        $this->load->library("Jsoncheck");

        // $session = $this->session->userdata("admin_lv_1");
        // if(isset($session)){
        //     if($session["status_active"] != "1" and $session["is_log"] != "1"){
        //         redirect(base_url("back-admin/login"));
        //     }
        // }else{
        //     redirect(base_url("back-admin/login"));
        // }
	}

    public function un_link(){
        $config['upload_path']          = './assets/core_img/icon_menu_jenis/';
        $config['file_name']            = "2518ffca8740277c52d7f4f7e4ee55bc203a09eeb79e72fde94270e20f688047.jpg";
        if(file_exists($config['upload_path'].$config['file_name'])){
            unlink($config['upload_path'].$config['file_name']);    
        }

    }
    
#=================================================================================================#
#-------------------------------------------main_upload_file--------------------------------------#
#=================================================================================================#
    private function main_upload_file($config, $input_name){
        if(file_exists($config['upload_path'].$config['file_name'])){
            unlink($config['upload_path'].$config['file_name']);    
        }
        
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        $return_array = array("status"=>"",
                                "main_msg"=>"",
                                "main_data"=>"");
        
        if (!$this->upload->do_upload($input_name)){
            $return_array["status"] = false;
            $return_array["main_msg"] = array('error' => $this->upload->display_errors());
            $return_array["main_data"] = null;
        }else{
            $return_array["status"] = true;
            $return_array["main_msg"] = "upload success";
            $return_array["main_data"] = array('upload_data' => $this->upload->data());
        }

        return $return_array;
    }
#=================================================================================================#
#-------------------------------------------main_upload_file--------------------------------------#
#=================================================================================================#

#=================================================================================================#
#-------------------------------------------Strata------------------------------------------------#
#=================================================================================================#

    private function validate_input_strata(){
        $config_val_input = array(
                array(
                    'field'=>'nama_strata',
                    'label'=>'nama Strata',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function insert_strata(){
        $detail_msg = array("nama_strata"=>"");
        $main_msg = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));

        if($this->validate_input_strata()){
            $nama_strata = $this->input->post("nama_strata");
            $time_update = date("Y-m-d h:i:s");
            $id_admin = $this->encrypt->decode($this->session->userdata("admin_lv_1")["id_admin"]);

            $insert = $this->pm->insert_strata($nama_strata, $time_update, $id_admin);
            if($insert){
                $main_msg = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
            }            
        }else {
            $detail_msg["nama_strata"] = strip_tags(form_error("nama_strata"));
        }

        $msg_array = $this->response_message->default_mgs($main_msg,$detail_msg);
        print_r(json_encode($msg_array));
    }


    public function get_strata(){
        $id = $this->encrypt->decode($this->input->post("id_strata"));
        $data = $this->mm->get_data_each("pendidikan_strata",array("id_strata"=>$id));

        $data_json["status"] = false;
        $data_json["val_response"] = null;
        if(!empty($data)){
            $data_json["status"] = true;
            $data_json["val_response"] = array("id_strata"=>$this->encrypt->encode($data["id_strata"]),
                                                "nama_strata"=>$data["nama_strata"]);
        }

        print_r(json_encode($data_json));
    }

    public function update_strata(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPDATE_FAIL"));
        $msg_detail = array("nama_strata"=>"");

        if($this->validate_input_strata()){
            $id_strata = $this->encrypt->decode($this->input->post("id_strata"));
            $nama_strata = $this->input->post("nama_strata");

            $id_admin = $this->encrypt->decode($this->session->userdata("admin_lv_1")["id_admin"]);
            $time_update = date("Y-m-d h:i:s");

            $set = array(
                        "nama_strata"=>$nama_strata,
                        "waktu"=>$time_update,
                        "id_admin"=>$id_admin
                    );
            
            $where = array(
                "id_strata"=>$id_strata
            );

            if($this->mm->update_data("pendidikan_strata", $set, $where)){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
            }       
        }else{
            $msg_detail["nama_strata"] = strip_tags(form_error('nama_strata'));               
        }
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
    

    public function val_form_delete_strata(){
        $config_val_input = array(
                array(
                    'field'=>'id_strata',
                    'label'=>'id',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function delete_strata(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("DELETE_FAIL"));
        if($this->val_form_delete_strata()){
            $id_strata = $this->encrypt->decode($this->input->post("id_strata"));

            $is_del = "1";
            $time_del = date("Y-m-d h:i:s");

            $set = array(
                    "is_delete"=>$is_del,
                    "waktu"=>$time_del
                );

            $where = array("id_strata"=>$id_strata);

            if($this->mm->update_data("pendidikan_strata", $set, $where)){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
            }
        }

        $res_msg = $this->response_message->default_mgs($msg_main, "null");
        print_r(json_encode($res_msg));
    }  
#=================================================================================================#
#-------------------------------------------Strata------------------------------------------------#
#=================================================================================================#

#=================================================================================================#
#-------------------------------------------Jenis-------------------------------------------------#
#=================================================================================================#

    public function index_jenis(){
        $data["list_jenis"] = $this->pm->get_jenis(array("j.is_delete"=>"0"));
        $data["list_strata"] = $this->mm->get_data_all_where("pendidikan_strata", array("is_delete"=>"0"));
        
        $this->load->view("ad_super/header");
        $this->load->view("ad_super/pendidikan/pd_jenis", $data);
        $this->load->view("ad_super/footer");
    }

    private function validate_input_jenis(){
        $config_val_input = array(
                array(
                    'field'=>'nama_jenis',
                    'label'=>'nama_jenis',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),array(
                    'field'=>'strata',
                    'label'=>'strata',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function insert_jenis(){
        $detail_msg = array("nama_jenis"=>"", "strata"=>"");
        $main_msg = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));

        if($this->validate_input_jenis()){
            $nama_jenis = $this->input->post("nama_jenis");
            $strata = $this->input->post("strata");

            $time_update = date("Y-m-d h:i:s");
            $id_admin = $this->encrypt->decode($this->session->userdata("admin_lv_1")["id_admin"]);

            $insert = $this->pm->insert_jenis($strata, $nama_jenis, $time_update, $id_admin);
            if($insert){
                $main_msg = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
            }            
        }else {
            $detail_msg["nama_jenis"] = strip_tags(form_error("nama_jenis"));
            $detail_msg["strata"] = strip_tags(form_error("strata"));
        }
        $msg_array = $this->response_message->default_mgs($main_msg,$detail_msg);
        print_r(json_encode($msg_array));
    }


    public function get_jenis(){
        $id = $this->encrypt->decode($this->input->post("id_jenis"));
        $data = $this->mm->get_data_each("pendidikan_jenis",array("id_jenis"=>$id));

        $data_json["status"] = false;
        $data_json["val_response"] = null;
        if(!empty($data)){
            $data_json["status"] = true;
            $data_json["val_response"] = array("id_jenis"=>$this->encrypt->encode($data["id_jenis"]),
                                                "strata"=>$data["id_strata"],
                                                "nama_jenis"=>$data["nama_jenis"]);
        }

        // print_r($data_json);
        print_r(json_encode($data_json));
    }

    public function update_jenis(){
        $detail_msg = array("nama_jenis"=>"", "strata"=>"");
        $main_msg = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPDATE_FAIL"));

        if($this->validate_input_jenis()){
            $nama_jenis = $this->input->post("nama_jenis");
            $strata = $this->input->post("strata");
            $id_jenis = $this->encrypt->decode($this->input->post("id_jenis"));

            $time_update = date("Y-m-d h:i:s");
            $id_admin = $this->encrypt->decode($this->session->userdata("admin_lv_1")["id_admin"]);

            $data_send = array("nama_jenis"=>$nama_jenis, "id_strata"=>$strata);
            $where = array("id_jenis"=>$id_jenis);

            $update = $this->mm->update_data("pendidikan_jenis", $data_send, $where);
            if($update){
                $main_msg = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
            }            
        }else {
            $detail_msg["nama_jenis"] = strip_tags(form_error("nama_jenis"));
            $detail_msg["strata"] = strip_tags(form_error("strata"));
        }
        $msg_array = $this->response_message->default_mgs($main_msg,$detail_msg);
        print_r(json_encode($msg_array));
    }
    

    public function val_form_delete_jenis(){
        $config_val_input = array(
                array(
                    'field'=>'id_jenis',
                    'label'=>'id',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function delete_jenis(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("DELETE_FAIL"));
        if($this->val_form_delete_jenis()){
            $id_jenis = $this->encrypt->decode($this->input->post("id_jenis"));

            $is_del = "1";
            $time_del = date("Y-m-d h:i:s");

            $set = array(
                    "is_delete"=>$is_del,
                    "waktu"=>$time_del
                );

            $where = array("id_jenis"=>$id_jenis);

            if($this->mm->update_data("pendidikan_jenis", $set, $where)){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
            }
        }

        $res_msg = $this->response_message->default_mgs($msg_main, "null");
        print_r(json_encode($res_msg));
    }  
#=================================================================================================#
#-------------------------------------------Jenis-------------------------------------------------#
#=================================================================================================#

#=================================================================================================#
#-------------------------------------------sekolah-----------------------------------------------#
#=================================================================================================#
    public function index_sekolah(){
        $data["list_data_sekolah"]  = $this->pm->get_sch_all(array("ps.is_delete"=>"0"));
        $data["list_jenis"]         = $this->pm->get_jenis(array("j.is_delete"=>"0"));
        // $data["list_strata"]        = $this->mm->get_data_all_where("pendidikan_strata", array("is_delete"=>"0"));

        $data["list_kecamatan"]     = $this->mm->get_data_all_where("master_kecamatan",array("is_delete"=>"0"));
        // $data["list_kelurahan"]     = $this->mm->get_data_all_where("master_kelurahan",array("is_delete"=>"0"));
        $data_kelurahan = array();
        foreach ($data["list_kecamatan"] as $key => $value) {
            $data_kelurahan[$value->id_kecamatan] = $this->mm->get_data_all_where("master_kelurahan",array("is_delete"=>"0", "id_kecamatan"=>$value->id_kecamatan));
        }

        $data["list_kelurahan"] = json_encode($data_kelurahan);


        // print_r($data["list_kelurahan"]);
        $this->load->view("ad_super/header");
        $this->load->view("ad_super/pendidikan/pd_sekolah", $data);
        $this->load->view("ad_super/footer");
    }

    public function val_form_sekolah(){
        $config_val_input = array(
                array(
                    'field'=>'nama_sekolah',
                    'label'=>'nama_sekolah',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),
                array(
                    'field'=>'jenis_sekolah',
                    'label'=>'jenis_sekolah',
                    'rules'=>'required|alpha_numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'alpha_numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                    )
                       
                ),
                array(
                    'field'=>'kec_sekolah',
                    'label'=>'kec_sekolah',
                    'rules'=>'required|alpha_numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'alpha_numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                    )
                       
                ),
                array(
                    'field'=>'kel_sekolah',
                    'label'=>'kel_sekolah',
                    'rules'=>'required|alpha_numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'alpha_numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                    )
                       
                ),
                array(
                    'field'=>'loc_sekolah',
                    'label'=>'loc_sekolah',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),
                array(
                    'field'=>'alamat_sekolah',
                    'label'=>'alamat_sekolah',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),
                array(
                    'field'=>'tlp_sekolah',
                    'label'=>'tlp_sekolah',
                    'rules'=>'required|numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                    )
                       
                ),
                array(
                    'field'=>'web_sekolah',
                    'label'=>'web_sekolah',
                    'rules'=>'required|valid_url',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'valid_url'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                    )
                       
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function insert_sekolah(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "nama_sekolah"=>"",
                    "jenis_sekolah"=>"",
                    "kec_sekolah"=>"",
                    "kel_sekolah"=>"",
                    "loc_sekolah"=>"",
                    "alamat_sekolah"=>"",
                    "tlp_sekolah"=>"",
                    "web_sekolah"=>""
                );

        if($this->val_form_sekolah()){
            $nama_sekolah   = $this->input->post("nama_sekolah");
            $jenis_sekolah  = $this->input->post("jenis_sekolah");
            $kec_sekolah    = $this->input->post("kec_sekolah");
            $kel_sekolah    = $this->input->post("kel_sekolah");
            $loc_sekolah    = $this->input->post("loc_sekolah");
            $alamat_sekolah = $this->input->post("alamat_sekolah");
            $tlp_sekolah    = $this->input->post("tlp_sekolah");
            $web_sekolah    = $this->input->post("web_sekolah");

            $id_admin = $this->encrypt->decode($this->session->userdata("admin_lv_1")["id_admin"]);
            $time_update = date("Y-m-d h:i:s");
            
            $insert = $this->db->query("SELECT insert_pd_sekolah('".$jenis_sekolah."', '".$kec_sekolah."', '".$kec_sekolah."',"SD N Pandanwangi 7", "malang", "0", "0000-00-00 00:00:00", "AD2019020001") as id_sekolah");

            if($insert){
                $config['upload_path']          = './assets/core_img/icon_menu_sekolah/';
                $config['allowed_types']        = "jpg|png";
                $config['max_size']             = 512;
                $config['file_name']            = hash("sha256", $insert->row_array()["id_sekolah"]).".jpg";
                   
                $upload_data = $this->main_upload_file($config, "foto");
                
                if($upload_data["status"]){
                    $where = array("id_sekolah" => $insert->row_array()["id_sekolah"]);
                    $set = array("foto_sekolah" => $upload_data["main_data"]["upload_data"]["file_name"]);

                    if($this->mm->update_data("kesehatan_sekolah", $set, $where)){
                        $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
                    }else {
                        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPLOAD_FAIL"));
                    }
                }else{
                    $detail_msg["foto_jenis"] = $upload_data["main_msg"]["error"];
                    $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPLOAD_FAIL"));
                }
            }
            
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["nama_sekolah"]  = strip_tags(form_error('nama_sekolah'));
            $msg_detail["jenis_sekolah"] = strip_tags(form_error('jenis_sekolah'));
            $msg_detail["kec_sekolah"]   = strip_tags(form_error('kec_sekolah'));
            $msg_detail["kel_sekolah"]   = strip_tags(form_error('kel_sekolah'));
            $msg_detail["loc_sekolah"]   = strip_tags(form_error('loc_sekolah'));
            $msg_detail["alamat_sekolah"]= strip_tags(form_error('alamat_sekolah'));
            $msg_detail["tlp_sekolah"]   = strip_tags(form_error('tlp_sekolah'));
            $msg_detail["web_sekolah"]   = strip_tags(form_error('web_sekolah'));       
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
    

    public function get_sekolah_update(){
        $id = $this->encrypt->decode($this->input->post("id_sekolah"));
        $data = $this->pm->get_sekolah_each(array("id_sekolah"=>$id));

        $data_json["status"] = false;
        $data_json["val_response"] = null;
        if(!empty($data)){
            $data_json["status"] = true;
            $data_json["val_response"] = array("id_sekolah"=>$this->encrypt->encode($data["id_sekolah"]),
                                                "nama_sekolah"=>$data["nama_sekolah"],
                                                "alamat"=>$data["alamat"],
                                                "telepon"=>$data["telepon"],
                                                "id_layanan"=>$data["id_layanan"],
                                                "id_poli"=>str_replace("'", "\"", $data["id_poli"]),
                                                "foto"=>$data["foto_sekolah"]
                                            );
        }

        print_r(json_encode($data_json));
    }

    public function update_sekolah(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "nama_sekolah"=>"",
                    "alamat"=>"",
                    "telepon"=>"",
                    "id_layanan"=>"",
                    "foto"=>""
                );

        // print_r("<pre>");
        // print_r($_POST);
        // print_r($_FILES);

        if($this->val_form_sekolah()){
            $id_sekolah      = $this->encrypt->decode($this->input->post("id_sekolah"));

            $nama_sekolah    = $this->input->post("nama_sekolah");
            $alamat     = $this->input->post("alamat");
            $telepon    = $this->input->post("telepon");
            $id_layanan = $this->input->post("id_layanan");
            $id_poli    = str_replace("\"", "'", $this->input->post("id_poli"));

            $id_admin   = $this->encrypt->decode($this->session->userdata("admin_lv_1")["id_admin"]);
            $time_update = date("Y-m-d h:i:s");
            
            $set = array();
            
            $status_foto_upload = false;
            #----cek isset image avail or not----#
            if(isset($_FILES["foto"])){
            #----cek empty or not----#
                if($_FILES["foto"]){
                    $config['upload_path']          = './assets/core_img/icon_menu_sekolah/';
                    $config['allowed_types']        = "jpg|png";
                    $config['max_size']             = 512;
                    $config['file_name']            = hash("sha256", $id_sekolah).".jpg";
                       
                    $upload_data = $this->main_upload_file($config, "foto");
                    if($upload_data["status"]){
                        $set["foto_sekolah"] = $config["file_name"];
                        $status_foto_upload = true;
                    }else{
                        $detail_msg["foto"] = $upload_data["main_msg"]["error"];
                        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPLOAD_FAIL"));
                    }
                }
            }
            
            $set["nama_sekolah"]=$nama_sekolah;
            $set["alamat"]=$alamat;
            $set["telepon"]=$telepon;

            $set["id_layanan"]=$id_layanan;
            $set["id_poli"]=$id_poli;
            $set["id_admin"]=$id_admin;
            $set["waktu"]=$time_update;

            $where = array(
                "id_sekolah"=>$id_sekolah
            );

            if($this->mm->update_data("kesehatan_sekolah", $set, $where)){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
            } 
            
            
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["nama_sekolah"]      = strip_tags(form_error('nama_sekolah'));
            $msg_detail["alamat"]       = strip_tags(form_error('alamat'));
            $msg_detail["telepon"]      = strip_tags(form_error('telepon'));
            $msg_detail["id_layanan"]   = strip_tags(form_error('id_layanan'));            
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
    
    public function val_form_delete_sekolah(){
        $config_val_input = array(
                array(
                    'field'=>'id_sekolah',
                    'label'=>'id',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function delete_sekolah(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("DELETE_FAIL"));
        $msg_detail = "null";
        if($this->val_form_delete_sekolah()){
            $id_sekolah = $this->encrypt->decode($this->input->post("id_sekolah"));

            $is_del = "1";
            $time_del = date("Y-m-d h:i:s");

            $set = array(
                    "is_delete"=>$is_del,
                    "waktu"=>$time_del
                );

            $where = array("id_sekolah"=>$id_sekolah);

            if($this->mm->update_data("kesehatan_sekolah", $set, $where)){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
            }
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#=================================================================================================#
#-------------------------------------------sekolah-----------------------------------------------#
#=================================================================================================#

}
?>