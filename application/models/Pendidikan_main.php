<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Pendidikan_main extends CI_Model{

#=================================================================================================#
#-------------------------------------------Strata------------------------------------------------#
#=================================================================================================#  
    public function insert_strata($nama_strata, $time_update, $id_admin){
        return $this->db->query("select insert_pd_strata('".$nama_strata."',  '".$time_update."', '".$id_admin."') as id_strata;")->row_array();
    }
#=================================================================================================#
#-------------------------------------------Strata------------------------------------------------#
#=================================================================================================#  

#=================================================================================================#
#-------------------------------------------Jenis-------------------------------------------------#
#=================================================================================================#
    public function insert_jenis($id_strata, $nama_jenis, $time_update, $id_admin){
        return $this->db->query("select insert_pd_jenis('".$id_strata."', '".$nama_jenis."', '".$time_update."', '".$id_admin."') as id_jenis")->row_array();
    }

    public function get_jenis($where){
        $this->db->select("id_jenis, j.id_strata, nama_jenis, s.nama_strata");
        $this->db->join("pendidikan_strata s", "j.id_strata = s.id_strata");
        return $this->db->get_where("pendidikan_jenis j", $where)->result();
    }

    public function get_sch_api($where){
    	$this->db->select("mk.nama_kecamatan, pj.nama_jenis, sha2(id_sekolah, 512) as id_sekolah, foto_sklh, lokasi, nama_sekolah, detail_sekolah");
        $this->db->join("master_kecamatan mk", "ps.id_kecamatan = mk.id_kecamatan");
        $this->db->join("pendidikan_jenis pj", "ps.id_jenis = pj.id_jenis");
        return $this->db->get_where("pendidikan_sekolah ps", $where)->result();    	
    }
#=================================================================================================#
#-------------------------------------------Jenis-------------------------------------------------#
#=================================================================================================#

#=================================================================================================#
#-------------------------------------------sekolah_detail----------------------------------------#
#=================================================================================================#
    public function insert_sekolah($id_jenis, $id_kelurahan, $nama_sekolah, $lokasi, $detail_sekolah, $time_update, $id_admin, $id_kecamatan){
        return $this->db->query("SELECT insert_pd_sekolah('".$id_jenis."','".$id_kelurahan."','".$nama_sekolah."', '".$lokasi."', '".$detail_sekolah."', '".$time_update."', '".$id_admin."', '".$id_kecamatan."') as id_sekolah")->row_array();
    }

    public function get_sch_all($where){
        // $this->db->select("mk.nama_kecamatan, pj.nama_jenis, sha2(id_sekolah, 512) as id_sekolah, foto_sklh, lokasi, nama_sekolah, detail_sekolah");
        // $this->db->join("master_kelurahan mkl", "ps.id_kelurahan = mkl.id_kelurahan");
        $this->db->join("master_kecamatan mk", "ps.id_kecamatan = mk.id_kecamatan");
        $this->db->join("pendidikan_jenis pj", "ps.id_jenis = pj.id_jenis");
        return $this->db->get_where("pendidikan_sekolah ps", $where)->result();     
    }

    public function get_detail_sch_api($where){
    	$this->db->select("mk.nama_kecamatan, pj.nama_jenis, sha2(id_sekolah, 512) as id_sekolah, foto_sklh, lokasi, nama_sekolah, detail_sekolah");
        $this->db->join("master_kecamatan mk", "ps.id_kecamatan = mk.id_kecamatan");
        $this->db->join("pendidikan_jenis pj", "ps.id_jenis = pj.id_jenis");
        return $this->db->get_where("pendidikan_sekolah ps", $where)->result();    	
    }
#=================================================================================================#
#-------------------------------------------sekolah_detail----------------------------------------#
#=================================================================================================#


}
?>