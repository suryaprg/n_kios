<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Kesehatan_main extends CI_Model{

#=================================================================================================#
#-------------------------------------------Poli--------------------------------------------------#
#=================================================================================================#    
    public function insert_poli($nama_poli, $time_update, $id_admin){
    	$data = $this->db->query("select insert_poli('".$nama_poli."', '".$time_update."', '".$id_admin."') as id_poli");
    	return $data;
    }

    public function get_kesehatan_poli_api($list_poli){
        $this->db->select("sha2(id_poli, 512) as id_poli, nama_poli, foto");
        $this->db->where_in("id_poli", $list_poli);
        $data = $this->db->get_where("kesehatan_poli")->result();

        return $data;
    }
#=================================================================================================#
#-------------------------------------------Poli--------------------------------------------------#
#=================================================================================================#

#=================================================================================================#
#-------------------------------------------Jenis_rs----------------------------------------------#
#=================================================================================================#
    public function insert_jenis_rs($nama_jenis, $time_update, $id_admin){
    	$data = $this->db->query("select insert_jenis_rs('".$nama_jenis."', '".$time_update."', '".$id_admin."') as id_jenis_rs");
    	return $data;
    }

    public function get_kesehatan_layanan_api($where){
        $this->db->select("sha2(id_layanan, 512) as id_layanan, nama_layanan, foto");
        $data = $this->db->get_where("kesehatan_jenis")->result();

        return $data;
    }
#=================================================================================================#
#-------------------------------------------Jenis_rs----------------------------------------------#
#=================================================================================================#

#=================================================================================================#
#-------------------------------------------Rumah_Sakit-------------------------------------------#
#=================================================================================================#
    public function insert_rs($nama_poli, $time_update, $id_admin){
    	$data = $this->db->query("select insert_rs('".$nama_poli."', '".$time_update."', '".$id_admin."') as id_rs");
    	return $data;
    }

    public function get_rs_all($where){
        $this->db->join("kesehatan_jenis a", "b.id_layanan = a.id_layanan");
        $data = $this->db->get_where("Kesehatan_rs b", $where)->result();

        return $data;
    }

    public function get_rs_each($where){
        $this->db->join("kesehatan_jenis a", "b.id_layanan = a.id_layanan");
        $data = $this->db->get_where("Kesehatan_rs b", $where)->row_array();

        return $data;
    }

    public function get_kesehatan_rs_api($where){
        $this->db->select("sha2(id_rs, 512) as id_rs, nama_rumah_sakit, alamat, telepon, nama_layanan, foto_rs");
        $this->db->join("kesehatan_jenis a", "b.id_layanan = a.id_layanan");
        $data = $this->db->get_where("Kesehatan_rs b",$where)->result();

        return $data;
    }


#=================================================================================================#
#-------------------------------------------Rumah_Sakit-------------------------------------------#
#=================================================================================================#

}
?>