<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_main extends CI_Model{

	public function select_admin($where){
		$this->db->select("id_admin, a.id_lv, al.ket, email, status_active, nama, jabatan, a.id_bidang, dn.nama_dinas");
        $this->db->join("admin_lv al", "al.id_lv = a.id_lv");
        $this->db->join("dinas dn", "dn.id_dinas = a.id_bidang");
        $data = $this->db->get_where("admin a", $where)->row_array();
        return $data;
	}

    public function select_admin_all($where){
        $this->db->select("id_admin, a.id_lv, al.ket, email, status_active, nama, jabatan, a.id_bidang, dn.nama_dinas");
        $this->db->join("admin_lv al", "al.id_lv = a.id_lv");
        $this->db->join("dinas dn", "dn.id_dinas = a.id_bidang");
        $data = $this->db->get_where("admin a", $where)->result();
        return $data;
    }

    public function insert_dinas($nama_dinas, $alamat, $page_admin, $admin, $time_update){
    	$data = $this->db->query("select insert_dinas('".$nama_dinas."','".$alamat."','".$page_admin."','".$admin."','".$time_update."') as id_dinas;");
    	return $data;
    }

    public function insert_page_main($id_kategori, $nama_page, $next_page, $waktu, $id_admin){
        $data = $this->db->query("select insert_page_main('".$id_kategori."','".$nama_page."','".$next_page."','".$waktu."','".$id_admin."') as id_page;");
        return $data;
    }

    public function get_menu_main($where){
        $this->db->join("home_page_kategori b", "a.id_kategori = b.id_kategori");
        $data = $this->db->get_where("home_page_main a", $where)->result();
        return $data;
    }

    public function get_menu_kategori($where){
        $this->db->select("sha2(id_kategori, '256') as id_kategori, nama_kategori");
        $data = $this->db->get_where("home_page_kategori", $where)->result();
        return $data;
    }

    

}
?>