
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?php print_r(base_url());?>admin_template/assets/images/favicon.png">
    <title>Admin Press Admin Template - The Ultimate Bootstrap 4 Admin Template</title>
    <!-- Bootstrap Core CSS -->
    <link href="<?php print_r(base_url());?>admin_template/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- chartist CSS -->
    <link href="<?php print_r(base_url());?>admin_template/assets/plugins/chartist-js/dist/chartist.min.css" rel="stylesheet">
    <link href="<?php print_r(base_url());?>admin_template/assets/plugins/chartist-js/dist/chartist-init.css" rel="stylesheet">
    <link href="<?php print_r(base_url());?>admin_template/assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php print_r(base_url());?>admin_template/horizontal/css/style.css" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="<?php print_r(base_url());?>admin_template/horizontal/css/colors/blue.css" id="theme" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body class="fix-header card-no-border logo-center">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-light">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">
                        <span>
                            <img src="<?php print_r(base_url());?>assets/core_img/logo_apelmas_home.png" class="light-logo" alt="homepage" />
                        </span> 
                    </a>
                </div>
                <div class="navbar-collapse">
                    <ul class="navbar-nav my-lg-0">
                        
                    </ul>
                </div> 
            </nav>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <div class="row">
                <!-- Column -->
                <div class="col-lg-12">
                    <div id="carouselExampleIndicators3" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#carouselExampleIndicators3" data-slide-to="0" class="active"></li>
                            <li data-target="#carouselExampleIndicators3" data-slide-to="1"></li>
                            <li data-target="#carouselExampleIndicators3" data-slide-to="2"></li>
                        </ol>
                        <div class="carousel-inner" role="listbox">
                            <div class="carousel-item active">
                                <img class="img-responsive" style="height: 600px;" src="<?php print_r(base_url());?>admin_template/assets/images/big/img6.jpg" alt="First slide">
                                <div class="carousel-caption d-none d-md-block">
                                    <h3 class="text-white">First title goes here</h3>
                                    <p>this is the subcontent you can use this</p>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <img class="img-responsive" style="height: 600px;" src="<?php print_r(base_url());?>admin_template/assets/images/big/img3.jpg" alt="Second slide">
                                <div class="carousel-caption d-none d-md-block">
                                    <h3 class="text-white">Second title goes here</h3>
                                    <p>this is the subcontent you can use this</p>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <img class="img-responsive" style="height: 600px;" src="<?php print_r(base_url());?>admin_template/assets/images/big/img4.jpg" alt="Third slide">
                                <div class="carousel-caption d-none d-md-block">
                                    <h3 class="text-white">Third title goes here</h3>
                                    <p>this is the subcontent you can use this</p>
                                </div>
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleIndicators3" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleIndicators3" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>

                </div>

            </div>
            
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
                <div class="row">
                    
                    <div class="col-md-2">
                        <div class="card text-center">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <img src="<?php print_r(base_url());?>assets/core_img/icon_menu/menu_fasilitas.png" alt="homepage"/>            
                                    </div>
                                    <div class="col-lg-12">
                                        &nbsp;
                                    </div>
                                    <div class="col-lg-12" style="height: 40px;">
                                        <a href="#"><h4 class="card-title">Event Pemerintahan</h4></a>
                                    </div>
                                </div>                                
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="card text-center">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <img src="<?php print_r(base_url());?>assets/core_img/icon_menu/menu_kependudukan.png" alt="homepage"/>            
                                    </div>
                                    <div class="col-lg-12">
                                        &nbsp;
                                    </div>
                                    <div class="col-lg-12" style="height: 40px;">
                                        <a href="#"><h4 class="card-title">Event Pemerintahan</h4></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-2" id="kesehatan">
                        <div class="card text-center">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <img src="<?php print_r(base_url());?>assets/core_img/icon_menu/menu_komunitas.png" alt="homepage"/>            
                                    </div>
                                    <div class="col-lg-12">
                                        &nbsp;
                                    </div>
                                    <div class="col-lg-12" style="height: 40px;">
                                        <a href="#"><h4 class="card-title">Event Pemerintahan</h4></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="card text-center">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <img src="<?php print_r(base_url());?>assets/core_img/icon_menu/menu_pangan.png" alt="homepage"/>            
                                    </div>
                                    <div class="col-lg-12">
                                        &nbsp;
                                    </div>
                                    <div class="col-lg-12" style="height: 40px;">
                                        <a href="#"><h4 class="card-title">Event Pemerintahan</h4></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="card text-center">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <img src="<?php print_r(base_url());?>assets/core_img/icon_menu/menu_kesehatan.png" alt="homepage"/>            
                                    </div>
                                    <div class="col-lg-12">
                                        &nbsp;
                                    </div>
                                    <div class="col-lg-12" style="height: 40px;">
                                        <a href="#"><h4 class="card-title">Event Pemerintahan</h4></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="card text-center">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <img src="<?php print_r(base_url());?>assets/core_img/icon_menu/menu_kesehatan.png" alt="homepage"/>            
                                    </div>
                                    <div class="col-lg-12">
                                        &nbsp;
                                    </div>
                                    <div class="col-lg-12" style="height: 40px;">
                                        <a href="#"><h4 class="card-title">Event Pemerintahan</h4></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="card text-center">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <img src="<?php print_r(base_url());?>assets/core_img/icon_menu/menu_kesehatan.png" alt="homepage"/>            
                                    </div>
                                    <div class="col-lg-12">
                                        &nbsp;
                                    </div>
                                    <div class="col-lg-12" style="height: 40px;">
                                        <a href="#"><h4 class="card-title">Event Pemerintahan</h4></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="card text-center">
                            <div class="card-body">
                                <a href="#"><h4 class="card-title">Kesehatan</h4></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="card text-center">
                            <div class="card-body">
                                <a href="#"><h4 class="card-title">Kesehatan</h4></a>
                            </div>
                        </div>
                    </div>

                </div>


                <div class="row">
                    <div class="col-md-2" id="kesehatan">
                        <div class="card text-center">
                            <div class="card-body">
                                <a href="#"><h4 class="card-title">Kesehatan</h4></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="card text-center">
                            <div class="card-body">
                                <a href="#"><h4 class="card-title">Kependudukan</h4></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="card text-center">
                            <div class="card-body">
                                <a href="#"><h4 class="card-title">Kesehatan</h4></a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Row -->
                               
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->

                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <div class="right-sidebar">
                    <div class="slimscrollright">
                        <div class="rpanel-title"> Service Panel <span><i class="ti-close right-side-toggle"></i></span> </div>
                        <div class="r-panel-body">
                            <ul class="m-t-20 chatonline">
                                <li><b>Chat option</b></li>
                                <li>
                                    <a href="javascript:void(0)"><img src="<?php print_r(base_url());?>admin_template/assets/images/users/1.jpg" alt="user-img" class="img-circle"> <span>Varun Dhavan <small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="<?php print_r(base_url());?>admin_template/assets/images/users/2.jpg" alt="user-img" class="img-circle"> <span>Genelia Deshmukh <small class="text-warning">Away</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="<?php print_r(base_url());?>admin_template/assets/images/users/3.jpg" alt="user-img" class="img-circle"> <span>Ritesh Deshmukh <small class="text-danger">Busy</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="<?php print_r(base_url());?>admin_template/assets/images/users/4.jpg" alt="user-img" class="img-circle"> <span>Arijit Sinh <small class="text-muted">Offline</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="<?php print_r(base_url());?>admin_template/assets/images/users/5.jpg" alt="user-img" class="img-circle"> <span>Govinda Star <small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="<?php print_r(base_url());?>admin_template/assets/images/users/6.jpg" alt="user-img" class="img-circle"> <span>John Abraham<small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="<?php print_r(base_url());?>admin_template/assets/images/users/7.jpg" alt="user-img" class="img-circle"> <span>Hritik Roshan<small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="<?php print_r(base_url());?>admin_template/assets/images/users/8.jpg" alt="user-img" class="img-circle"> <span>Pwandeep rajan <small class="text-success">online</small></span></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer"> ©2019 Dinas Komunikasi dan Informatika Kota Malang and Template by Admin Press Admin by themedesigner.in </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="<?php print_r(base_url());?>admin_template/assets/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php print_r(base_url());?>admin_template/assets/plugins/bootstrap/js/popper.min.js"></script>
    <script src="<?php print_r(base_url());?>admin_template/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?php print_r(base_url());?>admin_template/horizontal/js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="<?php print_r(base_url());?>admin_template/horizontal/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="<?php print_r(base_url());?>admin_template/horizontal/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="<?php print_r(base_url());?>admin_template/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <script src="<?php print_r(base_url());?>admin_template/assets/plugins/sparkline/jquery.sparkline.min.js"></script>
    <!--Custom JavaScript -->
    <script src="<?php print_r(base_url());?>admin_template/horizontal/js/custom.min.js"></script>
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
    <script src="<?php print_r(base_url());?>admin_template/assets/plugins/skycons/skycons.js"></script>
    <!-- chartist chart -->
    <script src="<?php print_r(base_url());?>admin_template/assets/plugins/chartist-js/dist/chartist.min.js"></script>
    <script src="<?php print_r(base_url());?>admin_template/assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js"></script>
    <script src="<?php print_r(base_url());?>admin_template/horizontal/js/dashboard3.js"></script>
    <!-- ============================================================== -->
    <!-- Style switcher -->
    <!-- ============================================================== -->
    <script src="<?php print_r(base_url());?>admin_template/assets/plugins/styleswitcher/jQuery.style.switcher.js"></script>

    <!-- <script src="<?php print_r(base_url());?>assets/js/jquery-3.2.1.js"></script> -->

    <script type="text/javascript">
        $("#kesehatan").click(function(){
            console.log("ok");
        });
    </script>
</body>

</html>