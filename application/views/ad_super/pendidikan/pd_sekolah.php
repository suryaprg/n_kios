<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Halaman Pendidikan - Daftar Sekolah</h3>
        </div>
    </div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-6">
                                <h4 class="card-title">Daftar Daftar Sekolah Kota Malang</h4>
                            </div>
                            <div class="col-lg-6 text-right">
                                <button type="button" class="btn btn-info btn-rounded" data-toggle="modal" data-target="#insert_data">
                                    <i class="fa fa-plus-circle"></i> Tambah
                                </button>
                            </div>
                        </div>
                        <br>
                        <div class="table-responsive">
                            <table id="myTable" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Sekolah</th>
                                        <th>Kecamatan</th>
                                        <th>Alamat</th>
                                        <th>URL(Website)</th>
                                        <th>Telephon</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        if(isset($list_data_sekolah)){
                                            if(!empty($list_data_sekolah)){
                                                // print_r($list_data_sekolah);
                                                $no = 1;
                                                foreach ($list_data_sekolah as $r_list_data_sekolah => $v_list_data_sekolah) {

                                                    $alamat = "-";
                                                    $website = "-";
                                                    $tlp = "-";

                                                    $data_detail = str_replace("'", "\"", $v_list_data_sekolah->detail_sekolah);

                                                    if($this->jsoncheck->isJSON($data_detail)){
                                                        $data_detail = json_decode($data_detail);
                                                        $alamat     = $data_detail->alamat;
                                                        $website    = $data_detail->url;
                                                        $tlp        = $data_detail->tlp;
                                                    }
                                                    
                                                    print_r("<tr>
                                                                <td>".$no++."</td>
                                                                <td>".$v_list_data_sekolah->nama_sekolah." (".$v_list_data_sekolah->nama_jenis.") </td>
                                                                <td>".$v_list_data_sekolah->nama_kecamatan."</td>
                                                                <td>".$alamat."</td>
                                                                <td>".$website."</td>
                                                                <td>".$tlp."</td>
                                                                <td>
                                                                <center>
                                                                    <button type=\"button\" class=\"btn btn-info\" id=\"up_sekolah\" onclick=\"get_update_sekolah('".$this->encrypt->encode($v_list_data_sekolah->id_sekolah)."')\"><i class=\"fa fa-pencil-square-o\"></i></button>
                                                                    <button class=\"btn btn-danger\" id=\"del_sekolah\" onclick=\"delete_sekolah('".$this->encrypt->encode($v_list_data_sekolah->id_sekolah)."')\" style=\"width: 40px;\"><i class=\"fa fa-trash-o\"></i></button>
                                                                </center>
                                                                </td>
                                                            </tr>");
                                                }
                                            }
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="insert_data" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Form Tambah Daftar Sekolah</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Nama Sekolah</label>
                                <input type="text" name="nama_sekolah" id="nama_sekolah" class="form-control form-control-line">
                                <a id="msg_nama_sekolah" style="color: red;"></a>
                            </div>   
                        </div>

                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Jenis Sekolah</label>
                                <select name="jenis_sekolah" id="jenis_sekolah" class="form-control form-control-line">
                                    <?php
                                        if($list_jenis){
                                            foreach ($list_jenis as $key => $value) {
                                                print_r("<option value=\"".$value->id_jenis."\">".$value->nama_jenis."</option>");            
                                            }
                                        }
                                    ?>
                                </select>
                                <a id="msg_jenis_sekolah" style="color: red;"></a>
                            </div>    
                        </div>


                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Kecamatan</label>
                                <select name="kec_sekolah" id="kec_sekolah" class="form-control form-control-line">
                                    <?php
                                        if($list_kecamatan){
                                            foreach ($list_kecamatan as $key => $value) {
                                                print_r("<option value=\"".$value->id_kecamatan."\">".$value->nama_kecamatan."</option>");            
                                            }
                                        }
                                    ?>
                                </select>
                                <!-- <input type="text" name="kec_sekolah" id="kec_sekolah" class="form-control form-control-line"> -->
                                <a id="msg_kec_sekolah" style="color: red;"></a>
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Kelurahan</label>
                                <select name="kel_sekolah" id="kel_sekolah" class="form-control form-control-line">
                                </select>
                                <a id="msg_kel_sekolah" style="color: red;"></a>
                            </div>    
                        </div>



                        <div class="col-lg-12">
                            <div class="form-group">
                                <label>Lokasi (Latitude & longitude)</label>
                                <input type="text" name="loc_sekolah" id="loc_sekolah" class="form-control form-control-line">
                                <a id="msg_loc_sekolah" style="color: red;"></a>
                            </div>
                        </div>


                        <div class="col-lg-12">
                            <div class="form-group">
                                <label>Alamat</label>
                                <textarea name="alamat_sekolah" id="alamat_sekolah" class="form-control form-control-line"></textarea>
                                <a id="msg_almt_sekolah" style="color: red;"></a>
                            </div>
                        </div>



                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Telepon</label>
                                <input type="text" name="tlp_sekolah" id="tlp_sekolah" class="form-control form-control-line">
                                <a id="msg_tlp_sekolah" style="color: red;"></a>
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Website</label>
                                <input type="text" name="web_sekolah" id="web_sekolah" class="form-control form-control-line">
                                <a id="msg_web_sekolah" style="color: red;"></a>
                            </div>
                        </div>


                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Gambar Keterangan Daftar Sekolah</label>
                                <input type="file" name="foto_sekolah" id="foto_sekolah" class="form-control form-control-line">
                                <a id="msg_foto_sekolah" style="color: red;"></a>
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <center><img id="img_foto_sekolah" src="" style="width: 350px; height: 225px;"></center>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                    <button type="submit" id="btn_insert_data" class="btn btn-info waves-effect">Simpan</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div id="update_data" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Form Update Daftar Sekolah</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">                
                    <div class="form-group">
                        <label>Keterangan Daftar Sekolah</label>
                        <input type="text" name="nama_sekolah" id="_nama_sekolah" class="form-control form-control-line">
                        <a id="_msg_nama_sekolah" style="color: red;"></a>
                    </div>
                    <div class="form-group">
                        <label>Gambar Keterangan Daftar Sekolah</label>
                        <input type="file" name="foto_sekolah" id="_foto_sekolah" class="form-control form-control-line">
                        <a id="_msg_foto_sekolah" style="color: red;"></a>
                    </div>
                    <center><img id="_img_foto_sekolah" src="" style="width: 259px; height: 173px;"></center>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                    <button type="submit" id="btn_update_data" class="btn btn-info waves-effect">Ubah</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.mod8al-dialog -->
    </div>

    <?php
        $kelurahan = "";

        if($list_kelurahan){
            $kelurahan = $list_kelurahan;
        }
    ?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript">
//========================================================================
//--------------------------------Get_Lokasi------------------------------
//========================================================================
    var str_list_kelurahan = '<?php print_r($kelurahan);?>';
    var json_list_kelurahan = [];
    try{
        json_list_kelurahan = JSON.parse(str_list_kelurahan);
    }catch(e){
        json_list_kelurahan = [];
    } 

    console.log(json_list_kelurahan);

    $(document).ready(function(){
        if(navigator.geolocation){
            navigator.geolocation.getCurrentPosition(showLocation);
        }else{ 
            console.log('Geolocation is not supported by this browser.');
        }
    });

    function showLocation(position){
        var latitude = position.coords.latitude;
        var longitude = position.coords.longitude;
        console.log(position);
    }
//========================================================================
//--------------------------------Get_Lokasi------------------------------
//========================================================================

//========================================================================
//--------------------------------kec_sekolah_change----------------------
//========================================================================
    $("#kec_sekolah").change(function(){
        var val_kec = $("#kec_sekolah").val();

        $("#kel_sekolah").html("");
        // console.log(json_list_kelurahan[val_kec]);
        if(json_list_kelurahan[val_kec]){
            // $("#kel_sekolah").html("");
            var str_options_kel = "";
            for (var i = 0; i < json_list_kelurahan[val_kec].length; i++) {
                // Things[i]
                str_options_kel += "<option value=\""+json_list_kelurahan[val_kec][i]["id_kelurahan"]+"\">"+json_list_kelurahan[val_kec][i]["nama_kelurahan"]+"</option>";
            }
            console.log(str_options_kel);
        }

        $("#kel_sekolah").html(str_options_kel);
    });
//========================================================================
//--------------------------------kec_sekolah_change----------------------
//========================================================================




//========================================================================
//--------------------------------Insert_data-----------------------------
//========================================================================
    var file = [];
    $("#foto_sekolah").change(function(e){
        file = e.target.files[0];

        $("#img_foto_sekolah").attr("src",URL.createObjectURL(file));
        console.log(file);
    });  

    $("#btn_insert_data").click(function(){
            var data_main =  new FormData();
            data_main.append('nama_sekolah' , $("#nama_sekolah").val());
            data_main.append('jenis_sekolah' , $("#jenis_sekolah").val());
            data_main.append('kec_sekolah' , $("#kec_sekolah").val());
            data_main.append('kel_sekolah' , $("#kel_sekolah").val());
            data_main.append('loc_sekolah' , $("#loc_sekolah").val());
            data_main.append('alamat_sekolah' , $("#alamat_sekolah").val());
            data_main.append('tlp_sekolah' , $("#tlp_sekolah").val());
            data_main.append('web_sekolah' , $("#web_sekolah").val());

            data_main.append('foto_sekolah' , file);
                                        
            $.ajax({
                url: "<?php echo base_url()."super/act/add/jenis";?>",
                dataType: 'html',  // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,                         
                type: 'post',
                success: function(res){
                    console.log(res);
                    // response_insert(res);
                }
            });
        });

        function response_insert(res){
            var data_json = JSON.parse(res);
            console.log(data_json);
                var main_msg = data_json.msg_main;
                var detail_msg = data_json.msg_detail;
            if(main_msg.status){
                !function($) {
                    "use strict";
                    var SweetAlert = function() {};
                    //examples 
                    SweetAlert.prototype.init = function() {
                        //Warning Message
                        swal({   
                            title: "Proses Berhasil.!!",   
                            text: "Data admin berhasil disimpan ..!",   
                            type: "success",   
                            showCancelButton: false,   
                            confirmButtonColor: "#28a745",   
                            confirmButtonText: "Lanjutkan",   
                            closeOnConfirm: false 
                        }, function(){
                            window.location.href = "<?php echo base_url()."admin/super/jenis_rumah_sakit";?>";
                        });                              
                    },
                    //init
                    $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                }(window.jQuery),
                
                function($) {
                    "use strict";
                    $.SweetAlert.init()
                }(window.jQuery);
            }else{
                !function($) {
                    "use strict";
                    var SweetAlert = function() {};
                    //examples 
                    SweetAlert.prototype.init = function() {
                        //Warning Message
                        $("#msg_nama_sekolah").html(detail_msg.nama);
                        $("#msg_foto_sekolah").html(detail_msg.nip);

                        swal("Proses Gagal.!!", "Data admin gagal disimpan, coba periksa jaringan dan koneksi anda", "warning");                   
                    },
                                              
                    $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                }(window.jQuery),

                function($) {
                    "use strict";
                    $.SweetAlert.init()
                }(window.jQuery);
                
            }
        }
//========================================================================
//--------------------------------Insert_data-----------------------------
//========================================================================

var id_sekolah_glob = "";

//========================================================================
//--------------------------------Get_Update_data-------------------------
//========================================================================
    function clear_from_update(){
        $("#_nama_sekolah").val("");
        $("#_foto_sekolah").val("");
        $("#_img_foto_sekolah").attr("src", "");
        id_sekolah_glob = "";
    }

    function get_update_sekolah(param){
        clear_from_update();

        var data_main =  new FormData();
        data_main.append('id_layanan', param);
                                        
        $.ajax({
            url: "<?php echo base_url()."super/act/get/jenis";?>",
            dataType: 'html',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: data_main,                         
            type: 'post',
            success: function(res){
                console.log(res);
                set_val_update(res, param);
                $("#update_data").modal('show');
            }
        });
    }

    function set_val_update(res, param){
        var res_pemohon = JSON.parse(res.toString());

        if(res_pemohon.status == true){
            var id_sekolah_chahce = res_pemohon.val_response.id_layanan;
            $("#_nama_sekolah").val(res_pemohon.val_response.nama_layanan);
            $("#_img_foto_sekolah").attr("src", res_pemohon.val_response.foto);

            id_sekolah_glob = id_sekolah_chahce;
        }else {
            clear_from_update();
        }
    }
//========================================================================
//--------------------------------Get_Update_data-------------------------
//========================================================================

//========================================================================
//--------------------------------Update_data-----------------------------
//========================================================================
    var file = [];
    $("#_foto_sekolah").change(function(e){
        file = e.target.files[0];

        $("#_img_foto_sekolah").attr("src",URL.createObjectURL(file));
        console.log(file);
    });

    $("#btn_update_data").click(function() {
        var data_main = new FormData();
        data_main.append('nama_sekolah', $("#_nama_sekolah").val());
        data_main.append('foto_sekolah', file);

        data_main.append('id_sekolah', id_sekolah_glob);

        $.ajax({
            url: "<?php echo base_url()."super/act/up/jenis";?>",
            dataType: 'html', // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: data_main,
            type: 'post',
            success: function(res) {
                console.log(res);
                response_update(res);
            }
        });
    });

    function response_update(res) {
        var data_json = JSON.parse(res);

        var main_msg = data_json.msg_main;
        var detail_msg = data_json.msg_detail;
        if (main_msg.status) {
            ! function($) {
                "use strict";
                var SweetAlert = function() {};
                //examples 
                SweetAlert.prototype.init = function() {
                        //Warning Message
                        swal({
                            title: "Proses Berhasil.!!",
                            text: "Data admin berhasil disimpan ..!",
                            type: "success",
                            showCancelButton: false,
                            confirmButtonColor: "#28a745",
                            confirmButtonText: "Lanjutkan",
                            closeOnConfirm: false
                        }, function() {
                            window.location.href = "<?php echo base_url()."admin/super/jenis_rumah_sakit";?>";
                        });
                    },
                    //init
                    $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
            }(window.jQuery),

            function($) {
                "use strict";
                $.SweetAlert.init()
            }(window.jQuery);
        } else {
            ! function($) {
                "use strict";
                var SweetAlert = function() {};
                //examples 
                SweetAlert.prototype.init = function() {
                        //Warning Message
                        $("#msg_nama_sekolah").html(detail_msg.nama);
                        $("#msg_foto_sekolah").html(detail_msg.nip);

                        swal("Proses Gagal.!!", "Data admin gagal disimpan, coba periksa jaringan dan koneksi anda", "warning");
                    },

                    $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
            }(window.jQuery),

            function($) {
                "use strict";
                $.SweetAlert.init()
            }(window.jQuery);

        }
    }
//========================================================================
//--------------------------------Update_data-----------------------------
//========================================================================

//========================================================================
//--------------------------------delete_data-----------------------------
//========================================================================
        function delete_sekolah(id_sekolah){
            !function($) {
                "use strict";
                var SweetAlert = function() {};
                //examples 
                SweetAlert.prototype.init = function() {
                    //Warning Message
                    swal({   
                        title: "Pesan Konfirmasi",   
                        text: "Silahkan Cermati data sebelem di hapus permanen, jika anda sudah yakin maka data ini dan seluruh data yang berkaitan akan di hapus",   
                        type: "warning",   
                        showCancelButton: true,   
                        confirmButtonColor: "#ffb22b",   
                        confirmButtonText: "Hapus",   
                        closeOnConfirm: true 
                    }, function(){
                        
                        var data_main =  new FormData();
                        data_main.append('id_sekolah', id_sekolah);
                                                    
                        $.ajax({
                            url: "<?php echo base_url()."super/act/del/jenis";?>",
                            dataType: 'html',  // what to expect back from the PHP script, if anything
                            cache: false,
                            contentType: false,
                            processData: false,
                            data: data_main,                         
                            type: 'post',
                            success: function(res){
                                console.log(res);
                                swal("Proses Berhasil.!!", "Penghapusan Data Berhasil", "success");
                                location.href="<?php print_r(base_url()."admin/super/jenis_rumah_sakit");?>";
                            }
                        });   
                    });                                     
                },
                                          
                $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
            }(window.jQuery),

            function($) {
                "use strict";
                $.SweetAlert.init()
            }(window.jQuery);
        }
//========================================================================
//--------------------------------delete_data-----------------------------
//========================================================================

</script>
