<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Halaman Pendidikan - Jenis Sekolah</h3>
        </div>
    </div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-6">
                                <h4 class="card-title">Daftar Jenis Sekolah Kota Malang</h4>
                            </div>
                            <div class="col-lg-6 text-right">
                                <button type="button" class="btn btn-info btn-rounded" data-toggle="modal" data-target="#insert_data">
                                    <i class="fa fa-plus-circle"></i> Tambah Jenis Sekolah
                                </button>
                                &nbsp;&nbsp;
                                <button type="button" class="btn btn-primary btn-rounded" data-toggle="modal" data-target="#insert_data_strata">
                                    <i class="fa fa-plus-circle"></i> Tambah Strata Sekolah
                                </button>
                            </div>
                        </div>
                        <br>
                        <div class="table-responsive">
                            <table id="myTable" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Jenis Sekolah</th>
                                        <th>Strata Pendidikan</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        if(isset($list_jenis)){
                                            if(!empty($list_jenis)){
                                                // print_r($list_jenis);
                                                $no = 1;
                                                foreach ($list_jenis as $r_list_jenis => $v_list_jenis) {
                                                    print_r("<tr>
                                                                <td>".$no++."</td>
                                                                <td>".$v_list_jenis->nama_jenis."</td>
                                                                <td>".$v_list_jenis->nama_strata."</td>
                                                                <td>
                                                                <center>
                                                                    <button type=\"button\" class=\"btn btn-info\" id=\"up_jenis\" onclick=\"get_update_jenis('".$this->encrypt->encode($v_list_jenis->id_jenis)."')\"><i class=\"fa fa-pencil-square-o\"></i></button>
                                                                    <button class=\"btn btn-danger\" id=\"del_jenis\" onclick=\"delete_jenis('".$this->encrypt->encode($v_list_jenis->id_jenis)."')\" style=\"width: 40px;\"><i class=\"fa fa-trash-o\"></i></button>
                                                                </center>
                                                                </td>
                                                            </tr>");
                                                }
                                            }
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="insert_data" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Form Tambah Jenis Sekolah</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                
                <div class="modal-body">                
                    <div class="form-group">
                        <div class="form-group">
                            <label>Keterangan Jenis Sekolah</label>
                            <input type="text" name="nama_jenis" id="nama_jenis" class="form-control form-control-line">
                            <a id="msg_nama_jenis" style="color: red;"></a>
                        </div>
                        <div class="form-group">
                            <label>Strata Sekolah</label>
                            <select name="strata" id="strata" class="form-control form-control-line">
                                <?php
                                if($list_strata){
                                    foreach ($list_strata as $key => $value) {
                                        // print_r($value);
                                        print_r("<option value=\"".$value->id_strata."\">".$value->nama_strata."</option>");    
                                    }
                                }
                                ?>
                            </select>
                            <a id="msg_strata" style="color: red;"></a>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                    <button type="submit" id="btn_insert_data" class="btn btn-info waves-effect">Simpan</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div id="update_data" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Form Ubah Jenis Sekolah</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                
                <div class="modal-body">                
                    <div class="form-group">
                        <div class="form-group">
                            <label>Keterangan Jenis Sekolah</label>
                            <input type="text" name="nama_jenis" id="_nama_jenis" class="form-control form-control-line">
                            <a id="_msg_nama_jenis" style="color: red;"></a>
                        </div>
                        <div class="form-group">
                            <label>Strata Sekolah</label>
                            <select name="strata" id="_strata" class="form-control form-control-line">
                                <?php
                                if($list_strata){
                                    foreach ($list_strata as $key => $value) {
                                        print_r("<option value=\"".$value->id_strata."\">".$value->nama_strata."</option>");  
                                    }
                                }
                                ?>
                            </select>
                            <a id="_msg_strata" style="color: red;"></a>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                    <button type="submit" id="btn_update_data" class="btn btn-info waves-effect">Ubah</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div id="insert_data_strata" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Form Tambah Strata Sekolah</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                
                <div class="modal-body">                
                    <div class="table-responsive">
                        <table class="table product-overview">
                            <thead>
                                <tr>
                                    <th>No. </th>
                                    <th>Nama Strata</th>
                                    <th>Aksi</th>
                                </tr>
                                <tr>
                                    <td>#</td>
                                    <td><input type="text" name="nama_strata" id="nama_strata" class="form-control form-control-line"></td>
                                    <td>
                                        <center>
                                        <button class="btn btn-info waves-effect" id="add_strata"><i class="mdi mdi-plus"></i></button>
                                        <button class="btn btn-warning waves-effect" id="up_strata"><i class="mdi mdi-grease-pencil"></i></button>
                                        <button class="btn btn-danger waves-effect" id="cancel_strata"><i class="mdi mdi-close"></i></button>
                                        </center>
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if($list_strata){
                                    $no = 1;
                                    foreach ($list_strata as $key => $value) {
                                        print_r("<tr>
                                                    <td>".$no."</td>
                                                    <td>".$value->nama_strata."</td>
                                                    <td>
                                                        <center>
                                                        <button type=\"button\" class=\"btn btn-info\" id=\"up_strata\" onclick=\"get_update_strata('".$this->encrypt->encode($value->id_strata)."')\"><i class=\"fa fa-pencil-square-o\"></i></button>
                                                        <button class=\"btn btn-danger\" id=\"del_strata\" onclick=\"delete_strata('".$this->encrypt->encode($value->id_strata)."')\" style=\"width: 40px;\"><i class=\"fa fa-trash-o\"></i></button>
                                                        </center>
                                                    </td>    
                                                </tr>");  $no++;
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Tutup</button>
                    <!-- <button type="submit" id="btn_insert_data" class="btn btn-info waves-effect">Simpan</button> -->
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript">
//========================================================================
//--------------------------------Get_Lokasi------------------------------
//========================================================================
    $(document).ready(function(){

        $("#up_strata").attr("hidden", true);
        $("#cancel_strata").attr("hidden", true);

        if(navigator.geolocation){
            navigator.geolocation.getCurrentPosition(showLocation);
        }else{ 
            console.log('Geolocation is not supported by this browser.');
        }
    });

    function showLocation(position){
        var latitude = position.coords.latitude;
        var longitude = position.coords.longitude;
        console.log(position);
    }
//========================================================================
//--------------------------------Get_Lokasi------------------------------
//========================================================================
    var id_jenis_glob_strata = "";

//----------------------------------------strata--------------------------------------

    //========================================================================
    //--------------------------------Insert_strata---------------------------
    //========================================================================
            $("#add_strata").click(function(){
                var data_main =  new FormData();
                data_main.append('nama_strata' , $("#nama_strata").val());
                // data_main.append('strata' , $("#strata").val());
                                            
                $.ajax({
                    url: "<?php echo base_url()."super/act/add/pd_strata";?>",
                    dataType: 'html',  // what to expect back from the PHP script, if anything
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: data_main,                         
                    type: 'post',
                    success: function(res){
                        // console.log(res);
                        response_insert_strata(res);
                    }
                });
            });

            function response_insert_strata(res){
                var data_json = JSON.parse(res);
                console.log(data_json);
                    var main_msg = data_json.msg_main;
                    var detail_msg = data_json.msg_detail;
                if(main_msg.status){
                    !function($) {
                        "use strict";
                        var SweetAlert = function() {};
                        //examples 
                        SweetAlert.prototype.init = function() {
                            //Warning Message
                            swal({   
                                title: "Proses Berhasil.!!",   
                                text: "Data admin berhasil disimpan ..!",   
                                type: "success",   
                                showCancelButton: false,   
                                confirmButtonColor: "#28a745",   
                                confirmButtonText: "Lanjutkan",   
                                closeOnConfirm: false 
                            }, function(){
                                window.location.href = "<?php echo base_url()."admin/super/pd_jenis";?>";
                            });                              
                        },
                        //init
                        $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                    }(window.jQuery),
                    
                    function($) {
                        "use strict";
                        $.SweetAlert.init()
                    }(window.jQuery);
                }else{
                    !function($) {
                        "use strict";
                        var SweetAlert = function() {};
                        //examples 
                        SweetAlert.prototype.init = function() {
                            //Warning Message
                            // $("#msg_nama_jenis").html(detail_msg.nama_jenis);
                            // $("#msg_strata").html(detail_msg.strata);

                            swal("Proses Gagal.!!", "Data admin gagal disimpan, coba periksa jaringan dan koneksi anda", "warning");                   
                        },
                                                  
                        $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                    }(window.jQuery),

                    function($) {
                        "use strict";
                        $.SweetAlert.init()
                    }(window.jQuery);
                    
                }
            }
    //========================================================================
    //--------------------------------Insert_strata---------------------------
    //========================================================================
    
    //========================================================================
    //--------------------------------get_update_strata-----------------------
    //========================================================================
    
        function get_update_strata(param){
            clear_from_update();

            var data_main =  new FormData();
            data_main.append('id_strata', param);
                                                
                $.ajax({
                    url: "<?php echo base_url()."super/act/get/pd_strata";?>",
                    dataType: 'html',  // what to expect back from the PHP script, if anything
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: data_main,                         
                    type: 'post',
                    success: function(res){
                        // console.log(res);
                        set_val_update_strata(res, param);
                    }
                });
        }

        function set_val_update_strata(res, param){
            var res_pemohon = JSON.parse(res.toString());

            if(res_pemohon.status == true){
                var id_jenis_chahce = res_pemohon.val_response.id_strata;
                $("#nama_strata").val(res_pemohon.val_response.nama_strata);

                id_jenis_glob_strata = id_jenis_chahce;

                $("#up_strata").removeAttr("hidden", true);
                $("#cancel_strata").removeAttr("hidden", true);

                $("#add_strata").attr("hidden", true);
            }else {
                $("#nama_strata").val("");
            }
        }
    //========================================================================
    //--------------------------------get_update_strata-----------------------
    //========================================================================

    //========================================================================
    //--------------------------------update_strata---------------------------
    //========================================================================
        $("#up_strata").click(function(){
            var data_main = new FormData();
            data_main.append('nama_strata' , $("#nama_strata").val());
            data_main.append('id_strata', id_jenis_glob_strata);

            $.ajax({
                url: "<?php echo base_url()."super/act/up/pd_strata";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    // console.log(res);
                    response_update_strata(res);
                }
            });
        });

        function response_update_strata(res) {
            var data_json = JSON.parse(res);

            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            if (main_msg.status) {
                ! function($) {
                    "use strict";
                    var SweetAlert = function() {};
                    //examples 
                    SweetAlert.prototype.init = function() {
                            //Warning Message
                            swal({
                                title: "Proses Berhasil.!!",
                                text: "Data admin berhasil disimpan ..!",
                                type: "success",
                                showCancelButton: false,
                                confirmButtonColor: "#28a745",
                                confirmButtonText: "Lanjutkan",
                                closeOnConfirm: false
                            }, function() {
                                window.location.href = "<?php echo base_url()."admin/super/pd_jenis";?>";
                            });
                        },
                        //init
                        $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                }(window.jQuery),

                function($) {
                    "use strict";
                    $.SweetAlert.init()
                }(window.jQuery);
            } else {
                ! function($) {
                    "use strict";
                    var SweetAlert = function() {};
                    //examples 
                    SweetAlert.prototype.init = function() {
                            //Warning Message
                            // $("#_msg_nama_jenis").html(detail_msg.nama_jenis);
                            // $("#_msg_strata").html(detail_msg.strata);

                            swal("Proses Gagal.!!", "Data admin gagal disimpan, coba periksa jaringan dan koneksi anda", "warning");
                        },

                        $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                }(window.jQuery),

                function($) {
                    "use strict";
                    $.SweetAlert.init()
                }(window.jQuery);

            }
        }
    //========================================================================
    //--------------------------------update_strata---------------------------
    //========================================================================
    
    //========================================================================
    //--------------------------------delete_strata---------------------------
    //========================================================================
    
        function delete_strata(param){
                !function($) {
                    "use strict";
                    var SweetAlert = function() {};
                    //examples 
                    SweetAlert.prototype.init = function() {
                        //Warning Message
                        swal({   
                            title: "Pesan Konfirmasi",   
                            text: "Silahkan Cermati data sebelem di hapus permanen, jika anda sudah yakin maka data ini dan seluruh data yang berkaitan akan di hapus",   
                            type: "warning",   
                            showCancelButton: true,   
                            confirmButtonColor: "#ffb22b",   
                            confirmButtonText: "Hapus",   
                            closeOnConfirm: true 
                        }, function(){
                            
                            var data_main =  new FormData();
                            data_main.append('id_strata', param);
                                                        
                            $.ajax({
                                url: "<?php echo base_url()."super/act/del/pd_strata";?>",
                                dataType: 'html',  // what to expect back from the PHP script, if anything
                                cache: false,
                                contentType: false,
                                processData: false,
                                data: data_main,                         
                                type: 'post',
                                success: function(res){
                                    console.log(res);
                                    swal("Proses Berhasil.!!", "Penghapusan Data Berhasil", "success");
                                    location.href="<?php print_r(base_url()."admin/super/pd_jenis");?>";
                                }
                            });   
                        });                                     
                    },
                                              
                    $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                }(window.jQuery),

                function($) {
                    "use strict";
                    $.SweetAlert.init()
                }(window.jQuery);
        }
    //========================================================================
    //--------------------------------delete_strata---------------------------
    //========================================================================

    //========================================================================
    //--------------------------------close_update_strata---------------------
    //========================================================================
        $("#cancel_strata").click(function(){
            $("#up_strata").attr("hidden", true);
            $("#cancel_strata").attr("hidden", true);

            $("#add_strata").removeAttr("hidden", true);

            id_jenis_glob_strata = "";
            $("#nama_strata").val("");        
        });
    //========================================================================
    //--------------------------------close_update_strata---------------------
    //========================================================================
    
//----------------------------------------strata--------------------------------------


//----------------------------------------Jenis--------------------------------------

    //========================================================================
    //--------------------------------Insert_data-----------------------------
    //========================================================================

            $("#btn_insert_data").click(function(){
                var data_main =  new FormData();
                data_main.append('nama_jenis' , $("#nama_jenis").val());
                data_main.append('strata' , $("#strata").val());
                                            
                $.ajax({
                    url: "<?php echo base_url()."super/act/add/pd_jenis";?>",
                    dataType: 'html',  // what to expect back from the PHP script, if anything
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: data_main,                         
                    type: 'post',
                    success: function(res){
                        // console.log(res);
                        response_insert(res);
                    }
                });
            });

            function response_insert(res){
                var data_json = JSON.parse(res);
                console.log(data_json);
                    var main_msg = data_json.msg_main;
                    var detail_msg = data_json.msg_detail;
                if(main_msg.status){
                    !function($) {
                        "use strict";
                        var SweetAlert = function() {};
                        //examples 
                        SweetAlert.prototype.init = function() {
                            //Warning Message
                            swal({   
                                title: "Proses Berhasil.!!",   
                                text: "Data admin berhasil disimpan ..!",   
                                type: "success",   
                                showCancelButton: false,   
                                confirmButtonColor: "#28a745",   
                                confirmButtonText: "Lanjutkan",   
                                closeOnConfirm: false 
                            }, function(){
                                window.location.href = "<?php echo base_url()."admin/super/pd_jenis";?>";
                            });                              
                        },
                        //init
                        $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                    }(window.jQuery),
                    
                    function($) {
                        "use strict";
                        $.SweetAlert.init()
                    }(window.jQuery);
                }else{
                    !function($) {
                        "use strict";
                        var SweetAlert = function() {};
                        //examples 
                        SweetAlert.prototype.init = function() {
                            //Warning Message
                            $("#msg_nama_jenis").html(detail_msg.nama_jenis);
                            $("#msg_strata").html(detail_msg.strata);

                            swal("Proses Gagal.!!", "Data admin gagal disimpan, coba periksa jaringan dan koneksi anda", "warning");                   
                        },
                                                  
                        $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                    }(window.jQuery),

                    function($) {
                        "use strict";
                        $.SweetAlert.init()
                    }(window.jQuery);
                    
                }
            }
    //========================================================================
    //--------------------------------Insert_data-----------------------------
    //========================================================================

    var id_jenis_glob = "";

    //========================================================================
    //--------------------------------Get_Update_data-------------------------
    //========================================================================
        function clear_from_update(){
            $("#_nama_jenis").val("");
            $("#_strata").val("");
            id_jenis_glob = "";
        }

        function get_update_jenis(param){
            clear_from_update();

            var data_main =  new FormData();
            data_main.append('id_jenis', param);
                                            
            $.ajax({
                url: "<?php echo base_url()."super/act/get/pd_jenis";?>",
                dataType: 'html',  // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,                         
                type: 'post',
                success: function(res){
                    // console.log(res);
                    set_val_update(res, param);
                    $("#update_data").modal('show');
                }
            });
        }

        function set_val_update(res, param){
            var res_pemohon = JSON.parse(res.toString());

            if(res_pemohon.status == true){
                var id_jenis_chahce = res_pemohon.val_response.id_jenis;
                $("#_nama_jenis").val(res_pemohon.val_response.nama_jenis);
                $("#_strata").val(res_pemohon.val_response.strata);

                id_jenis_glob = id_jenis_chahce;
            }else {
                clear_from_update();
            }
        }
    //========================================================================
    //--------------------------------Get_Update_data-------------------------
    //========================================================================

    //========================================================================
    //--------------------------------Update_data-----------------------------
    //========================================================================
       
        $("#btn_update_data").click(function() {
            var data_main = new FormData();
            data_main.append('nama_jenis' , $("#_nama_jenis").val());
            data_main.append('strata' , $("#_strata").val());

            data_main.append('id_jenis', id_jenis_glob);

            $.ajax({
                url: "<?php echo base_url()."super/act/up/pd_jenis";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    console.log(res);
                    response_update(res);
                }
            });
        });

        function response_update(res) {
            var data_json = JSON.parse(res);

            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            if (main_msg.status) {
                ! function($) {
                    "use strict";
                    var SweetAlert = function() {};
                    //examples 
                    SweetAlert.prototype.init = function() {
                            //Warning Message
                            swal({
                                title: "Proses Berhasil.!!",
                                text: "Data admin berhasil disimpan ..!",
                                type: "success",
                                showCancelButton: false,
                                confirmButtonColor: "#28a745",
                                confirmButtonText: "Lanjutkan",
                                closeOnConfirm: false
                            }, function() {
                                window.location.href = "<?php echo base_url()."admin/super/pd_jenis";?>";
                            });
                        },
                        //init
                        $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                }(window.jQuery),

                function($) {
                    "use strict";
                    $.SweetAlert.init()
                }(window.jQuery);
            } else {
                ! function($) {
                    "use strict";
                    var SweetAlert = function() {};
                    //examples 
                    SweetAlert.prototype.init = function() {
                            //Warning Message
                            $("#_msg_nama_jenis").html(detail_msg.nama_jenis);
                            $("#_msg_strata").html(detail_msg.strata);

                            swal("Proses Gagal.!!", "Data admin gagal disimpan, coba periksa jaringan dan koneksi anda", "warning");
                        },

                        $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                }(window.jQuery),

                function($) {
                    "use strict";
                    $.SweetAlert.init()
                }(window.jQuery);

            }
        }
    //========================================================================
    //--------------------------------Update_data-----------------------------
    //========================================================================

    //========================================================================
    //--------------------------------delete_data-----------------------------
    //========================================================================
            function delete_jenis(id_jenis){
                !function($) {
                    "use strict";
                    var SweetAlert = function() {};
                    //examples 
                    SweetAlert.prototype.init = function() {
                        //Warning Message
                        swal({   
                            title: "Pesan Konfirmasi",   
                            text: "Silahkan Cermati data sebelem di hapus permanen, jika anda sudah yakin maka data ini dan seluruh data yang berkaitan akan di hapus",   
                            type: "warning",   
                            showCancelButton: true,   
                            confirmButtonColor: "#ffb22b",   
                            confirmButtonText: "Hapus",   
                            closeOnConfirm: true 
                        }, function(){
                            
                            var data_main =  new FormData();
                            data_main.append('id_jenis', id_jenis);
                                                        
                            $.ajax({
                                url: "<?php echo base_url()."super/act/del/pd_jenis";?>",
                                dataType: 'html',  // what to expect back from the PHP script, if anything
                                cache: false,
                                contentType: false,
                                processData: false,
                                data: data_main,                         
                                type: 'post',
                                success: function(res){
                                    console.log(res);
                                    swal("Proses Berhasil.!!", "Penghapusan Data Berhasil", "success");
                                    location.href="<?php print_r(base_url()."admin/super/pd_jenis");?>";
                                }
                            });   
                        });                                     
                    },
                                              
                    $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                }(window.jQuery),

                function($) {
                    "use strict";
                    $.SweetAlert.init()
                }(window.jQuery);
            }
    //========================================================================
    //--------------------------------delete_data-----------------------------
    //========================================================================

//----------------------------------------Jenis--------------------------------------

</script>
