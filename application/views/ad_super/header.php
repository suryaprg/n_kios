<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?=base_url()?>admin_template/assets/images/favicon.png">
    <title>Apel_Mas_Admin</title>
    <!-- Bootstrap Core CSS -->
    <link href="<?=base_url()?>admin_template/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- chartist CSS -->
    <link href="<?=base_url()?>admin_template/assets/plugins/chartist-js/dist/chartist.min.css" rel="stylesheet">
    <link href="<?=base_url()?>admin_template/assets/plugins/chartist-js/dist/chartist-init.css" rel="stylesheet">
    <link href="<?=base_url()?>admin_template/assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css" rel="stylesheet">
    <!-- morris CSS -->
    <link href="<?=base_url()?>admin_template/assets/plugins/morrisjs/morris.css" rel="stylesheet">
    <!-- Vector CSS -->
    <link href="<?=base_url()?>admin_template/assets/plugins/vectormap/jquery-jvectormap-2.0.2.css" rel="stylesheet" />
    <!-- Custom CSS -->
    <link href="<?=base_url()?>admin_template/main/css/style.css" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="<?=base_url()?>admin_template/main/css/colors/blue.css" id="theme" rel="stylesheet">
    <!--alerts CSS -->
    <link href="<?php print_r(base_url());?>admin_template/assets/plugins/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
    <link href="<?php print_r(base_url());?>admin_template/assets/plugins/icheck/skins/all.css" rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="<?=base_url()?>assets/js/jquery-3.2.1.js"></script>

    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<?php
        $nama_admin = "";
        $email_admin = "";
        $dinas_admin = "";
        $lv_admin = "";

        if(isset($_SESSION["admin_lv_1"])){
            if($_SESSION["admin_lv_1"]){
                $nama_admin = $_SESSION["admin_lv_1"]["nama"];
                $email_admin = $_SESSION["admin_lv_1"]["email"];
                $dinas_admin = $_SESSION["admin_lv_1"]["nama_dinas"];
                $lv_admin = $_SESSION["admin_lv_1"]["ket"];
            }
        }
?>
<body class="fix-sidebar fix-header card-no-border">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-light">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="index.html">
                        <!-- Logo icon --><b>
                            <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                            <!-- Dark Logo icon -->
                            <img src="<?=base_url()?>admin_template/assets/images/logo-icon.png" alt="homepage" class="dark-logo" />
                            <!-- Light Logo icon -->
                            <img src="<?=base_url()?>admin_template/assets/images/logo-light-icon.png" alt="homepage" class="light-logo" />
                        </b>
                        <!--End Logo icon -->
                        <!-- Logo text --><span>
                         <!-- dark Logo text -->
                         <img src="<?=base_url()?>admin_template/assets/images/logo-text.png" alt="homepage" class="dark-logo" />
                         <!-- Light Logo text -->    
                         <img src="<?=base_url()?>admin_template/assets/images/logo-light-text.png" class="light-logo" alt="homepage" /></span> </a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav mr-auto mt-md-0">
                        <!-- This is  -->
                        <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="mdi mdi-menu"></i></a> </li>
                        <li class="nav-item m-l-10"> <a class="nav-link sidebartoggler hidden-sm-down text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                        
                    </ul>
                    <!-- ============================================================== -->
                    <!-- User profile and search -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav my-lg-0">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php print_r($lv_admin.", ".$dinas_admin)?></a>
                            <div class="dropdown-menu dropdown-menu-right scale-up">
                                <ul class="dropdown-user">
                                    <li>
                                        <div class="dw-user-box">
                                            <div class="u-text">
                                                <h4><?php print_r($nama_admin)?></h4>
                                                <p class="text-muted"><?php print_r($email_admin)?></p>
                                            </div>
                                        </div>
                                    </li>
                                    <li><a href="<?php print_r(base_url());?>back-admin/logout"><i class="fa fa-power-off"></i> Logout</a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- User profile -->
                <div class="user-profile">
                    <!-- User profile text-->
                    <div class="profile-text"> 
                            <h5><?php print_r($nama_admin)?></h5>
                            <center><a href="<?= base_url();?>back-admin/logout" class="" data-toggle="tooltip" title="Logout"><i class="mdi mdi-power"></i></a></center>
                    </div>
                </div>
                <!-- End User profile text-->
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li class="nav-devider"></li>
                        <li class="nav-small-cap">MENU ADMIN DAN HAK AKSES</li>
                        <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-account-network"></i><span class="hide-menu">Admin</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="<?=base_url()?>admin/super/add_admin">Data Admin</a></li>
                                <li><a href="<?=base_url()?>admin/super/dinas">Data Dinas</a></li>
                            </ul>
                        </li>

                        <li class="nav-small-cap">MENU DAFTAR LAYANAN KIOS</li>
                        <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-format-list-bulleted-type"></i><span class="hide-menu">Data List Menu</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="<?=base_url()?>admin/super/halaman_menu">Data Menu Layanan</a></li>
                            </ul>
                        </li>

                        <li class="nav-small-cap">MENU MASTER ANTRIAN</li>
                        <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-heart-pulse"></i><span class="hide-menu">Antrian Kesehatan</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="<?=base_url()?>admin/super/jenis_rumah_sakit">Data Jenis Rumah Sakit</a></li>
                                <li><a href="<?=base_url()?>admin/super/poli">Data Poli</a></li>
                                <li><a href="<?=base_url()?>admin/super/rumah_sakit">Data Rumah Sakit</a></li>
                            </ul>
                        </li>
                        <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-human-male-female"></i><span class="hide-menu">Kependudukan</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="<?=base_url()?>admin/super/kependudukan_kategori">Data Kategori Kependudukan</a></li>
                                <li><a href="<?=base_url()?>admin/super/kependudukan_jenis">Data Jenis Kependudukan</a></li>
                            </ul>
                        </li>
                        <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-truck-delivery"></i><span class="hide-menu">Data Macito</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="<?=base_url()?>admin/super/macito">Data Menu Layanan</a></li>
                            </ul>
                        </li>
                        <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-clipboard-text"></i><span class="hide-menu">Perijinan</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="<?=base_url()?>admin/super/perijinan_kategori">Data Kategori Perijinan</a></li>
                                <li><a href="<?=base_url()?>admin/super/perijinan_sub_kategori">Data Sub. Kategori Perijinan</a></li>
                                <li><a href="<?=base_url()?>admin/super/perijinan_jenis">Data Jenis Perijinan</a></li>
                            </ul>
                        </li>


                        <li class="nav-small-cap">MENU MASTER INFORMASI</li>
                        <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-school"></i><span class="hide-menu">Pendidikan</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="<?=base_url()?>admin/super/pd_strata">Data Strata Pendidikan</a></li>
                                <li><a href="<?=base_url()?>admin/super/pd_jenis">Data Jenis Pendidikan</a></li>
                                <li><a href="<?=base_url()?>admin/super/pd_sekolah">Data Sekolah</a></li>
                            </ul>
                        </li>

                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            