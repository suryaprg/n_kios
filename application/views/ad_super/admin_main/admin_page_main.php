<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor">Halaman Daftar Menu</h3>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- Row -->
    <div class="row">

        <div class="col-12 m-t-30">
            <!-- Card -->
            <div class="card card-outline-info">
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <h4 class="card-title">Daftar Menu</h4>
                        </div>
                        <div class="col-lg-6 text-right">
                            <button type="button" class="btn btn-rounded btn-success" data-toggle="modal" data-target="#insert_page_kategori">
                                <i class="fa fa-plus-circle"></i>&nbsp;&nbsp;&nbsp;Tambah Kategori Menu
                            </button>
                            <button type="button" class="btn btn-rounded btn-info" data-toggle="modal" data-target="#insert_page_main">
                                <i class="fa fa-plus-circle"></i>&nbsp;&nbsp;&nbsp;Tambah Daftar Menu
                            </button>
                        </div>
                    </div>

                    
                            <div class="table-responsive m-t-40">
                                <table id="myTable" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th width="5%">No. </th>
                                            <th width="*">Keterangan Menu</th>
                                            <th width="20%">Kategori </th>
                                            <th width="20%">Foto</th>
                                            <th width="20%">Halaman Selanjutnya</th>
                                            <th width="15%">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                                        if(!empty($list_menu_main)){
                                                            foreach ($list_menu_main as $r_list_menu_main => $v_list_menu_main) {
                                                                echo "<tr>
                                                                        <td>".($r_list_menu_main+1)."</td>
                                                                        <td>".$v_list_menu_main->nama_page."</td>
                                                                        <td>".$v_list_menu_main->nama_kategori."</td>
                                                                        <td><img id=\"img_list_menu\" src=\"".base_url()."assets/core_img/icon_menu_layanan/".$v_list_menu_main->foto_page."\"</td>
                                                                        <td>".$v_list_menu_main->next_page."</td>
                                                                        <td>
                                                                        <center>
                                                                            <button type=\"button\" class=\"btn btn-info\" id=\"up_page_main\" onclick=\"update_page_main('".$this->encrypt->encode($v_list_menu_main->id_page)."')\" style=\"width: 40px;\">
                                                                                <i class=\"fa fa-pencil-square-o\"></i>
                                                                            </button>

                                                                            <button type=\"button\" class=\"btn btn-danger\" id=\"del_page_main\" onclick=\"delete_page_main('".$this->encrypt->encode($v_list_menu_main->id_page)."')\" style=\"width: 40px;\">
                                                                                <i class=\"fa fa-trash-o\"></i>
                                                                            </button>
                                                                        </center>
                                                                        </td>
                                                                    </tr>";
                                                            }
                                                        }
                                                    ?>

                                    </tbody>
                                </table>
                            </div>
                        
                </div>
                <div class="card-body">
                    <div class="text-right">
                        <label class="form-label">Keterangan Tombol Aksi ==> </label>

                        <a class="btn btn-info" style="width: 40px;"><i class="fa fa-pencil-square-o" style="color: white;"></i></a>
                        <label class="form-label text-info">Update Data</label>,&nbsp;
                        <a class="btn btn-danger" style="width: 40px;"><i class="fa fa-trash-o" style="color: white;"></i></a>
                        <label class="form-label text-danger">Delete Data</label>
                    </div>
                </div>
            </div>
            <!-- Card -->
        </div>
    </div>
    <!-- End Row -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- --------------------------insert_page_kategori---------------- -->
<!-- ============================================================== -->
    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog"  id="insert_page_kategori" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel1">Form Tambah Kategori Menu</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                                            
                <div class="modal-body">
                    <div id="slimtest2">
                        <div class="table">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th width="5%">No. </th>
                                        <th width="75%">Kategori Menu</th>
                                        <th width="20%">Aksi</th>
                                    </tr>
                                    <tr>
                                        <td>#</td>
                                        <td>
                                            <input type="input" class="form-control" name="ket_kategori" id="ket_kategori" placeholder="Keterangan">
                                            <p id="msg_ket_kategori" style="color: red;"></p>
                                        </td>
                                        <td>
                                            <center>
                                            <button name="add_kategori" id="add_kategori" class="btn btn-info"><i class="fa fa-plus"></i></button>
                                            </center>
                                        </td>
                                    </tr>
                                </thead>
                                <tbody id="out_act_kategori">
                                    <?php
                                        foreach ($list_menu_kategori as $r_list_menu_kategori => $v_list_menu_kategori) {
                                            print_r("<tr>
                                                        <td>".($r_list_menu_kategori+1)."</td>
                                                        <td>".$v_list_menu_kategori->nama_kategori."</td>
                                                        <td>
                                                            <center>
                                                                <button type=\"button\" class=\"btn btn-danger\" id=\"del_page_kategori\" onclick=\"delete_page_kategori('".$this->encrypt->encode($v_list_menu_kategori->id_kategori)."')\" style=\"width: 40px;\">
                                                                    <i class=\"fa fa-trash-o\"></i></button>
                                                            </center>                
                                                        </td>
                                                    </tr>");
                                        }

                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- ============================================================== -->
<!-- --------------------------insert_page_kategori---------------- -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- --------------------------insert_page_main-------------------- -->
<!-- ============================================================== -->

    <div class="modal fade" id="insert_page_main" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel1">Form Tambah Halaman Menu</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">

                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Keterangan Menu :</label>
                        <input type="text" class="form-control" id="nama_page" name="nama_page" required="">
                        <p id="msg_nama_page" style="color: red;"></p>
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="control-label">Kategori Menu :</label>
                        <select class="form-control" id="kategori" name="kategori" required="">
                            
                            <?php
                                if($list_menu_kategori){
                                    foreach ($list_menu_kategori as $r_list_menu_kategori => $v_list_menu_kategori) {
                                        print_r("<option value=\"".$v_list_menu_kategori->id_kategori."\">".$v_list_menu_kategori->nama_kategori."</option>");
                                    }
                                }
                            ?>  
                        </select>
                        <p id="msg_kategori" style="color: red;"></p>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Link Halaman Selanjutnya</label>
                        <input type="text" class="form-control" id="next_page" name="next_page" required="">
                        <p id="msg_next_page" style="color: red;"></p>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Foto Menu :</label>
                        <input type="file" class="form-control" id="foto_page" name="foto_page" required="">
                    </div>
                    
                    <center><img id="img_foto_page" src="" style="width: 259px; height: 173px;"></center>
                    <p id="msg_foto_page" style="color: red;"></p>
                </div>
                <div class="modal-footer">
                    <button id="add_page_main" type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </div>
        </div>
    </div>
<!-- ============================================================== -->
<!-- --------------------------insert_page_main-------------------- -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- --------------------------update_page_main-------------------- -->
<!-- ============================================================== -->

    <div class="modal fade" id="update_page_main_" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel1">Form Ubah Halaman Menu</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">

                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Keterangan Menu :</label>
                        <input type="text" class="form-control" id="_nama_page" name="nama_page" required="">
                        <p id="_msg_nama_page" style="color: red;"></p>
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="control-label">Kategori Menu :</label>
                        <select class="form-control" id="_kategori" name="kategori" required="">
                            <?php
                                if($list_menu_kategori){
                                    foreach ($list_menu_kategori as $r_list_menu_kategori => $v_list_menu_kategori) {
                                        print_r("<option value=\"".$v_list_menu_kategori->id_kategori."\">".$v_list_menu_kategori->nama_kategori."</option>");
                                    }
                                }
                            ?>  
                        </select>
                        <p id="_msg_kategori" style="color: red;"></p>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Link Halaman Selanjutnya</label>
                        <input type="text" class="form-control" id="_next_page" name="next_page" required="">
                        <p id="_msg_next_page" style="color: red;"></p>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Foto Menu :</label>
                        <input type="file" class="form-control" id="_foto_page" name="foto_page" required="">
                    </div>

                    <center><img id="_img_foto_page" src="" style="width: 259px; height: 173px;"></center>
                    <p id="_msg_foto_page" style="color: red;"></p>
                    
                </div>
                <div class="modal-footer">
                    <button id="up_page_main_" type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </div>
        </div>
    </div>
    <!-- /.modal -->
<!-- ============================================================== -->
<!-- --------------------------update_page_main-------------------- -->
<!-- ============================================================== -->

<script type="text/javascript">
    var id_page_menu_gl;
    //==========================================================================================//
    //----------------------------------------------------Insert_page_kategori------------------//
    //==========================================================================================//
        var file = [];
        $("#foto_page").change(function(e){
            file = e.target.files[0];

            $("#img_foto_page").attr("src",URL.createObjectURL(file));
            console.log(file);
        });

        $("#add_kategori").click(function(){
            var data_main = new FormData();
            data_main.append('nama_page', $("#nama_page").val());
            data_main.append('kategori', $("#kategori").val());
            data_main.append('next_page', $("#next_page").val());
            data_main.append('foto_page', file);

            $.ajax({
                url: "<?php echo base_url()."super/act/add/menu_main";?>", // point to server-side PHP script 
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    response_add_kategori(res);
                }
            });
        });

        function response_add_kategori(res) {
            var data_json = JSON.parse(res);
                var main_msg = data_json.msg_main;
                var detail_msg = data_json.msg_detail;
                
            if (main_msg.status) {
                ! function($) {
                    "use strict";
                    var SweetAlert = function() {};
                    //examples 
                    SweetAlert.prototype.init = function() {
                            swal({   
                                title: "Proses Berhasil.!!",   
                                text: "Data kategori halaman berhasil disimpan ..!",   
                                type: "success",   
                                showCancelButton: false,   
                                confirmButtonColor: "#28a745",   
                                confirmButtonText: "Lanjutkan",   
                                closeOnConfirm: false 
                            }, function(){
                                window.location.href = "<?php echo base_url()."admin/super/halaman_menu";?>";
                            });  
                        },
                        //init
                        $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                }(window.jQuery),

                function($) {
                    "use strict";
                    $.SweetAlert.init()
                }(window.jQuery);
            } else {
                ! function($) {
                    "use strict";
                    var SweetAlert = function() {};
                    //examples 
                    SweetAlert.prototype.init = function() {
                            //Warning Message
                            $("#msg_ket_kategori").html(detail_msg.nama_kategori);

                            swal("Proses Gagal.!!", "Data Menu Kategori gagal disimpan, coba periksa jaringan dan koneksi anda", "warning");
                        },

                        $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                }(window.jQuery),

                function($) {
                    "use strict";
                    $.SweetAlert.init()
                }(window.jQuery);

            }
        }
    //==========================================================================================//
    //----------------------------------------------------Insert_page_kategori------------------//
    //==========================================================================================// 

    //==========================================================================================//
    //----------------------------------------------------Update_page_kategori------------------//
    //==========================================================================================//
        function update_page_kategori(param){

        }
    //==========================================================================================//
    //----------------------------------------------------Update_page_kategori------------------//
    //==========================================================================================// 

    //==========================================================================================//
    //----------------------------------------------------Delete_page_kategori------------------//
    //==========================================================================================//
        function delete_page_kategori(param){
                ! function($) {
                    "use strict";
                    var SweetAlert = function() {};
                    //examples 
                    SweetAlert.prototype.init = function() {
                            swal({   
                                title: "Proses Berhasil.!!",   
                                text: "Data kategori halaman berhasil hapus ..!",   
                                type: "warning",   
                                showCancelButton: true,   
                                confirmButtonColor: "#d39e00",   
                                confirmButtonText: "Hapus",   
                                closeOnConfirm: false 
                            }, function(){
                                var data_main = new FormData();
                                data_main.append('id_kategori', param);

                                $.ajax({
                                    url: "<?php echo base_url()."super/act/del/menu_kategori";?>", // point to server-side PHP script 
                                    dataType: 'html', // what to expect back from the PHP script, if anything
                                    cache: false,
                                    contentType: false,
                                    processData: false,
                                    data: data_main,
                                    type: 'post',
                                    success: function(res) {
                                        response_delete_kategori(res);
                                        // console.log(res);
                                    }
                                });
                            });  
                        },
                        //init
                        $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                }(window.jQuery),

                function($) {
                    "use strict";
                    $.SweetAlert.init()
                }(window.jQuery);
            
        }

        function response_delete_kategori(res) {
            var data_json = JSON.parse(res);
                var main_msg = data_json.msg_main;
                var detail_msg = data_json.msg_detail;
                
            if (main_msg.status) {
                ! function($) {
                    "use strict";
                    var SweetAlert = function() {};
                    //examples 
                    SweetAlert.prototype.init = function() {
                            swal({   
                                title: "Proses Berhasil.!!",   
                                text: "Data kategori halaman berhasil dihapus ..!",   
                                type: "success",   
                                showCancelButton: false,   
                                confirmButtonColor: "#28a745",   
                                confirmButtonText: "Lanjutkan",   
                                closeOnConfirm: false 
                            }, function(){
                                window.location.href = "<?php echo base_url()."admin/super/halaman_menu";?>";
                            });  
                        },
                        //init
                        $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                }(window.jQuery),

                function($) {
                    "use strict";
                    $.SweetAlert.init()
                }(window.jQuery);
            } else {
                ! function($) {
                    "use strict";
                    var SweetAlert = function() {};
                    //examples 
                    SweetAlert.prototype.init = function() {
                            //Warning Message

                            swal("Proses Gagal.!!", "Data Menu Kategori gagal dihapus, coba periksa jaringan dan koneksi anda", "warning");
                        },

                        $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                }(window.jQuery),

                function($) {
                    "use strict";
                    $.SweetAlert.init()
                }(window.jQuery);

            }
        }
    //==========================================================================================//
    //----------------------------------------------------Delete_page_kategori------------------//
    //==========================================================================================// 






    //==========================================================================================//
    //----------------------------------------------------Insert_page_menu----------------------//
    //==========================================================================================//
        var file = [];
        $("#foto_page").change(function(e){
            file = e.target.files[0];

            $("#img_foto_page").attr("src",URL.createObjectURL(file));
        });  

        $("#add_page_main").click(function() {
            var data_main = new FormData();
            data_main.append('nama_page', $("#nama_page").val());
            data_main.append('kategori', $("#kategori").val());
            data_main.append('foto_page', file);
            data_main.append('next_page', $("#next_page").val());

            $.ajax({
                url: "<?php echo base_url()."super/act/add/menu_main";?>", // point to server-side PHP script 
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    response_add_page_main(res);
                }
            });
        });

        function response_add_page_main(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            if (main_msg.status) {
                ! function($) {
                    "use strict";
                    var SweetAlert = function() {};
                    //examples 
                    SweetAlert.prototype.init = function() {
                            //Warning Message
                            swal({
                                title: "Proses Berhasil.!!",
                                text: "Data Menu berhasil disimpan ..!",
                                type: "success",
                                showCancelButton: false,
                                confirmButtonColor: "#28a745",
                                confirmButtonText: "Lanjutkan",
                                closeOnConfirm: false
                            }, function() {
                                window.location.href = "<?php echo base_url()."admin/super/halaman_menu";?>";
                            });
                        },
                        //init
                        $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                }(window.jQuery),

                function($) {
                    "use strict";
                    $.SweetAlert.init()
                }(window.jQuery);
            } else {
                ! function($) {
                    "use strict";
                    var SweetAlert = function() {};
                    //examples 
                    SweetAlert.prototype.init = function() {
                            //Warning Message
                            $("#msg_nama_page").html(detail_msg.nama_page);
                            $("#msg_kategori").html(detail_msg.nama_kategori);
                            $("#msg_foto_page").html(detail_msg.foto_page);
                            $("#msg_next_page").html(detail_msg.next_page);

                            swal("Proses Gagal.!!", "Data Menu gagal disimpan, coba periksa jaringan dan koneksi anda", "warning");
                        },

                        $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                }(window.jQuery),

                function($) {
                    "use strict";
                    $.SweetAlert.init()
                }(window.jQuery);

            }
        }
    //==========================================================================================//
    //----------------------------------------------------Insert_page_menu----------------------//
    //==========================================================================================//

    //==========================================================================================//
    //----------------------------------------------------Get_page_menu-------------------------//
    //==========================================================================================//

        function update_page_main(id_page_main) {
            clear_from_update();

            var data_main = new FormData();
            data_main.append('id_page', id_page_main);

            $.ajax({
                url: "<?php echo base_url()."super/act/get/menu_main";?>", // point to server-side PHP script 
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    set_val_update(res, id_page_main);
                    $("#update_page_main_").modal('show');
                }
            });
        }

        function set_val_update(res, id_page_main) {
            var res_pemohon = JSON.parse(res);
            id_page_menu_gl = id_page_main;

            if (res_pemohon.status == true) {
                $("#_nama_page").val(res_pemohon.val_response.nama_page);
                $("#_kategori").val(res_pemohon.val_response.id_kategori);
                $("#_img_foto_page").prop("src",res_pemohon.val_response.foto_page);
                $("#_next_page").val(res_pemohon.val_response.next_page);
            } else {
                clear_from_update();
            }
        }

        function clear_from_update() {
            $("#_nama_page").val("");
            $("#_kategori").val("");
            $("#_img_foto_page").val("");
            $("#_nama_page").val("");
        }
    //==========================================================================================//
    //----------------------------------------------------Get_page_menu-------------------------//
    //==========================================================================================//

    //==========================================================================================//
    //----------------------------------------------------Update_page_main----------------------//
    //==========================================================================================//
        var file = [];
        $("#_foto_page").change(function(e){
            file = e.target.files[0];

            $("#_img_foto_page").attr("src",URL.createObjectURL(file));
        });

        $("#up_page_main_").click(function() {
            var data_main = new FormData();
            
            data_main.append('nama_page', $("#_nama_page").val());
            data_main.append('kategori', $("#_kategori").val());
            data_main.append('foto_page', file);
            data_main.append('next_page', $("#_next_page").val());

            data_main.append('id_page', id_page_menu_gl);

            $.ajax({
                url: "<?php echo base_url()."super/act/up/menu_main";?>", // point to server-side PHP script 
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    response_update_page_main(res);
                }
            });
        });

        function response_update_page_main(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            if (main_msg.status) {
                ! function($) {
                    "use strict";
                    var SweetAlert = function() {};
                    //examples 
                    SweetAlert.prototype.init = function() {
                            //Warning Message
                            swal({
                                title: "Proses Berhasil.!!",
                                text: "Data menu berhasil diubah ..!",
                                type: "success",
                                showCancelButton: false,
                                confirmButtonColor: "#28a745",
                                confirmButtonText: "Lanjutkan",
                                closeOnConfirm: false
                            }, function() {
                                window.location.href = "<?php echo base_url()."admin/super/halaman_menu";?>";
                            });
                        },
                        //init
                        $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                }(window.jQuery),

                function($) {
                    "use strict";
                    $.SweetAlert.init()
                }(window.jQuery);
            } else {
                ! function($) {
                    "use strict";
                    var SweetAlert = function() {};
                    //examples 
                    SweetAlert.prototype.init = function() {
                            //Warning Message
                            $("#_msg_nama_page").html(detail_msg.nama_page);
                            $("#_msg_kategori").html(detail_msg.nama_kategori);
                            $("#_msg_foto_page").html(detail_msg.foto_page);
                            $("#_msg_next_page").html(detail_msg.next_page);

                            swal("Proses Gagal.!!", "Data menu gagal diubah, coba periksa jaringan dan koneksi anda", "warning");
                        },

                        $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                }(window.jQuery),

                function($) {
                    "use strict";
                    $.SweetAlert.init()
                }(window.jQuery);

            }
        }
    //==========================================================================================//
    //----------------------------------------------------Update_page_main----------------------//
    //==========================================================================================//

    //==========================================================================================//
    //----------------------------------------------------delete_page_kategori------------------//
    //==========================================================================================//
        function delete_page_main(id_page_main) {
            ! function($) {
                "use strict";
                var SweetAlert = function() {};
                //examples 
                SweetAlert.prototype.init = function() {
                        //Warning Message
                        swal({
                            title: "Pesan Konfirmasi",
                            text: "Silahkan Cermati data sebelum di hapus, jika anda sudah yakin maka data ini dan seluruh data yang berkaitan akan di hapus",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#ffb22b",
                            confirmButtonText: "Hapus",
                            closeOnConfirm: false
                        }, function() {
                            var data_main = new FormData();
                            data_main.append('id_page', id_page_main);

                            $.ajax({
                                url: "<?php echo base_url()."super/act/del/menu_main";?>", // point to server-side PHP script 
                                dataType: 'html', // what to expect back from the PHP script, if anything
                                cache: false,
                                contentType: false,
                                processData: false,
                                data: data_main,
                                type: 'post',
                                success: function(res) {
                                    response_delete_page_main(res);
                                }
                            });
                        });
                    },
                    //init
                    $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
            }(window.jQuery),

            function($) {
                "use strict";
                $.SweetAlert.init()
            }(window.jQuery);
        }

        function response_delete_page_main(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            if (main_msg.status) {
                swal({
                    title: "Proses Berhasil.!!",
                    text: "Data menu berhasil dihapus ..!",
                    type: "success",
                    showCancelButton: false,
                    confirmButtonColor: "#28a745",
                    confirmButtonText: "Lanjutkan",
                    closeOnConfirm: false
                }, function() {
                    window.location.href = "<?php echo base_url()."admin/super/halaman_menu";?>";
                });
            } else {

                swal("Proses Gagal.!!", "Data menu gagal dihapus, coba periksa jaringan dan koneksi anda", "warning");
            }
        }
    //==========================================================================================//
    //----------------------------------------------------delete_page_kategori------------------//
    //==========================================================================================//
</script>