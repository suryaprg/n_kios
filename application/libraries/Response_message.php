<?php
/**
 * @author Surya Hanggara (Filosofi_code)
 * @copyright 2019
 */
class Response_message{
    
    public function get_error_msg($id_message){
        $errors_msg = array(
            "REQUIRED"=>"Tidak boleh kosong",
            "NUMBER"=>"Input Salah, Input harus berupa angka",
            "NUMBER_CHAR"=>"Input Salah, Input tidak diperkenankan selain berupa angka dan huruf",
            "EXACT_LENGHT_NIK"=>"Input harus 16 digit",
            "EMAIL"=>"Input Email tidak valid, mohon input dengan benar",
            "PASSWORD_LENGHT"=>"Input tidak sesuai, maksimal karakter adalah 15 dan minimal karakter adalah 5",
            "NIK_AVAIL"=>"Nik sudah terdaftar, satu Nik tidak bisa di gunakan untuk 2 akun berbeda",
            "EMAIL_AVAIL"=>"Email sudah terdaftar, satu Email tidak bisa di gunakan untuk 2 akun berbeda",
            "INPUT_FAIL"=>"Input tidak tepat, mohon periksa input saudara kembali",
            "RE_PASSWORD_FAIL"=>"Mohon Ulangi password anda dengan benar",
            "RE_PASSWORD_FAIL"=>"Mohon Ulangi password anda dengan benar",
            "AVAIL_DATA_PENELITIAN"=>"maaf",
            "PENDAFTARAN_FAIL"=>"Proses pendaftaran gagal, silahkan periksa kelengkapan data dan jaringan anda lagi..!",
            "PENDAFTARAN_DOC_FAIL"=>"Mohon maaf proses pendaftaran di batalkan, Silahkan periksa kelengkapan dokumen saudara dan Lakukan pendafatran ulang..",
            "PENDAFTARAN_AVAIL_FAIL"=>"Mohon maaf, pendaftaran Gagal di terima.. Anda masih memeiliki program proposal atau magang yang belum di selesaikan..",
            "REGISTER_FAIL"=>"Mohon maaf register anda gagal, silahkan periksa kembali input saudara.",
            "ACTIVATION_FAIL"=>"Aktivasi gagal, silahkan lakukan aktivasi ulang untuk mengaktifkan akun anda.",
            "DATE_START_FAIL"=> "Mohon maaf tanggal mulai harus lebih kecil dari tanggal berakhir.",
            
            "UPDATE_FAIL"=>"Mohon maaf, Proses update gagal silahkan perikasa input saudara lagi",
            "INSERT_FAIL"=>"Mohon maaf, Proses insert gagal silahkan perikasa input saudara lagi",
            "DELETE_FAIL"=>"Mohon maaf, Proses delete gagal silahkan perikasa input saudara lagi",
            "UPLOAD_FAIL"=>"Mohon maaf, uploade file gagal, silahkan update kembali file saudara",

            "GET_FAIL"=>"Mohon maaf, proses pengambilan ini gagal. Silahkan periksa jaringan saudara atau coba lagi nanti",
            "ACCESS_FAIL"=>"Mohon maaf, anda tidak mendapat autorisasi untuk akses ke halaman ini. Untuk infromasi lebih lanjut silahkan hubungi Dinas Kominfo Kota Malang :)",
            
            "LOG_FAIL"=>"Mohon maaf, Login gagal, silahkan patikan username dan password saudara telah sesuai"
        );
        
        return $errors_msg[$id_message];
    }
    
    public function get_success_msg($id_message){
        $succes_msg = array(
            "REG_SUC"=>"Permintaan registrasi saudara telah di terima, Silahkan buka email saudara dan klik tautan yang sudah kami kirim ke email saudara",
            "LOG_SUC"=>"Login Berhasil, Selamat datang di halaman admin",
            
            "UPDATE_PROF_SUC"=>"Permintaan perubahan saudara telah di terima",
            "UPDATE_PROF_SUC_EMAIL"=>"Permintaan perubahan saudara telah di terima, Silahkan buka email saudara dan klik tautan yang sudah kami kirim ke email saudara",
            
            "PENDAFTARAN_SUC"=>"Proses pendaftaran berhasil, silahkan masuk ke halaman riwayat untuk melakukan cetak surat pernyataan..!",

            "REGISTER_SUC"=>"Register anda berhasil, kami sudah mengirimkan email verifikasi kepada email saudara.",

            "ACTIVATION_SUC"=>"Aktivasi sukses, silahkan login untuk mendaftar.",

            "GET_SUC"=>"get data success",
            "ACCESS_SUC"=>"auth success",

            "UPDATE_SUC"=>"Update berhasil",
            "INSERT_SUC"=>"Insert berhasil",
            "DELETE_SUC"=>"Delete berhasil"
        );
        
        return $succes_msg[$id_message];
    }
    
    public function default_mgs($msg_main, $msg_detail){
        return array(
                    "msg_main"=>$msg_main,
                    "msg_detail"=>$msg_detail
                );
    }
}
?>